# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0002_auto_20150228_1820'),
    ]

    operations = [
        migrations.CreateModel(
            name='Schedule',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('prev_date', models.DateField()),
                ('prev_time', models.TimeField()),
                ('schedule_date', models.DateField()),
                ('schedule_time', models.TimeField()),
                ('place', models.CharField(max_length=15)),
                ('sent_to', models.TextField()),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
