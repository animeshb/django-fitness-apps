# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('fitness', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='UserAssessment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('assessment_date', models.DateField()),
                ('assessment_time', models.TimeField()),
                ('assessent_unit', models.IntegerField()),
                ('assessment', models.ForeignKey(to='fitness.Assessment')),
                ('user', models.ForeignKey(to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='assessment',
            name='users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL, through='fitness.UserAssessment'),
            preserve_default=True,
        ),
    ]
