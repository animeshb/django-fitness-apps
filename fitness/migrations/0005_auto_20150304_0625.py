# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('fitness', '0004_auto_20150303_1837'),
    ]

    operations = [
        migrations.AlterField(
            model_name='schedule',
            name='prev_time',
            field=models.TimeField(null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='schedule',
            name='schedule_time',
            field=models.TimeField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
