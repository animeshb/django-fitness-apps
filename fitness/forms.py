from django.forms import *
from fitness.models import *

from datetime import datetime

from django import forms

'''
class CommentForm(ModelForm):
    class Meta:
        model = BlogComment
        exclude = ["post"]

    def clean_author(self):
        # we can modify / raise validation error as it is run before save 
        # clean_fldname and clean() after all field validation done
        return self.cleaned_data.get("author") or "Anonymous"
'''

'''
Adding Extra Field
'''
user_lists = User.objects.values("id","email")
user_lists = [(x["id"],x["email"]) for x in user_lists]

class ScheduleForm(ModelForm):
    class Meta:
        model = Schedule
        fields=["prev_date","prev_time", "schedule_date", "schedule_time","place"]
        widgets = {
            'prev_date': TextInput(attrs={'id':'datepicker1'}),

            'prev_time': TextInput(attrs={'id':'timepicker1'}),

            'schedule_date': TextInput(attrs={'id':'datepicker'}),

            'schedule_time': TextInput(attrs={'id':'timepicker'})
        }

##overide to add extra field in model form
class ScheduleExtraForm(ScheduleForm):

    user_id = forms.ChoiceField(choices=user_lists, label=u'Users', \
        widget=forms.Select(attrs={'id':'myselect'}))

    class Meta(ScheduleForm.Meta):
        fields = ScheduleForm.Meta.fields + ['user_id']
        
    '''
    def clean_prev_date(self):
        data = str(self.cleaned_data['prev_date'])
        data = datetime.strptime(data, "%m/%d/%Y").strftime('%Y-%m-%d')
        return data

    def clean_schedule_date(self):
        data = str(self.cleaned_data['schedule_date'])
        data = datetime.strptime(data, "%m/%d/%Y").strftime('%Y-%m-%d')
        return data
    '''


### custom form

class UserAssessForm(forms.Form):
    assessment_date = forms.CharField(max_length=10, \
        widget=forms.TextInput(attrs={'id':'datepicker'}))

    assessment_time = forms.CharField(max_length=10, \
        widget=forms.TextInput(attrs={'id':'timepicker'}))

    user_list = forms.ChoiceField(choices=user_lists, label=u'Users')


class ProfileForm(ModelForm):
    class Meta:
        model = Profile
        exclude=["created_at","updated_at","user"]



class UserForm(ModelForm):
    class Meta:
        model = User
        fields=["email","password", "first_name", "last_name","username"]        

class UserEditForm(ModelForm):
    class Meta:
        model = User
        fields=["email", "first_name", "last_name","username"]

class ContentForm(ModelForm):
    class Meta:
        model = Content
        exclude = ["created","updated"]