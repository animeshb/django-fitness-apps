from django.conf.urls import *
from fitness.views import *

urlpatterns = patterns("fitness.views",
   #(r"^post/(?P<dpk>\d+)/$"          , PostView.as_view(), {}, "post"),
   #(r"^archive_month/(\d+)/(\d+)/$"  , ArchiveMonth.as_view(), {}, "archive_month"),
   (r"^$" , HomeView.as_view(), {}, "home"),

   ## admin login

   url(r'^login/$', admin_login, {}, name='login'),
   url(r'^logout/$', admin_logout, {}, name='logout'),


   ## Schedule CRUD

   url(r'^schedule/list/$', schedule_list, {}, name='schedule-list'),
   url(r'^schedule/create/$', schedule_create, {}, name='schedule-create'),
   url(r'^schedule/delete/(?P<pk>\d+)/$', schedule_delete, {}, name='schedule-del'),


   ## Users Assess CRUD
   url(r'^userassess/list/$', user_assess_list, {}, name='user-assess-list'),
   url(r'^userassess/create/$', user_assess_create, {}, name='user-assess-create'),
   url(r'^userassess/edit/(?P<pk>\d+)/$', user_assess_update, {}, name='user-assess-detail'),
   url(r'^userassess/delete/(?P<pk>\d+)/$', user_assess_delete, {}, name='user-assess-del'),

   ### json response 
   url(r'^userassess/addassessmentperuser/$', user_assess_add, {}, name='user-assess-add'),


   ## Users CRUD
   url(r'^userlist/$', user_list, {}, name='user-list'),
   url(r'^user/create/$', user_create, {}, name='user-create'),
   url(r'^user/edit/(?P<pk>\d+)/$', user_update, {}, name='user-detail'),
   url(r'^user/delete/(?P<pk>\d+)/$', user_delete, {}, name='user-del'),


   ## News CRUD
   (r"^newslist/$", NewsListView.as_view(), {}, "news-list"),
   (r"^news/create$", ContentCreate.as_view(), {}, "news-create"),
   (r"^news/edit/(?P<pk>\d+)/$", ContentUpdate.as_view(), {}, "news-detail"),
   (r"^news/delete/(?P<pk>\d+)/$", ContentDelete.as_view(), {}, "news-del"),

   ##tips crud
   (r"^tipslist/$", TipsListView.as_view(), {}, "tips-list"),


    ## Medical CRUD
   (r"^medilist/$", MedicalList.as_view(), {}, "medi-list"),
   (r"^medi/create$", MedicalCreate.as_view(), {}, "medi-create"),
   (r"^medi/edit/(?P<pk>\d+)/$", MedicalUpdate.as_view(), {}, "medi-detail"),
   (r"^medi/delete/(?P<pk>\d+)/$", MedicalDelete.as_view(), {}, "medi-del"),


    ## Assess CRUD
   (r"^aseslist/$", AssessmentList.as_view(), {}, "ases-list"),
   (r"^ases/create$", AssessmentCreate.as_view(), {}, "ases-create"),
   (r"^ases/edit/(?P<pk>\d+)/$", AssessmentUpdate.as_view(), {}, "ases-detail"),
   (r"^ases/delete/(?P<pk>\d+)/$", AssessmentDelete.as_view(), {}, "ases-del"),


   # (r"^delete_comment/(\d+)/$"       , "delete_comment", {}, "delete_comment"),
)
