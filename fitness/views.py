from django.http import HttpResponse, JsonResponse
from django.views.generic.base import TemplateView
from django.views.generic.edit import DeleteView , CreateView, UpdateView
from django.core.urlresolvers import reverse_lazy

from django.views.generic.list import ListView
from django.shortcuts import redirect, get_object_or_404, render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

from django.forms.models import modelform_factory
from django.contrib.auth.hashers import make_password
from django.contrib.auth import authenticate, login, logout
from django.views.decorators.csrf import csrf_exempt
from django.db.models import Q
from datetime import datetime

from fitness.models import Content
from forms import *
import json


from django.core import serializers

'''
admin login
'''
def admin_logout(request):
    logout(request)
    return redirect('adminfitness:login')

def admin_login(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')

        # authenticate  a user
        user = authenticate(username=username, password=password)

        if user:
            if user.is_active and user.is_staff:
                #add user info in to session 
                login(request, user)
                return redirect('adminfitness:schedule-list')
            else:
                 return redirect('adminfitness:login')
        else:
            return redirect('adminfitness:login')
    else:
        return render(request, 'login.jinja', {})

'''
Schedule CRED
'''

def schedule_list(request, template_name='schedule_list.jinja'):
    schedule_list = Schedule.objects.all()
    paginator = Paginator(schedule_list, 4) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        schedule_list = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        schedule_list = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        schedule_list = paginator.page(paginator.num_pages)

    data = {}
    data['schedule_list'] = schedule_list
    return render(request, template_name, data)

def schedule_create(request, template_name='schedule_create.jinja'):

    ## model form with extra field ...
    ## will implement the push notification ...
    schedule_extra_form = ScheduleExtraForm(request.POST or None)

    if schedule_extra_form.is_valid():
        data = request.POST
        if data['wholeday'] == 1 :
            schedule_extra_form.cleaned_data['prev_time'] = ''
            schedule_extra_form.cleaned_data['schedule_time'] = ''

        schedule = schedule_extra_form.save(commit = False)
        if data['sentall']:
            user_info = data['sentall']
        else:
            user_info = [ v for (k,v) in data.iteritems() if k.find("sent_to")==0 ]
            user_info = json.dumps( user_info, ensure_ascii=False )
        schedule.sent_to = user_info
        schedule.save()
        return redirect('adminfitness:schedule-list')

    data = {}
    data['form'] = schedule_extra_form
    return render(request, template_name, data)

def schedule_delete(request, pk ):
    schedule = get_object_or_404(Schedule, pk=pk)    
    if request.method=='POST':
        schedule.delete()
    return redirect('adminfitness:schedule-list')

'''
User_Assessment CRUD
'''

def user_assess_list(request, template_name='user_assess_list.jinja'):
    
    user_assess = UserAssessment.objects.raw(' select * from fitness_userassessment group by assessment_date')

    
    #user_assess = UserAssessment.objects.values("id","assessment_date","assessment_time")
    paginator = Paginator(user_assess, 4) # Show 25 contacts per page
    paginator._count = len(list(user_assess))
    page = request.GET.get('page')
    try:
        user_assess = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        user_assess = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        user_assess = paginator.page(paginator.num_pages)

    

    data = {}
    data['user_assess'] = user_assess
    return render(request, template_name, data)

def user_assess_create(request, template_name='user_assess_cred.jinja'):
    userassess_form = UserAssessForm(request.POST or None)
    if userassess_form.is_valid():

        assess_date = userassess_form.cleaned_data['assessment_date']
        assess_date = datetime.strptime(assess_date, "%m/%d/%Y").strftime('%Y-%m-%d')
        assess_time = userassess_form.cleaned_data['assessment_time']

        data = request.POST
        assess_row = [ (k,v) for (k,v) in data.iteritems() if k.find("data")==0 ]

        assess_list = []
        for (k,v) in assess_row:
            k = k[3:].split("-")
            assess_id = k[1].split("]")[0]
            users_id = k[2].split("]")[0]
            if not v:
                v = '0'

            asssess_model = UserAssessment( user_id = users_id, assessment_id = assess_id, \
                assessment_date = assess_date, assessment_time = assess_time ,assessent_unit = v)
            
            assess_list.append(asssess_model)

        ## DELETE OLD rows if have with same date and time 

        
        UserAssessment.objects.filter(
            Q(assessment_time=assess_time),
            Q(assessment_date=assess_date) 
        ).delete()

        UserAssessment.objects.bulk_create(assess_list)
        
        return redirect('adminfitness:user-assess-list')


    data = {}
    data['create_edit'] = 1
    data['form'] = userassess_form
    data['assess_list'] = Assessment.objects.values("id","name","unit")
    assess_id_unit = {}
    for x in data['assess_list']:
        assess_id_unit[x['id']] = x['unit']
    data['assess_id_name'] = assess_id_unit
    return render(request, template_name, data)



def user_assess_update(request, pk,  template_name='user_assess_cred.jinja'):

    user_assess = get_object_or_404(UserAssessment, pk=pk)

    #users_id = user_assess.user_id
    assess_date = user_assess.assessment_date
    assess_time = user_assess.assessment_time
    
    form_init = initial={'assessment_date': assess_date, 'assessment_time': assess_time }
    userassess_form = UserAssessForm(request.POST or form_init )


    ##for post ...

    if userassess_form.is_valid():
        assess_date = userassess_form.cleaned_data['assessment_date']

        old_date = request.POST.get('old_date')
        old_time = request.POST.get('old_time')

        try:
            assess_date = datetime.strptime(assess_date, "%m/%d/%Y").strftime('%Y-%m-%d')
        except ValueError:
            assess_date = userassess_form.cleaned_data['assessment_date']

        
        assess_time = userassess_form.cleaned_data['assessment_time']

        data = request.POST
        assess_row = [ (k,v) for (k,v) in data.iteritems() if k.find("data")==0 ]

        assess_list = []
        for (k,v) in assess_row:
            k = k[3:].split("-")
            assess_id = k[1].split("]")[0]
            users_id = k[2].split("]")[0]
            if not v:
                v = '0'

            asssess_model = UserAssessment( user_id = users_id, assessment_id = assess_id, \
                assessment_date = assess_date, assessment_time = assess_time ,assessent_unit = v)
            
            assess_list.append(asssess_model)

       

        ## DELETE OLD rows if have with same date and time 

        
        UserAssessment.objects.filter(
            Q(assessment_time=old_time),
            Q(assessment_date=old_date) 
        ).delete()

        UserAssessment.objects.bulk_create(assess_list)
        
        return redirect('adminfitness:user-assess-list')


    ## for get display

    assess_list_current = UserAssessment.objects.filter(

        Q(assessment_time=assess_time),
        Q(assessment_date=assess_date) 
        ).order_by("user_id","assessment_id")


    user_id = [ x.user_id for x in assess_list_current]
    user_list = User.objects.filter(id__in=user_id)

    user_id_name = {}
    for x in user_list:
        user_id_name[x.id] = x.username

    

    data = {}
    data['old_date'] = assess_date
    data['old_time'] = assess_time
    data['user_id'] = pk
    data['create_edit'] = 2
    data['form'] = userassess_form
    data['tabinfo'] = assess_list_current
    data['assess_list'] = Assessment.objects.all().order_by("id")
    assess_id_unit = {}
    for x in data['assess_list']:
        assess_id_unit[x.id] = x.unit

    data['assess_id_name'] = assess_id_unit
    data['user_id_name'] = user_id_name
    ulist = user_id_name.keys()
    ulist = ','.join(ulist)
    data['ulist'] = ulist
    return render(request, template_name, data)

   
    

def user_assess_delete(request, pk):
    user_assess = get_object_or_404(UserAssessment, pk=pk)
    assess_date = user_assess.assessment_date
    assess_time = user_assess.assessment_time

    assess_list = UserAssessment.objects.filter(

        Q(assessment_time=assess_time),
        Q(assessment_date=assess_date) 
        ).delete()

    return redirect('adminfitness:user-assess-list')


@csrf_exempt
def user_assess_add(request):
    uid = request.POST.get('userId')
    assess_list = serializers.serialize('json', Assessment.objects.all(), fields=("id","name","unit"))
    user_info = User.objects.get(pk=uid)
    user_info = serializers.serialize('json', [user_info], ensure_ascii=False)
    user_info = user_info[1:-1]

    return JsonResponse({'assessment' : assess_list, 'user' : user_info},safe=False)

        




'''
User crud
'''
def user_list(request, template_name='user_list.jinja'):
    users = User.objects.values("id","email","date_joined")
    paginator = Paginator(users, 4) # Show 25 contacts per page
    page = request.GET.get('page')
    try:
        users = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        users = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        users = paginator.page(paginator.num_pages)

    data = {}
    data['userlist'] = users
    return render(request, template_name, data)


def user_create(request, template_name="user_cred.jinja"):
    user_form = UserForm(request.POST or None)
    profile_form = ProfileForm(request.POST or None)

    if user_form.is_valid() and profile_form.is_valid():
        user = user_form.save(commit = False)
        user.password = make_password(user_form.cleaned_data["password"])
        user.is_active = 1
        user.save()

        medi_list = request.POST.getlist("medical")
        ##save if any new medical exist 
        med_new = request.POST["medical99"]
        if med_new:
            med_new = Medical.objects.create(name=med_new)
            medi_list.append(med_new.id)

        ## saving many to many medical
        if len(medi_list):
            user.medical_set = medi_list
        
        #saving profile 
        profile = profile_form.save(commit = False)
        profile.user = user
        profile.save()
        return redirect('adminfitness:user-list')


    data = {}
    data['create_edit'] = 1
    data['user_form'] = user_form
    data['profile_form'] = profile_form
    data['medilist'] = Medical.objects.values("id","name")

    return render(request, template_name, data)
    

def user_update(request, pk, template_name='user_cred.jinja'):

    user = get_object_or_404(User, pk=pk)
    profile = user.profile
    ##list of medi id for the user by values()
    ## if we use all() it will return the obj list
    user_medi_list = user.medical_set.values("id")
    ##extract the value from dict
    user_medi_list = [val["id"] for val in user_medi_list]

    user_form = UserEditForm(request.POST or None, instance=user)
    profile_form = ProfileForm(request.POST or None, instance=profile)

    if user_form.is_valid() and profile_form.is_valid():
        user = user_form.save()
        user.save()

        medi_list = request.POST.getlist("medical")
        ##save if any new medical exist 
        med_new = request.POST["medical99"]
        if med_new:
            med_new = Medical.objects.create(name=med_new)
            medi_list.append(med_new.id)

        ## saving many to many medical
        if len(medi_list):
            user.medical_set = medi_list
        
        #saving profile 
        profile = profile_form.save(commit = False)
        profile.user = user
        profile.save()
        return redirect('adminfitness:user-list')


    data = {}
    data['create_edit'] = 2
    data['user_form'] = user_form
    data['profile_form'] = profile_form
    data['medilist'] = Medical.objects.values("id","name")
    data['user_medi_list'] = user_medi_list
    data['user_id'] = pk
    return render(request, template_name, data)
    

def user_delete(request, pk):
    user = get_object_or_404(User, pk=pk)    
    if request.method=='POST':
        user.medical_set.clear()
        user.delete()

    return redirect('adminfitness:user-list')

'''
Assessment crud
'''
class AssessmentList(ListView):
    paginate_by = 10
    template_name = 'fitness/aseslist.jinja'
    context_object_name = 'aseslist'
    model = Assessment

class AssessmentCreate(CreateView):
    model = Assessment
    fields = ['name', 'unit']
    success_url = reverse_lazy('adminfitness:ases-list')
    template_name = 'fitness/asescrtupd.jinja'
    def get_context_data(self, **kwargs):
        context = super(AssessmentCreate, self).get_context_data(**kwargs)
        context['create_edit'] = 1
        return context

class AssessmentUpdate(UpdateView):
    model = Assessment
    fields = ['name', 'unit']
    success_url = reverse_lazy('adminfitness:ases-list')
    template_name = 'fitness/asescrtupd.jinja'
   
    def get_context_data(self, **kwargs):
        context = super(AssessmentUpdate, self).get_context_data(**kwargs)
        context['create_edit'] = 2
        context['newsid'] = self.kwargs['pk']
        return context


class AssessmentDelete(DeleteView):
    model = Assessment
    success_url = reverse_lazy('adminfitness:ases-list')



'''
Medical crud
'''
class MedicalList(ListView):
    paginate_by = 10
    template_name = 'fitness/medilist.jinja'
    context_object_name = 'medilist'
    model = Medical

class MedicalCreate(CreateView):
    model = Medical
    fields = ['name']
    success_url = reverse_lazy('adminfitness:medi-list')
    template_name = 'fitness/medicrtupd.jinja'
    def get_context_data(self, **kwargs):
        context = super(MedicalCreate, self).get_context_data(**kwargs)
        context['create_edit'] = 1
        return context

class MedicalUpdate(UpdateView):
    model = Medical
    fields = ['name']
    success_url = reverse_lazy('adminfitness:medi-list')
    template_name = 'fitness/medicrtupd.jinja'
   
    def get_context_data(self, **kwargs):
        context = super(MedicalUpdate, self).get_context_data(**kwargs)
        context['create_edit'] = 2
        context['newsid'] = self.kwargs['pk']
        return context


class MedicalDelete(DeleteView):
    model = Medical
    fields = ['name']
    success_url = reverse_lazy('adminfitness:medi-list')



'''
Content  crud
'''

class ContentUpdate(UpdateView):
    template_name = 'fitness/content.jinja'
    form_class = ContentForm
    success_url = reverse_lazy('adminfitness:news-list')
    model = Content
    
    def get_success_url(self):
        if self.object.types == 1:
            return reverse_lazy('adminfitness:tips-list')
        reverse_lazy('adminfitness:news-list')

   
    def get_context_data(self, **kwargs):
        context = super(ContentUpdate, self).get_context_data(**kwargs)
        context['create_edit'] = 2
        context['newsid'] = self.kwargs['pk']
        context['cont_type'] = self.object.types
        #we can access the object via self.oject in reate/update view
        # after calling the super.form_valid
        return context
    
class ContentCreate(CreateView):
    template_name = 'fitness/content.jinja'
    form_class = ContentForm
    success_url = reverse_lazy('adminfitness:news-list') 
   
    def get_context_data(self, **kwargs):
        context = super(ContentCreate, self).get_context_data(**kwargs)
        context['create_edit'] = 1
        return context

'''
    def get_context_data(self, **kwargs):
        context = super(ContentCreate, self).get_context_data(**kwargs)
        context['newsid'] = kwargs['pk']
        return context
'''


class ContentDelete(DeleteView):
    model = Content
    success_url = reverse_lazy('adminfitness:news-list')


class HomeView(TemplateView):
    template_name = "fitness/index.html"

class NewsListView(ListView):
    paginate_by = 2
    context_object_name = 'newslist'
    queryset = Content.objects.filter(types=2)
    template_name = "fitness/news_list.jinja"

    def get_context_data(self, **kwargs):
        context = super(NewsListView, self).get_context_data(**kwargs)
        context['types'] = 'News'
        return context

class TipsListView(ListView):
    paginate_by = 2
    context_object_name = 'newslist'
    queryset = Content.objects.filter(types=1)
    template_name = "fitness/news_list.jinja"

    def get_context_data(self, **kwargs):
        context = super(TipsListView, self).get_context_data(**kwargs)
        context['types'] = 'Tips'
        return context

   