from django.db import models

from django.contrib.auth.models import User
'''
class UserMedical(models.Model):
    user = models.ForeignKey(User)
    medical = models.ForeignKey(Medical)
    ## always create intermediate if you need domething extra info to put 
    ##other wise declare any model as manyto many of other
''' 
class Schedule(models.Model):
    prev_date = models.DateField()
    prev_time = models.TimeField(blank=True,null=True)
    schedule_date = models.DateField()
    schedule_time = models.TimeField(blank=True,null=True)
    place = models.CharField(max_length=15)
    sent_to = models.TextField()

    def __unicode__(self):              # __unicode__ on Python 2
        return str(self.schedule_date)


gender_type = ( ("male",'Male'), ("female",'Female') )
class Profile(models.Model):
    regId = models.CharField(max_length=15)
    user = models.OneToOneField(User)
    gender = models.CharField(max_length=10,
        choices=gender_type,default=1)
    birthdate = models.DateField(auto_now_add=True)
    address1 = models.TextField(max_length=10)
    phone = models.CharField(max_length=15)
    mobile = models.CharField(max_length=15)
    address2 = models.TextField(max_length=10)
    city = models.CharField(max_length=10)
    postcode = models.CharField(max_length=10)
    state = models.CharField(max_length=10)
    country = models.CharField(max_length=10)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True) 

    def __unicode__(self):              # __unicode__ on Python 2
        return self.regId

class Assessment(models.Model):
    name = models.CharField(max_length=15)
    unit = models.IntegerField()
    users = models.ManyToManyField(User, through='UserAssessment')
    created_at = models.DateTimeField(auto_now_add=True)
    #firsttime will be now()
    updated_at = models.DateTimeField(auto_now=True) 
    def __unicode__(self):              # __unicode__ on Python 2
        return self.name


class UserAssessment(models.Model):
    user = models.ForeignKey(User)
    assessment = models.ForeignKey(Assessment)
    assessment_date = models.DateField()
    assessment_time = models.TimeField()
    assessent_unit = models.IntegerField()
    def __unicode__(self):
        # __unicode__ on Python 2
        return str(self.assessent_unit)



class Medical(models.Model):
    name = models.CharField(max_length=15)
    users = models.ManyToManyField(User,related_name="medical")
    created_at = models.DateTimeField(auto_now_add=True)
    #firsttime will be now()
    updated_at = models.DateTimeField(auto_now=True) 
    def __unicode__(self):              # __unicode__ on Python 2
        return self.name


news_type = ( (2,'News'), (1,'Tips') )
class Content(models.Model):
    title = models.CharField(max_length=15)
    body = models.TextField(max_length=100)
    url = models.URLField()
    types = models.IntegerField(max_length=2,
    	choices=news_type,default=1)

    created = models.DateTimeField(auto_now_add=True)
    #firsttime will be now()
    updated = models.DateTimeField(auto_now=True) 
    # every time save() called 
    def __unicode__(self):              # __unicode__ on Python 2
		return self.title
