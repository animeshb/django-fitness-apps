from django.contrib.auth.models import User
from rest_framework import serializers

from fitness.models import *

class MedicalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Medical
        fields = ('id','name')

class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('regId','gender','address1','phone','mobile','address2',\
            'city','postcode','state','country')

class UserProSerializer(serializers.ModelSerializer):
    medical = serializers.PrimaryKeyRelatedField(many=True, read_only=True)
    class Meta:
        model = User
        fields = ( 'first_name', 'last_name' , 'medical')

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('password', 'first_name', 'last_name', 'email','username')
        write_only_fields = ('password',)
 
    def restore_object(self, attrs, instance=None):
        # call set_password on user object. Without this
        # the password will be stored in plain text.
        user = super(UserSerializer, self).restore_object(attrs, instance)
        user.set_password(attrs['password'])
        return user



class NewsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Content
        fields = ('title','body','id','url')

class AssessSerializer(serializers.ModelSerializer):
    class Meta:
        model = Assessment
        fields = ('id','name','unit')

class UserAssessSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserAssessment
        fields = ('assessment_date','assessment_time','assessent_unit')


class ScheduleSerializer(serializers.ModelSerializer):
    class Meta:
        model = Schedule
        fields = ('prev_date','schedule_date','place','schedule_time','prev_time')

