from django.conf.urls import *


from services.views import *

urlpatterns = patterns("services.views",

		url(r'^users/profilemedical/$',profile_medical),

		url(r'^auth/$',AuthView.as_view(),name='authenticate'),

		url(r'^news/findtips/$',TipsList.as_view()),

		url(r'^news/display/$',NewsList.as_view()),

		url(r'^news/details/(?P<news_id>\d+)/$',NewsDetail.as_view()),

		url(r'^users/assessmentlist/$',AssessList.as_view()),

		url(r'^users/assessmentcurvedata/(?P<assess_id>\d+)/$', UserAssessList.as_view()),

		url(r'^schedules/getreschedule/(?P<username>[\w.%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4})/$', ScheduleList.as_view()),

	)

##urlpatterns = format_suffix_patterns(urlpatterns)

