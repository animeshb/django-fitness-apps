from django.shortcuts import render
from rest_framework import generics
from django.contrib.auth import login, logout
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from . import authentication
from serializers import *
from fitness.models import *
import datetime
from rest_framework.decorators import api_view
from django.views.decorators.csrf import csrf_exempt
from rest_framework.authentication import SessionAuthentication



@api_view(['GET', 'POST'])
def profile_medical(request, format=None):
    """
    List all snippets, or create a new snippet.
    """
    if request.method == 'GET':
        user = User.objects.get(pk=request.user.id)
        medi_list = Medical.objects.all()
        data = {}
        data['user'] = UserProSerializer(user).data
        data['profile'] = ProfileSerializer(user.profile).data
        data['medi_list'] = MedicalSerializer(medi_list, many=True).data
        return Response(data)

    elif request.method == 'POST':

        user = User.objects.get(pk=request.user.id)

        user_serial = UserProSerializer(user, data=request.data)
        profile_serial = ProfileSerializer(user.profile,data=request.data)
        if profile_serial.is_valid() and user_serial.is_valid():

            medi_list = request.POST.getlist("medical")
            ##save if any new medical exist 
            med_new = request.POST["MedicalsUser"]
            if med_new:
                med_new = Medical.objects.create(name=med_new)
                medi_list.append(med_new.id)

            ## saving many to many medical
            if len(medi_list):
                user.medical = medi_list

            user_serial.save()
            profile_serial.save(user=request.user)

            user = User.objects.get(pk=request.user.id)
            medi_list = Medical.objects.all()
            data = {}
            data['user'] = UserProSerializer(user).data
            data['profile'] = ProfileSerializer(user.profile).data
            data['medi_list'] = MedicalSerializer(medi_list, many=True).data
            return Response(data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class AuthView(APIView):
    authentication_classes = (authentication.QuietBasicAuthentication,)
    def post(self, request, *args, **kwargs):
        login(request, request.user)
        return Response(UserSerializer(request.user).data)
 
    def delete(self, request, *args, **kwargs):
        logout(request)
        print request
        return Response({})

class TipsList(generics.ListAPIView):
    authentication_classes = (SessionAuthentication,)
    queryset = Content.objects.filter(types=1)
    serializer_class = NewsSerializer


class NewsList(generics.ListAPIView):
    authentication_classes = (SessionAuthentication,)
    queryset = Content.objects.filter(types=2)
    serializer_class = NewsSerializer


class NewsDetail(generics.ListAPIView):
    authentication_classes = (SessionAuthentication,)
    serializer_class = NewsSerializer
    def get_queryset(self):
        return Content.objects.filter(id=self.kwargs.get('news_id'))

class ScheduleList(generics.ListAPIView):
    authentication_classes = (SessionAuthentication,)
    serializer_class = ScheduleSerializer
    def get_queryset(self):
    	username = self.kwargs.get('username')
    	scheds = Schedule.objects.filter(schedule_date__gt = datetime.datetime.now().strftime("%Y-%m-%d"))
        scheds = [ sched for sched in scheds if sched.sent_to=='all' or username in sched.sent_to.split(",")]
        return scheds


class AssessList(generics.ListAPIView):
    authentication_classes = (SessionAuthentication,)
    queryset = Assessment.objects.all()
    serializer_class = AssessSerializer

class UserAssessList(generics.ListAPIView):
    authentication_classes = (SessionAuthentication,)
    serializer_class = UserAssessSerializer

    def get_queryset(self):
        return UserAssessment.objects.filter(assessment=self.kwargs.get('assess_id'), \
        	user=self.request.user,)
