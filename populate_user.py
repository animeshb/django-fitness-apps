import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pollapp.settings')

import django
django.setup()

from fitness.models import User
import factory
from faker import Factory as FakerFactory
faker = FakerFactory.create()


def populate():
    python_cat = add_cat(name='Python',likes=2)

    frame_cat = add_cat("Other Frameworks" ,likes=3)

    add_page(cat=frame_cat,
        title="Bottle",
        url="http://bottlepy.org/docs/dev/")

    add_page(cat=frame_cat,
        title="Flask",
        url="http://flask.pocoo.org")

    # Print out what we have added to the user.
    for c in Category.objects.all():
        for p in Page.objects.filter(category=c):
            print "- {0} - {1}".format(str(c), str(p))

def add_page(username, email, first_name, last_name, password=123, is_staff=0, is_active=1, is_supervisor=0, last_login=timezone.n):
    p = User.objects.get_or_create(category=cat, title=title, url=url, views=views)[0]
    return p

def add_cat(name,likes):
    c = Category.objects.get_or_create(name=name,likes=likes)[0]
    return c

# Start execution here!
if __name__ == '__main__':
    print "Starting Rango population script..."
    populate()