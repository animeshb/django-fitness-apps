from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf.urls.static import static
from django.conf import settings

from snippets import views



urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'pyfitness.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include('fitness.urls' ,namespace="adminfitness" )),
    url(r'^snippets/', include('snippets.urls' ,namespace="snippets" )),
    url(r'^services/', include('services.urls' ,namespace="services" )),

    #url(r'^admin/', include(admin.site.urls)),
)+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) \
+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

