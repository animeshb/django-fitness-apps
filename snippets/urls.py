from django.conf.urls import *
from snippets.views import *
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = patterns("snippets.views",
		url(r'^$', snippet_list),
		url(r'^(?P<pk>[0-9]+)/$', snippet_detail),
		url(r'^users/$', UserList.as_view()),
		url(r'^users/(?P<pk>[0-9]+)/$', UserDetail.as_view()),
	)

urlpatterns = format_suffix_patterns(urlpatterns)

