-- phpMyAdmin SQL Dump
-- version 4.1.12
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Mar 07, 2015 at 12:12 PM
-- Server version: 5.6.16
-- PHP Version: 5.5.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `802`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group` (`group`,`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `group`, `key`, `description`) VALUES
(1, 'user', 'username', 'Select username'),
(2, 'blog', 'view', 'View blog entry'),
(3, 'blog', 'create', 'Create/Edit blog entry'),
(4, 'album', 'create', 'Create/Edit photo album'),
(5, 'album', 'view', 'View photo album'),
(6, 'event', 'create', 'Create/Edit event'),
(7, 'event', 'view', 'View event'),
(8, 'group', 'create', 'Create/Edit group'),
(9, 'group', 'view', 'View group'),
(10, 'photo', 'upload', 'Upload photos'),
(11, 'photo', 'view', 'View photo'),
(12, 'topic', 'create', 'Create/Edit topic'),
(13, 'topic', 'view', 'View topic'),
(14, 'video', 'share', 'Share video'),
(15, 'video', 'view', 'View video'),
(16, 'attachment', 'upload', 'Upload attachment'),
(17, 'attachment', 'download', 'Download attachment');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `action` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `item_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `activity_comment_count` smallint(5) unsigned NOT NULL,
  `query` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `guest_home` (`type`,`privacy`,`modified`),
  KEY `profile` (`target_id`,`type`,`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `activity_comments`
--

CREATE TABLE IF NOT EXISTS `activity_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `activity_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `wall_id` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_notifications`
--

CREATE TABLE IF NOT EXISTS `admin_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modified` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plugin_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `original_filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `downloads` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plugin_id` (`plugin_id`,`target_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blocked_members`
--

CREATE TABLE IF NOT EXISTS `blocked_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `category_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cake_sessions`
--

CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `weight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `header` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `create_permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `type` (`type`,`active`,`weight`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `type`, `parent_id`, `name`, `description`, `item_count`, `active`, `weight`, `header`, `create_permission`) VALUES
(1, 'album', 0, 'Member Albums', '', 0, 1, 0, 0, ''),
(2, 'group', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(3, 'topic', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(4, 'video', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(5, 'event', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(6, 'friend', 0, 'Friends', '', 0, 1, 0, 0, ''),
(7, 'blog', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(8, 'skin', 0, 'All Categories', '', 0, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `censor_words`
--

CREATE TABLE IF NOT EXISTS `censor_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `emails_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `words_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `replacement` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `censor_words`
--

INSERT INTO `censor_words` (`id`, `users_list`, `emails_list`, `words_list`, `replacement`) VALUES
(1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `circles`
--

CREATE TABLE IF NOT EXISTS `circles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `type` smallint(5) NOT NULL DEFAULT '1',
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type` (`type`,`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_groups`
--

CREATE TABLE IF NOT EXISTS `conversation_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(10) unsigned NOT NULL,
  `group_users` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conversation_id` (`conversation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_users`
--

CREATE TABLE IF NOT EXISTS `conversation_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `unread` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conversation_id` (`conversation_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE IF NOT EXISTS `conversations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `message_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `lastposter_id` (`lastposter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecard_photos`
--

CREATE TABLE IF NOT EXISTS `ecard_photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecard_id` int(10) unsigned NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ecard_id` (`ecard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecard_senders`
--

CREATE TABLE IF NOT EXISTS `ecard_senders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecard_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `receiver_id` int(10) unsigned NOT NULL,
  `text` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecards`
--

CREATE TABLE IF NOT EXISTS `ecards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `text` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_notifications_settings`
--

CREATE TABLE IF NOT EXISTS `email_notifications_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `get_request` tinyint(4) NOT NULL DEFAULT '1',
  `acceept_request` tinyint(4) NOT NULL DEFAULT '1',
  `luvs` tinyint(4) NOT NULL DEFAULT '1',
  `hifives` tinyint(4) NOT NULL DEFAULT '1',
  `hugs` tinyint(4) NOT NULL DEFAULT '1',
  `kisses` tinyint(4) NOT NULL DEFAULT '1',
  `winks` tinyint(4) NOT NULL DEFAULT '1',
  `update_photo` tinyint(4) NOT NULL DEFAULT '1',
  `update_status` tinyint(4) NOT NULL DEFAULT '1',
  `upcoming_birthday` tinyint(4) NOT NULL DEFAULT '1',
  `new_message` tinyint(4) NOT NULL DEFAULT '1',
  `group_invite` tinyint(4) NOT NULL DEFAULT '1',
  `group_topic` tinyint(4) NOT NULL DEFAULT '1',
  `group_post` tinyint(4) NOT NULL DEFAULT '1',
  `comment_status` tinyint(4) NOT NULL DEFAULT '1',
  `comment_photo` tinyint(4) NOT NULL DEFAULT '1',
  `like_status` tinyint(4) NOT NULL DEFAULT '1',
  `like_photo` tinyint(4) NOT NULL DEFAULT '1',
  `comment_blog` tinyint(4) NOT NULL DEFAULT '1',
  `like_blog` tinyint(4) NOT NULL DEFAULT '1',
  `comment_event` tinyint(4) NOT NULL DEFAULT '1',
  `like_event` tinyint(4) NOT NULL DEFAULT '1',
  `new_event` tinyint(4) NOT NULL DEFAULT '1',
  `profile_view` tinyint(4) NOT NULL DEFAULT '1',
  `group_rank` tinyint(4) NOT NULL DEFAULT '1',
  `alert_reminder` tinyint(4) NOT NULL DEFAULT '1',
  `site_newsletter` tinyint(4) NOT NULL DEFAULT '1',
  `ecards_received` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_rsvps`
--

CREATE TABLE IF NOT EXISTS `event_rsvps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `rsvp` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rsvp_list` (`event_id`,`user_id`),
  KEY `event_id` (`event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from` date NOT NULL,
  `from_time` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `to` date NOT NULL,
  `to_time` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `event_rsvp_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `address` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events_category_settings`
--

CREATE TABLE IF NOT EXISTS `events_category_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `location` tinyint(4) NOT NULL DEFAULT '1',
  `address` tinyint(4) NOT NULL DEFAULT '1',
  `cat_description` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friend_requests`
--

CREATE TABLE IF NOT EXISTS `friend_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_accounts`
--

CREATE TABLE IF NOT EXISTS `gold_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `virtual_cash` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_discounts`
--

CREATE TABLE IF NOT EXISTS `gold_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bundle_id` int(11) NOT NULL,
  `bundle_name` varchar(200) NOT NULL,
  `discount_start_date` datetime NOT NULL,
  `discount_end_date` datetime NOT NULL,
  `discount_rate` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_payment_histories`
--

CREATE TABLE IF NOT EXISTS `gold_payment_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `virtual_cash` int(11) NOT NULL,
  `real_money` decimal(10,2) NOT NULL,
  `reason` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goldbundles`
--

CREATE TABLE IF NOT EXISTS `goldbundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `gold_coins` int(11) NOT NULL,
  `real_money` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goldsettings`
--

CREATE TABLE IF NOT EXISTS `goldsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `options` varchar(200) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `goldsettings`
--

INSERT INTO `goldsettings` (`id`, `options`, `value`) VALUES
(1, 'paypal_email', ''),
(2, 'live_or_sandbox_paypal', '1'),
(3, 'gold_conversion_rate', '2'),
(4, 'give_to_friend', '1'),
(5, 'friend_gift_cost', '10'),
(6, 'admin__gold_account', '');

-- --------------------------------------------------------

--
-- Table structure for table `group_users`
--

CREATE TABLE IF NOT EXISTS `group_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` tinyint(2) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(2) unsigned NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_user_count` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `topic_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `video_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `event_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `category_id`, `name`, `group_url`, `user_id`, `description`, `type`, `photo`, `group_user_count`, `created`, `photo_count`, `topic_count`, `video_count`, `event_count`, `comment_count`, `featured`) VALUES
(1, 3, 'Community', 'Community', 0, 'This group is common for all community topics', 1, '', 1, '2014-08-02 12:49:49', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hifives`
--

CREATE TABLE IF NOT EXISTS `hifives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hooks`
--

CREATE TABLE IF NOT EXISTS `hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `weight` smallint(6) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `version` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `controller` (`controller`),
  KEY `action` (`action`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `hooks`
--

INSERT INTO `hooks` (`id`, `name`, `key`, `controller`, `action`, `position`, `weight`, `enabled`, `version`, `settings`, `permission`) VALUES
(1, 'Featured Members', 'featured_members', 'home', 'index', 'home_sidebar', 6, 1, '2.0', 'a:0:{}', ''),
(2, 'Popular Blogs', 'popular_blogs', 'blogs', 'index', 'blogs_sidebar', 2, 1, '2.0', 'a:1:{s:9:"num_blogs";s:1:"5";}', ''),
(3, 'Popular Albums', 'popular_albums', 'photos', 'index', 'photos_sidebar', 3, 1, '2.0', 'a:1:{s:10:"num_albums";s:1:"5";}', ''),
(4, 'Today Birthdays', 'today_birthdays', 'home', 'index', 'home_sidebar', 4, 1, '2.0', 'a:1:{s:16:"friend_birthdays";s:1:"1";}', ''),
(5, 'Online Users', 'online_users', '', '', 'global_sidebar', 5, 1, '2.0', 'a:1:{s:16:"num_online_users";s:2:"12";}', ''),
(6, 'Recently Joined', 'recently_joined', 'home', 'index', 'home_sidebar', 7, 1, '2.0', 'a:1:{s:15:"num_new_members";s:2:"10";}', ''),
(7, 'Popular Events', 'popular_events', 'events', 'index', 'events_sidebar', 8, 1, '2.0', 'a:1:{s:10:"num_events";s:1:"5";}', ''),
(8, 'Popular Groups', 'popular_groups', 'groups', 'index', 'groups_sidebar', 9, 1, '2.0', 'a:1:{s:10:"num_groups";s:1:"5";}', ''),
(9, 'Popular Topics', 'popular_topics', 'topics', 'index', 'topics_sidebar', 10, 1, '2.0', 'a:1:{s:10:"num_topics";s:1:"5";}', ''),
(10, 'Popular Videos', 'popular_videos', 'videos', 'index', 'videos_sidebar', 11, 1, '2.0', 'a:1:{s:10:"num_videos";s:1:"5";}', ''),
(11, 'Friend Suggestions', 'friend_suggestions', '', '', 'global_sidebar', 12, 1, '2.0', 'a:1:{s:22:"num_friend_suggestions";s:1:"2";}', ''),
(12, 'Featured Groups', 'featured_groups', 'groups', 'index', 'groups_sidebar', 13, 1, '2.0', 'a:0:{}', ''),
(13, 'Profile Luv', 'profile_luvs', '', '', 'profile_sidebar', 14, 1, '1.0', '', ''),
(14, 'Profile Affections', 'profile_affections', '', '', 'profile_sidebar', 15, 1, '1.0', '', ''),
(15, 'Notifications', 'notifications', '', '', 'profile_sidebar', 16, 1, '1.0', 'a:1:{s:17:"num_notifications";s:2:"10";}', ''),
(16, 'Online Friends', 'online_friends', '', '', 'profile_sidebar', 17, 1, '1.0', 'a:1:{s:11:"num_friends";s:2:"10";}', '');

-- --------------------------------------------------------

--
-- Table structure for table `hugs`
--

CREATE TABLE IF NOT EXISTS `hugs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kisses`
--

CREATE TABLE IF NOT EXISTS `kisses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `key`, `name`) VALUES
(1, 'eng', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `layouts`
--

CREATE TABLE IF NOT EXISTS `layouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout` text COLLATE utf8_unicode_ci,
  `modified` datetime NOT NULL,
  `has_header` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `has_footer` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

--
-- Dumping data for table `layouts`
--

INSERT INTO `layouts` (`id`, `controller`, `task`, `layout`, `modified`, `has_header`, `has_footer`) VALUES
(1, 'Groups', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/group_tabs.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/view.ctp"}]}]}]}', '2014-05-14 14:46:00', 1, 1),
(2, 'Groups', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"groups/leftcol_list.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"groups/list.ctp"}]}]}]}', '2014-02-22 16:53:36', 1, 1),
(3, 'Affections', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"hook","label":"Hook","title":"","divid":"jvleftnav","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"affections/index.ctp"}]}]}]}', '2014-02-11 15:46:53', 1, 1),
(4, 'Pages', 'display', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"pages/index.ctp"}]}]}]}', '2014-02-11 20:22:40', 1, 1),
(5, 'Home', 'contact', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/contact.ctp"}]}]}]}', '2014-02-22 14:28:32', 1, 1),
(6, 'Home', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/header.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/index.ctp"}]}]}]}', '2014-06-24 06:54:05', 1, 1),
(7, 'Conversations', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"conversations/view.ctp"}]}]}]}', '2014-05-09 14:17:46', 0, 0),
(8, 'Users', 'register', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"jvLogin","contents":[{"type":"span8","label":"Column - span8","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/register-bg.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/registration.ctp"}]}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_three.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_four.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_five.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_six.ctp"}]}]}]}', '2014-06-24 06:48:56', 1, 1),
(9, 'Users', 'password', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/password.ctp"}]}]}]}', '2014-06-12 16:25:45', 1, 1),
(10, 'Users', 'recover', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/recover.ctp"}]}]}]}', '2014-02-23 13:53:59', 1, 1),
(11, 'Users', 'resetpass', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/resetpass.ctp"}]}]}]}', '2014-02-23 13:55:12', 1, 1),
(12, 'Users', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/view_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/view_leftcol.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"users/view_avatar.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"profile_luvs","name":"Profile Luv"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"profile_affections","name":"Profile Affections"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/view_center.ctp"}]}]}]}', '2014-06-10 18:05:31', 1, 1),
(13, 'Users', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/index_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"users/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/index_center.ctp"}]}]}]}', '2014-02-23 13:53:05', 1, 1),
(14, 'Videos', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/index.ctp"}]}]}]}', '2014-02-23 13:56:08', 1, 1),
(15, 'Videos', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"videos/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/view.ctp"}]}]}]}', '2014-02-23 13:56:19', 1, 1),
(16, 'Blogs', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/index.ctp"}]}]}]}', '2014-02-11 20:14:07', 1, 1),
(17, 'Blogs', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span7","label":"Column - span7","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/view.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-02-11 21:45:29', 1, 1),
(18, 'Blogs', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/create.ctp"}]}]}]}', '2014-02-11 20:13:37', 1, 1),
(19, 'Events', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"events/index.ctp"}]}]}]}', '2014-02-11 20:36:07', 1, 1),
(20, 'Events', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"events/view_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/view.ctp"}]}]}]}', '2014-02-11 20:35:37', 1, 1),
(21, 'Events', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/create.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-05-01 15:46:46', 1, 1),
(22, 'Tags', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"tags/view_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"tags/view.ctp"}]}]}]}', '2014-02-11 20:16:13', 1, 1),
(23, 'Search', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"search/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"search/index.ctp"}]}]}]}', '2014-02-11 20:16:41', 1, 1),
(24, 'Photos', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"jvleftnav","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index.ctp"}]}]}]}', '2014-02-22 16:54:25', 1, 1),
(25, 'Photos', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/view.ctp"}]}]}]}', '2014-02-11 20:21:13', 1, 1),
(26, 'Photos', 'upload', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/upload.ctp"}]}]}]}', '2014-02-11 20:22:14', 1, 1),
(27, 'Albums', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"hook","label":"Hook","title":"","divid":"leftnav","divclass":"","key":"popular_albums","name":"Popular Albums"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"albums/view.ctp"}]}]}]}', '2014-02-11 15:48:07', 1, 1),
(28, 'Albums', 'edit', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"albums/edit.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-02-11 15:47:34', 1, 1),
(29, 'Friends', 'my', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/users/ajax_browse/home"}]}]}]}', '2014-03-02 18:41:19', 1, 1),
(30, 'Friends', 'invite', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/friends/ajax_invite"}]}]}]}', '2014-02-22 14:27:25', 1, 1),
(31, 'Friends', 'requests', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/requests.ctp"}]}]}]}', '2014-02-22 14:27:37', 1, 1),
(32, 'Notifications', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/notifications/ajax_show/home"}]}]}]}', '2014-02-22 16:54:04', 1, 1),
(33, 'Conversations', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/conversations/ajax_browse"}]}]}]}', '2014-02-23 21:35:11', 1, 1),
(34, 'Users', 'visitors', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/visitors.ctp"}]}]}]}', '2014-02-23 13:55:32', 1, 1),
(35, 'Friends', 'favourites', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/users/ajax_browse/favourites"}]}]}]}', '2014-02-22 14:27:04', 1, 1),
(36, 'Users', 'blocked_users', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/blocked_users.ctp"}]}]}]}', '2014-06-12 16:26:56', 1, 1),
(37, 'Friends', 'birthdays', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/friends/ajax_browse/birthdays"}]}]}]}', '2014-04-24 01:05:25', 1, 1),
(38, 'Users', 'verifyanswer', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"users/verifyanswer.ctp"}]}]}]}', '2014-04-24 19:37:25', 1, 1),
(39, 'Groups', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/create.ctp"}]}]}]}', '2014-05-15 21:31:26', 1, 1),
(40, 'Events', 'all_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/all_events.ctp"}]}]}]}', '2014-05-05 16:48:59', 1, 1),
(41, 'Events', 'categorized_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/categorized_events.ctp"}]}]}]}', '2014-05-05 17:05:48', 1, 1),
(42, 'Events', 'friends_attend_event', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/friends_attend_event.ctp"}]}]}]}', '2014-05-06 16:40:42', 1, 1),
(43, 'Events', 'past_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/past_events.ctp"}]}]}]}]}', '2014-05-06 16:44:01', 1, 1),
(44, 'Events', 'my_upcoming_event', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/my_upcoming_event.ctp"}]}]}]}', '2014-05-06 16:48:05', 1, 1),
(45, 'Blogs', 'my_blogs', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/my_blogs.ctp"}]}]}]}', '2014-05-06 17:54:17', 1, 1),
(46, 'Blogs', 'friends_blogs', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/friends_blogs.ctp"}]}]}]}', '2014-05-06 18:04:37', 1, 1),
(47, 'Videos', 'my_videos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"videos/my_videos.ctp"}]}]}]}', '2014-05-09 15:25:05', 1, 1),
(48, 'Videos', 'friends_videos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/friends_videos.ctp"}]}]}]}', '2014-05-07 17:00:05', 1, 1),
(49, 'Users', 'setnotifications', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/setnotifications.ctp"}]}]}]}', '2014-06-12 16:26:16', 1, 1),
(50, 'Albums', 'my_albums', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"jvcenter","divclass":"","path":"albums/my_albums.ctp"}]}]}]}', '2014-05-07 16:54:17', 1, 1),
(51, 'Albums', 'friends_albums', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"albums/friends_albums.ctp"}]}]}]}', '2014-05-07 16:57:00', 1, 1),
(52, 'Conversations', 'my_conversation', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/my_conversation.ctp"}]}]}]}', '2014-05-16 19:35:06', 1, 1),
(53, 'Friends', 'my_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/my_friends.ctp"}]}]}]}', '2014-05-09 13:36:47', 1, 1),
(54, 'Friends', 'favourite_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/favourite_friends.ctp"}]}]}]}', '2014-05-09 13:32:00', 1, 1),
(55, 'Users', 'profile', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/profile.ctp"}]}]}]}', '2014-06-12 16:24:52', 1, 1);
INSERT INTO `layouts` (`id`, `controller`, `task`, `layout`, `modified`, `has_header`, `has_footer`) VALUES
(56, 'Users', 'my_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/index_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"users/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/my_friends.ctp"}]}]}]}', '2014-05-22 13:53:05', 1, 1),
(57, 'Friends', 'requests_received', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/requests_received.ctp"}]}]}]}', '2014-05-09 13:36:47', 1, 1),
(58, 'Home', 'activities', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/header.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"notifications","name":"Notifications"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/activities.ctp"}]}]}]}', '2014-02-22 14:49:50', 1, 1),
(59, 'Topics', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/index.ctp"}]}]}]}', '2014-06-10 18:51:48', 1, 1),
(60, 'Topics', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/view.ctp"}]}]}]}', '2014-06-10 18:49:06', 1, 1),
(61, 'Topics', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"topics/create.ctp"}]}]}]}', '2014-06-10 18:55:25', 1, 1),
(62, 'Videos', 'videosearch', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"videos/videosearch.ctp"}]}]}]}', '2014-06-18 15:14:15', 1, 1),
(63, 'Playlists', 'allplaylists', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"playlists/allplaylists.ctp"}]}]}]}', '2014-06-18 15:17:45', 1, 1),
(64, 'Playlists', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"playlists/view.ctp"}]}]}]}', '2014-06-18 15:21:49', 1, 1),
(65, 'Ecards', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"ecards/index.ctp"}]}]}]}', '2014-06-23 14:19:01', 1, 1),
(66, 'Ecards', 'my_ecards', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/my_ecards.ctp"}]}]}]}', '2014-06-23 14:20:50', 1, 1),
(67, 'Ecards', 'send_wigets', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/send_wigets.ctp"}]}]}]}', '2014-06-23 14:22:35', 1, 1),
(68, 'Ecards', 'rcv_ecards', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/rcv_ecards.ctp"}]}]}]}', '2014-06-10 18:57:25', 1, 1),
(69, 'Videos', 'storedvideos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/storedvideos.ctp"}]}]}]}', '2014-07-10 15:40:57', 1, 1),
(70, 'Goldbundles', 'buygold', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/buygold.ctp"}]}]}', '2014-07-15 13:15:15', 1, 1),
(71, 'Goldbundles', 'buygoldhistory', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/buygoldhistory.ctp"}]}]}]}', '2014-07-15 13:17:13', 1, 1),
(72, 'Goldbundles', 'friend_gift', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/friend_gift.ctp"}]}]}]}', '2014-07-15 13:19:36', 1, 1),
(73, 'Playlists', 'sharedvideos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"playlists/sharedvideos.ctp"}]}]}]}', '2014-08-01 11:19:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_up` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type` (`type`,`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_infos`
--

CREATE TABLE IF NOT EXISTS `login_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created` date NOT NULL,
  `count` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `luvs`
--

CREATE TABLE IF NOT EXISTS `luvs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mooskin_users`
--

CREATE TABLE IF NOT EXISTS `mooskin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mooskin_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mooskin_id` (`mooskin_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mooskins`
--

CREATE TABLE IF NOT EXISTS `mooskins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `total_used` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `mooskins`
--

INSERT INTO `mooskins` (`id`, `name`, `category_id`, `total_used`, `status`, `created`) VALUES
(1, 'default', 8, 0, 1, '0000-00-00 00:00:00'),
(2, 'First Skin', 8, 0, 1, '0000-00-00 00:00:00'),
(3, 'Second Skin', 8, 0, 1, '0000-00-00 00:00:00'),
(4, 'Cars', 8, 0, 1, '0000-00-00 00:00:00'),
(5, 'Audi', 8, 0, 1, '0000-00-00 00:00:00'),
(6, 'Lamborghini', 8, 0, 1, '0000-00-00 00:00:00'),
(7, 'Citroen', 8, 0, 1, '0000-00-00 00:00:00'),
(8, 'American Choppers', 8, 0, 1, '0000-00-00 00:00:00'),
(9, 'American Choppers2', 8, 0, 1, '0000-00-00 00:00:00'),
(10, 'Bugatti', 8, 0, 1, '0000-00-00 00:00:00'),
(11, 'Custom Bike', 8, 0, 1, '0000-00-00 00:00:00'),
(12, 'Ducati', 8, 0, 1, '0000-00-00 00:00:00'),
(13, 'Ducati V2', 8, 0, 1, '0000-00-00 00:00:00'),
(14, 'Harley', 8, 0, 1, '0000-00-00 00:00:00'),
(15, 'My Buddy', 8, 0, 1, '0000-00-00 00:00:00'),
(16, 'My Buddy V2', 8, 0, 1, '0000-00-00 00:00:00'),
(17, 'The Veyron', 8, 0, 1, '0000-00-00 00:00:00'),
(18, 'The Veyron White', 8, 0, 1, '0000-00-00 00:00:00'),
(19, 'The Veyron White V2', 8, 0, 1, '0000-00-00 00:00:00'),
(20, 'Yamaha R1', 8, 0, 1, '0000-00-00 00:00:00'),
(21, 'BMW Bike', 8, 0, 1, '0000-00-00 00:00:00'),
(22, 'Boga Lake', 8, 0, 1, '0000-00-00 00:00:00'),
(23, 'Field', 8, 0, 1, '0000-00-00 00:00:00'),
(24, 'Flower', 8, 0, 1, '0000-00-00 00:00:00'),
(25, 'Hill', 8, 0, 1, '0000-00-00 00:00:00'),
(26, 'House', 8, 0, 1, '0000-00-00 00:00:00'),
(27, 'Nature', 8, 0, 1, '0000-00-00 00:00:00'),
(28, 'Pink Tree', 8, 0, 1, '0000-00-00 00:00:00'),
(29, 'River', 8, 0, 1, '0000-00-00 00:00:00'),
(30, 'Road', 8, 0, 1, '0000-00-00 00:00:00'),
(31, 'Tree', 8, 0, 1, '0000-00-00 00:00:00'),
(32, 'Brazil 2014', 8, 0, 1, '0000-00-00 00:00:00'),
(33, 'England', 8, 0, 1, '0000-00-00 00:00:00'),
(34, 'FIFA World Cup 2014', 8, 0, 1, '0000-00-00 00:00:00'),
(35, 'Football', 8, 0, 1, '0000-00-00 00:00:00'),
(36, 'Garman', 8, 0, 1, '0000-00-00 00:00:00'),
(37, 'Italy', 8, 0, 1, '0000-00-00 00:00:00'),
(38, 'Lion El Messi', 8, 0, 1, '0000-00-00 00:00:00'),
(39, 'Messi', 8, 0, 1, '0000-00-00 00:00:00'),
(40, 'Messi Addias', 8, 0, 1, '0000-00-00 00:00:00'),
(41, 'Samba', 8, 0, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `myvideos`
--

CREATE TABLE IF NOT EXISTS `myvideos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `group_id` int(10) unsigned DEFAULT NULL,
  `playlist_id` int(11) DEFAULT '0',
  `position_order` int(11) DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `sender_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `params` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `alias`, `content`, `permission`, `params`, `created`, `modified`, `menu`, `icon_class`, `weight`) VALUES
(1, 'Terms', 'terms', '<p>These are our terms</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:19:26', '2014-06-10 16:19:26', 1, '', 0),
(2, 'About', 'about', '<p>This is all about us</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:20:19', '2014-06-10 16:20:19', 1, '', 0),
(3, 'Privacy', 'privacy', '<p>This is your privacy</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:21:03', '2014-06-10 16:21:03', 1, '', 0),
(4, 'Online Safety', 'online_safety', '<p>Be safe</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:21:52', '2014-06-10 16:21:52', 1, '', 0),
(5, 'Help', 'help', '<p>Here is some help</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:22:21', '2014-06-10 16:22:21', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pass_settings`
--

CREATE TABLE IF NOT EXISTS `pass_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(50) NOT NULL,
  `value` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_requests`
--

CREATE TABLE IF NOT EXISTS `password_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo_tags`
--

CREATE TABLE IF NOT EXISTS `photo_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `tagger_id` int(10) unsigned NOT NULL,
  `value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `style` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `photo_id` (`photo_id`),
  KEY `tagger_id` (`tagger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `target_id` (`target_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE IF NOT EXISTS `playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `video_count` int(11) NOT NULL DEFAULT '0',
  `cover` varchar(500) DEFAULT NULL,
  `shared_playlist` tinyint(4) NOT NULL DEFAULT '0',
  `on_off` tinyint(4) DEFAULT '0',
  `display_in_profile` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `core` tinyint(1) unsigned NOT NULL,
  `version` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `weight` smallint(5) unsigned NOT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `key`, `permission`, `settings`, `enabled`, `core`, `version`, `weight`, `menu`, `url`, `icon_class`) VALUES
(1, 'People', 'user', '', 'a:0:{}', 1, 1, '2.0', 1, 1, '/users', 'icon-user'),
(2, 'Blogs', 'blog', '', '', 1, 1, '2.0', 2, 1, '/blogs', 'icon-edit'),
(3, 'Photos', 'photo', '', '', 1, 1, '2.0', 3, 1, '/photos', 'icon-picture'),
(4, 'Videos', 'video', '', '', 1, 1, '2.0', 4, 1, '/videos', 'icon-facetime-video'),
(5, 'Topics', 'topic', '', '', 1, 1, '2.0', 5, 1, '/topics', 'icon-comments'),
(6, 'Groups', 'group', '', '', 1, 1, '2.0', 6, 1, '/groups', 'icon-group'),
(7, 'Events', 'event', '', '', 1, 1, '2.0', 7, 1, '/events', 'icon-calendar'),
(8, 'Conversations', 'conversation', '', '', 1, 1, '2.0', 8, 0, '', ''),
(9, 'Pages', 'page', '', '', 1, 1, '2.0', 9, 0, '', ''),
(10, 'Maintenance', 'maintenance', '', '', 1, 0, '1.0', 10, 0, '', ''),
(11, 'Layout Builder', 'layoutbuilder', '', '', 1, 0, '1.0', 11, 1, '', ''),
(12, 'Affections', 'affection', '', 'a:11:{s:13:"luvs_duration";s:1:"3";s:19:"luvs_perday_regular";s:1:"3";s:15:"luvs_perday_vip";s:2:"10";s:21:"hugs_per_user_regular";s:1:"1";s:17:"hugs_per_user_vip";s:1:"3";s:24:"hifives_per_user_regular";s:1:"1";s:20:"hifives_per_user_vip";s:1:"3";s:23:"kisses_per_user_regular";s:1:"1";s:19:"kisses_per_user_vip";s:1:"3";s:22:"winks_per_user_regular";s:1:"1";s:18:"winks_per_user_vip";s:1:"3";}', 1, 0, '1.0', 12, 1, '/affections', '');

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings`
--

CREATE TABLE IF NOT EXISTS `privacy_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `action_title` varchar(100) NOT NULL,
  `action_controller` varchar(200) NOT NULL,
  `privacy_cat_value` int(11) NOT NULL DEFAULT '0',
  `privacy_sub_cat` int(11) NOT NULL DEFAULT '0',
  `comment_privacy` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `privacy_settings`
--

INSERT INTO `privacy_settings` (`id`, `user_id`, `action_title`, `action_controller`, `privacy_cat_value`, `privacy_sub_cat`, `comment_privacy`) VALUES
(1, 0, 'Profile View', 'users_view', 1, 0, 0),
(2, 0, 'Album View', 'albums_view', 1, 0, 0),
(3, 0, 'Blogs View', 'blogs_view', 1, 0, 0),
(4, 0, 'Contact Message', 'conversations_ajax_doSend', 1, 0, 0),
(5, 0, 'Video Comment', 'videos_view', 1, 0, 1),
(6, 0, 'Photo Comment', 'photos_view', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings_categories`
--

CREATE TABLE IF NOT EXISTS `privacy_settings_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `privacy_settings_categories`
--

INSERT INTO `privacy_settings_categories` (`id`, `name`, `value`, `type`) VALUES
(1, 'public', 1, 0),
(2, 'member', 2, 1),
(3, 'me', 3, 0),
(4, 'friend', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile_field_values`
--

CREATE TABLE IF NOT EXISTS `profile_field_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `profile_field_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `profile_field_id` (`profile_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_fields`
--

CREATE TABLE IF NOT EXISTS `profile_fields` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `values` text COLLATE utf8_unicode_ci NOT NULL,
  `required` tinyint(1) unsigned NOT NULL,
  `registration` tinyint(1) unsigned NOT NULL,
  `searchable` tinyint(1) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  `weight` smallint(5) unsigned NOT NULL,
  `profile` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_views`
--

CREATE TABLE IF NOT EXISTS `profile_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `viewer_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ranks`
--

CREATE TABLE IF NOT EXISTS `ranks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `min_posts` mediumint(8) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_super` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `is_admin`, `is_super`, `params`, `core`) VALUES
(1, 'Super Admin', 1, 1, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,group_delete,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1),
(2, 'Member', 0, 0, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1),
(3, 'Guest', 0, 0, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1);

-- --------------------------------------------------------

--
-- Table structure for table `security_questions`
--

CREATE TABLE IF NOT EXISTS `security_questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `security_questions`
--

INSERT INTO `security_questions` (`id`, `question`, `active`) VALUES
(1, 'What was your childhood nickname? ', 1),
(2, 'In what city did you meet your spouse/significant other?', 1),
(3, 'What is the name of your favorite childhood friend? ', 1),
(4, 'What street did you live on in third grade?', 1),
(5, 'What is your oldest sibling''s middle name?', 1),
(6, 'What was the name of your first stuffed animal?', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `field` (`field`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=138 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `field`, `value`) VALUES
(1, 'site_name', 'animesh'),
(2, 'site_email', 'animesh.ruet@gmail.com'),
(3, 'site_keywords', ''),
(4, 'site_description', ''),
(5, 'default_feed', 'everyone'),
(6, 'recaptcha', '0'),
(7, 'recaptcha_publickey', ''),
(8, 'recaptcha_privatekey', ''),
(9, 'email_validation', '0'),
(10, 'guest_message', ''),
(11, 'header_code', ''),
(12, 'sidebar_code', ''),
(13, 'footer_code', ''),
(14, 'member_message', ''),
(15, 'select_theme', '1'),
(16, 'guest_search', '1'),
(17, 'default_theme', 'jcoterie'),
(18, 'force_login', '1'),
(19, 'homepage_code', ''),
(20, 'ban_ips', ''),
(21, 'feed_selection', '1'),
(22, 'admin_notes', ''),
(23, 'site_offline', '0'),
(24, 'offline_message', ''),
(25, 'cron_last_run', ''),
(26, 'version', '2.0.2'),
(27, 'profile_privacy', '1'),
(28, 'fb_app_id', ''),
(29, 'fb_app_secret', ''),
(30, 'fb_integration', '0'),
(31, 'popular_interval', '30'),
(32, 'add_this', '1'),
(33, 'default_language', 'eng'),
(34, 'disable_registration', '0'),
(35, 'time_format', '12h'),
(36, 'date_format', 'F j \\a\\t g:ia'),
(37, 'logo', ''),
(38, 'hide_admin_link', '0'),
(39, 'select_language', '1'),
(40, 'analytics_code', ''),
(41, 'registration_notify', '0'),
(42, 'hide_activites', '0'),
(43, 'username_change', '1'),
(44, 'save_original_image', '0'),
(45, 'mail_transport', 'Mail'),
(46, 'smtp_host', 'localhost'),
(47, 'smtp_username', ''),
(48, 'smtp_password', ''),
(49, 'smtp_port', ''),
(50, 'restricted_usernames', ''),
(51, 'enable_registration_code', '0'),
(52, 'registration_code', ''),
(53, 'languages', 'eng=English'),
(54, 'google_dev_key', ''),
(55, 'timezone', 'Africa/Abidjan'),
(56, 'enable_timezone_selection', '1'),
(57, 'require_birthday', '1'),
(58, 'enable_spam_challenge', '0'),
(59, 'show_credit', '0'),
(60, 'name_change', '1'),
(61, 'registration_message', ''),
(62, 'admin_password', ''),
(63, 'recently_joined', '1'),
(64, 'featured_members', '1'),
(65, 'num_new_members', '10'),
(66, 'num_friend_suggestions', '2'),
(67, 'friend_birthdays', '0'),
(68, 'set_autorefresh_interval', ''),
(69, 'enable_security_ques', ''),
(70, 'enable_fan_pages', ''),
(71, 'number_fan_pages', ''),
(72, 'censor_word', ''),
(73, 'users_not_adult', ''),
(74, 'default_subject', ''),
(75, 'default_body', ''),
(76, 'get_request_subject', ''),
(77, 'get_request_body', ''),
(78, 'acceept_request_subject', ''),
(79, 'acceept_request_body', ''),
(80, 'affection_subject', ''),
(81, 'affection_body', ''),
(82, 'update_photo_subject', ''),
(83, 'update_photo_body', ''),
(84, 'update_status_subject', ''),
(85, 'update_status_body', ''),
(86, 'upcoming_birthday_subject', ''),
(87, 'upcoming_birthday_body', ''),
(88, 'new_message_subject', ''),
(89, 'new_message_body', ''),
(90, 'group_invite_subject', ''),
(91, 'group_invite_body', ''),
(92, 'group_topic_subject', ''),
(93, 'group_topic_body', ''),
(94, 'group_post_subject', ''),
(95, 'group_post_body', ''),
(96, 'comment_status_subject', ''),
(97, 'comment_status_body', ''),
(98, 'comment_photo_subject', ''),
(99, 'comment_photo_body', ''),
(100, 'like_status_subject', ''),
(101, 'like_status_body', ''),
(102, 'like_photo_subject', ''),
(103, 'like_photo_body', ''),
(104, 'comment_blog_subject', ''),
(105, 'comment_blog_body', ''),
(106, 'like_blog_subject', ''),
(107, 'like_blog_body', ''),
(108, 'comment_event_subject', ''),
(109, 'comment_event_body', ''),
(110, 'like_event_subject', ''),
(111, 'like_event_body', ''),
(112, 'new_event_subject', ''),
(113, 'new_event_body', ''),
(114, 'profile_view_subject', ''),
(115, 'profile_view_body', ''),
(116, 'group_rank_subject', ''),
(117, 'group_rank_body', ''),
(118, 'alert_reminder_subject', ''),
(119, 'alert_reminder_body', ''),
(120, 'site_newsletter_subject', ''),
(121, 'site_newsletter_body', ''),
(122, 'admin_approval_registration', '0'),
(123, 'event_auto_publish', '0'),
(124, 'blog_auto_publish', '0'),
(125, 'ecards_received_subject', ''),
(126, 'ecards_received_body', ''),
(127, 'waitfor_approval_subject', ''),
(128, 'waitfor_approval_body', ''),
(129, 'approval_req_subject', ''),
(130, 'approval_req_body', ''),
(131, 'account_approved_subject', ''),
(132, 'account_approved_body', ''),
(133, 'override_privacy_settings', '0'),
(134, 'uploaded_video_auto_publish', '0'),
(135, 'uploaded_video_size_limit', '50'),
(136, 'retricted_wordlist', ''),
(137, 'enable_myvideos', '0');

-- --------------------------------------------------------

--
-- Table structure for table `sharedbyme_playlists`
--

CREATE TABLE IF NOT EXISTS `sharedbyme_playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sharedwithme_playlists`
--

CREATE TABLE IF NOT EXISTS `sharedwithme_playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `skins`
--

CREATE TABLE IF NOT EXISTS `skins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `color` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `font_family` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `font_size` int(10) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `spam_challenges`
--

CREATE TABLE IF NOT EXISTS `spam_challenges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `answers` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status_updates`
--

CREATE TABLE IF NOT EXISTS `status_updates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_create` tinyint(4) NOT NULL DEFAULT '1',
  `blog_comment` tinyint(4) NOT NULL DEFAULT '1',
  `comment_add` tinyint(4) NOT NULL DEFAULT '1',
  `event_attend` tinyint(4) NOT NULL DEFAULT '1',
  `event_create` tinyint(4) NOT NULL DEFAULT '1',
  `friend_add` tinyint(4) NOT NULL DEFAULT '1',
  `group_create` tinyint(4) NOT NULL DEFAULT '1',
  `group_join` tinyint(4) NOT NULL DEFAULT '1',
  `like_add` tinyint(4) NOT NULL DEFAULT '1',
  `photos_add` tinyint(4) NOT NULL DEFAULT '1',
  `photos_tag` tinyint(4) NOT NULL DEFAULT '1',
  `topic_create` tinyint(4) NOT NULL DEFAULT '1',
  `video_add` tinyint(4) NOT NULL DEFAULT '1',
  `wall_post` tinyint(4) NOT NULL DEFAULT '1',
  `wall_post_link` tinyint(4) NOT NULL DEFAULT '1',
  `user_create` tinyint(4) NOT NULL DEFAULT '1',
  `user_avatar` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `status_updates`
--

INSERT INTO `status_updates` (`id`, `blog_create`, `blog_comment`, `comment_add`, `event_attend`, `event_create`, `friend_add`, `group_create`, `group_join`, `like_add`, `photos_add`, `photos_tag`, `topic_create`, `video_add`, `wall_post`, `wall_post_link`, `user_create`, `user_avatar`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `tag` (`tag`),
  KEY `target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `key`, `name`, `core`) VALUES
(3, 'mobile', 'Mobile Theme', 1),
(4, 'jcoterie', 'jCoterie', 1);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `last_post` datetime NOT NULL,
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(10) unsigned NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL,
  `pinned` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `attachment` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `announcement` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `topic_category_id` (`category_id`),
  KEY `lastposter_id` (`lastposter_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_group_likes`
--

CREATE TABLE IF NOT EXISTS `user_group_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `num_likes` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_user_like_key` (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_group_posts`
--

CREATE TABLE IF NOT EXISTS `user_group_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `num_posts` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_user_post_key` (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `security_question_id` tinyint(3) NOT NULL,
  `security_answers` text CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `friend_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `notification_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `friend_request_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `blog_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `topic_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `conversation_user_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `video_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `confirm_status` tinyint(3) NOT NULL DEFAULT '0',
  `confirmed` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `notification_email` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `timezone` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `hide_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cover` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `luvs` float NOT NULL DEFAULT '0',
  `profile_views` int(10) unsigned NOT NULL DEFAULT '0',
  `disabled_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_username` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_by_admin` tinyint(4) DEFAULT '0',
  `no_word_restriction` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `gender` (`gender`),
  KEY `active` (`active`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `email`, `password`, `role_id`, `security_question_id`, `security_answers`, `avatar`, `photo`, `created`, `last_login`, `photo_count`, `friend_count`, `notification_count`, `friend_request_count`, `blog_count`, `topic_count`, `conversation_user_count`, `video_count`, `gender`, `birthday`, `active`, `confirm_status`, `confirmed`, `code`, `notification_email`, `timezone`, `ip_address`, `privacy`, `username`, `url`, `about`, `featured`, `lang`, `hide_online`, `cover`, `luvs`, `profile_views`, `disabled_name`, `disabled_username`, `disabled_avatar`, `disabled_photo`, `disabled_cover`, `approved_by_admin`, `no_word_restriction`) VALUES
(1, 'animesh', 'biswas', 'animesh.ruet@gmail.com', '$2y$10$4AxL0NcTQTO3Yks60nspY.6E1T5qJkoNWBhnbDd.ASFMHBQpObT9K', 1, 0, '', '', '', '2014-08-02 12:50:19', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, 0, 'Male', '2014-08-02', 1, 0, 1, 'a00b0c0e2e325fb871dc0a8abcf1cd41', 1, 'Africa/Abidjan', '', 1, '', '', '', 0, '', 0, '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `group_id` int(10) unsigned NOT NULL,
  `playlist_id` int(11) DEFAULT '0',
  `position_order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_positions`
--

CREATE TABLE IF NOT EXISTS `widget_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parentbox` varchar(50) NOT NULL,
  `positions` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_settings`
--

CREATE TABLE IF NOT EXISTS `widget_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '5',
  `photos` int(11) NOT NULL,
  `friends` int(11) NOT NULL DEFAULT '5',
  `videos` int(11) NOT NULL DEFAULT '5',
  `blogs` int(11) NOT NULL DEFAULT '5',
  `groups` int(11) NOT NULL DEFAULT '5',
  `wallone` int(11) NOT NULL DEFAULT '5',
  `status` int(11) NOT NULL DEFAULT '5',
  `ecard` int(11) NOT NULL DEFAULT '5',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_types`
--

CREATE TABLE IF NOT EXISTS `widget_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE IF NOT EXISTS `widgets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `widget_type_id` int(10) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `order` smallint(5) unsigned NOT NULL DEFAULT '1',
  `wallposition` int(11) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `winks`
--

CREATE TABLE IF NOT EXISTS `winks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
--
-- Database: `803`
--
--
-- Database: `804`
--
--
-- Database: `805`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group` (`group`,`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `group`, `key`, `description`) VALUES
(1, 'user', 'username', 'Select username'),
(2, 'blog', 'view', 'View blog entry'),
(3, 'blog', 'create', 'Create/Edit blog entry'),
(4, 'album', 'create', 'Create/Edit photo album'),
(5, 'album', 'view', 'View photo album'),
(6, 'event', 'create', 'Create/Edit event'),
(7, 'event', 'view', 'View event'),
(8, 'group', 'create', 'Create/Edit group'),
(9, 'group', 'view', 'View group'),
(10, 'photo', 'upload', 'Upload photos'),
(11, 'photo', 'view', 'View photo'),
(12, 'topic', 'create', 'Create/Edit topic'),
(13, 'topic', 'view', 'View topic'),
(14, 'video', 'share', 'Share video'),
(15, 'video', 'view', 'View video'),
(16, 'attachment', 'upload', 'Upload attachment'),
(17, 'attachment', 'download', 'Download attachment');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `action` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `item_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `activity_comment_count` smallint(5) unsigned NOT NULL,
  `query` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `guest_home` (`type`,`privacy`,`modified`),
  KEY `profile` (`target_id`,`type`,`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `activity_comments`
--

CREATE TABLE IF NOT EXISTS `activity_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `activity_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `wall_id` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_notifications`
--

CREATE TABLE IF NOT EXISTS `admin_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modified` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plugin_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `original_filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `downloads` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plugin_id` (`plugin_id`,`target_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blocked_members`
--

CREATE TABLE IF NOT EXISTS `blocked_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `category_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cake_sessions`
--

CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `weight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `header` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `create_permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `type` (`type`,`active`,`weight`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `type`, `parent_id`, `name`, `description`, `item_count`, `active`, `weight`, `header`, `create_permission`) VALUES
(1, 'album', 0, 'Member Albums', '', 0, 1, 0, 0, ''),
(2, 'group', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(3, 'topic', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(4, 'video', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(5, 'event', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(6, 'friend', 0, 'Friends', '', 0, 1, 0, 0, ''),
(7, 'blog', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(8, 'skin', 0, 'All Categories', '', 0, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `censor_words`
--

CREATE TABLE IF NOT EXISTS `censor_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `emails_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `words_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `replacement` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `censor_words`
--

INSERT INTO `censor_words` (`id`, `users_list`, `emails_list`, `words_list`, `replacement`) VALUES
(1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `circles`
--

CREATE TABLE IF NOT EXISTS `circles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `type` smallint(5) NOT NULL DEFAULT '1',
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type` (`type`,`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_groups`
--

CREATE TABLE IF NOT EXISTS `conversation_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(10) unsigned NOT NULL,
  `group_users` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conversation_id` (`conversation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_users`
--

CREATE TABLE IF NOT EXISTS `conversation_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `unread` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conversation_id` (`conversation_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE IF NOT EXISTS `conversations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `message_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `lastposter_id` (`lastposter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecard_photos`
--

CREATE TABLE IF NOT EXISTS `ecard_photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecard_id` int(10) unsigned NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ecard_id` (`ecard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecard_senders`
--

CREATE TABLE IF NOT EXISTS `ecard_senders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecard_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `receiver_id` int(10) unsigned NOT NULL,
  `text` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecards`
--

CREATE TABLE IF NOT EXISTS `ecards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `text` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_notifications_settings`
--

CREATE TABLE IF NOT EXISTS `email_notifications_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `get_request` tinyint(4) NOT NULL DEFAULT '1',
  `acceept_request` tinyint(4) NOT NULL DEFAULT '1',
  `luvs` tinyint(4) NOT NULL DEFAULT '1',
  `hifives` tinyint(4) NOT NULL DEFAULT '1',
  `hugs` tinyint(4) NOT NULL DEFAULT '1',
  `kisses` tinyint(4) NOT NULL DEFAULT '1',
  `winks` tinyint(4) NOT NULL DEFAULT '1',
  `update_photo` tinyint(4) NOT NULL DEFAULT '1',
  `update_status` tinyint(4) NOT NULL DEFAULT '1',
  `upcoming_birthday` tinyint(4) NOT NULL DEFAULT '1',
  `new_message` tinyint(4) NOT NULL DEFAULT '1',
  `group_invite` tinyint(4) NOT NULL DEFAULT '1',
  `group_topic` tinyint(4) NOT NULL DEFAULT '1',
  `group_post` tinyint(4) NOT NULL DEFAULT '1',
  `comment_status` tinyint(4) NOT NULL DEFAULT '1',
  `comment_photo` tinyint(4) NOT NULL DEFAULT '1',
  `like_status` tinyint(4) NOT NULL DEFAULT '1',
  `like_photo` tinyint(4) NOT NULL DEFAULT '1',
  `comment_blog` tinyint(4) NOT NULL DEFAULT '1',
  `like_blog` tinyint(4) NOT NULL DEFAULT '1',
  `comment_event` tinyint(4) NOT NULL DEFAULT '1',
  `like_event` tinyint(4) NOT NULL DEFAULT '1',
  `new_event` tinyint(4) NOT NULL DEFAULT '1',
  `profile_view` tinyint(4) NOT NULL DEFAULT '1',
  `group_rank` tinyint(4) NOT NULL DEFAULT '1',
  `alert_reminder` tinyint(4) NOT NULL DEFAULT '1',
  `site_newsletter` tinyint(4) NOT NULL DEFAULT '1',
  `ecards_received` tinyint(4) NOT NULL DEFAULT '1',
  `shared_video` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_rsvps`
--

CREATE TABLE IF NOT EXISTS `event_rsvps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `rsvp` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rsvp_list` (`event_id`,`user_id`),
  KEY `event_id` (`event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from` date NOT NULL,
  `from_time` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `to` date NOT NULL,
  `to_time` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `event_rsvp_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `address` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events_category_settings`
--

CREATE TABLE IF NOT EXISTS `events_category_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `location` tinyint(4) NOT NULL DEFAULT '1',
  `address` tinyint(4) NOT NULL DEFAULT '1',
  `cat_description` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friend_requests`
--

CREATE TABLE IF NOT EXISTS `friend_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_accounts`
--

CREATE TABLE IF NOT EXISTS `gold_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `virtual_cash` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_discounts`
--

CREATE TABLE IF NOT EXISTS `gold_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bundle_id` int(11) NOT NULL,
  `bundle_name` varchar(200) NOT NULL,
  `discount_start_date` datetime NOT NULL,
  `discount_end_date` datetime NOT NULL,
  `discount_rate` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_payment_histories`
--

CREATE TABLE IF NOT EXISTS `gold_payment_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `virtual_cash` int(11) NOT NULL,
  `real_money` decimal(10,2) NOT NULL,
  `reason` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goldbundles`
--

CREATE TABLE IF NOT EXISTS `goldbundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `gold_coins` int(11) NOT NULL,
  `real_money` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goldsettings`
--

CREATE TABLE IF NOT EXISTS `goldsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `options` varchar(200) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `goldsettings`
--

INSERT INTO `goldsettings` (`id`, `options`, `value`) VALUES
(1, 'paypal_email', ''),
(2, 'live_or_sandbox_paypal', '1'),
(3, 'gold_conversion_rate', '2'),
(4, 'give_to_friend', '1'),
(5, 'friend_gift_cost', '10'),
(6, 'admin__gold_account', '');

-- --------------------------------------------------------

--
-- Table structure for table `group_users`
--

CREATE TABLE IF NOT EXISTS `group_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` tinyint(2) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(2) unsigned NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_user_count` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `topic_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `video_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `event_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `category_id`, `name`, `group_url`, `user_id`, `description`, `type`, `photo`, `group_user_count`, `created`, `photo_count`, `topic_count`, `video_count`, `event_count`, `comment_count`, `featured`) VALUES
(1, 3, 'Community', 'Community', 0, 'This group is common for all community topics', 1, '', 1, '2014-08-05 20:04:57', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hifives`
--

CREATE TABLE IF NOT EXISTS `hifives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hooks`
--

CREATE TABLE IF NOT EXISTS `hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `weight` smallint(6) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `version` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `controller` (`controller`),
  KEY `action` (`action`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `hooks`
--

INSERT INTO `hooks` (`id`, `name`, `key`, `controller`, `action`, `position`, `weight`, `enabled`, `version`, `settings`, `permission`) VALUES
(1, 'Featured Members', 'featured_members', 'home', 'index', 'home_sidebar', 6, 1, '2.0', 'a:0:{}', ''),
(2, 'Popular Blogs', 'popular_blogs', 'blogs', 'index', 'blogs_sidebar', 2, 1, '2.0', 'a:1:{s:9:"num_blogs";s:1:"5";}', ''),
(3, 'Popular Albums', 'popular_albums', 'photos', 'index', 'photos_sidebar', 3, 1, '2.0', 'a:1:{s:10:"num_albums";s:1:"5";}', ''),
(4, 'Today Birthdays', 'today_birthdays', 'home', 'index', 'home_sidebar', 4, 1, '2.0', 'a:1:{s:16:"friend_birthdays";s:1:"1";}', ''),
(5, 'Online Users', 'online_users', '', '', 'global_sidebar', 5, 1, '2.0', 'a:1:{s:16:"num_online_users";s:2:"12";}', ''),
(6, 'Recently Joined', 'recently_joined', 'home', 'index', 'home_sidebar', 7, 1, '2.0', 'a:1:{s:15:"num_new_members";s:2:"10";}', ''),
(7, 'Popular Events', 'popular_events', 'events', 'index', 'events_sidebar', 8, 1, '2.0', 'a:1:{s:10:"num_events";s:1:"5";}', ''),
(8, 'Popular Groups', 'popular_groups', 'groups', 'index', 'groups_sidebar', 9, 1, '2.0', 'a:1:{s:10:"num_groups";s:1:"5";}', ''),
(9, 'Popular Topics', 'popular_topics', 'topics', 'index', 'topics_sidebar', 10, 1, '2.0', 'a:1:{s:10:"num_topics";s:1:"5";}', ''),
(10, 'Popular Videos', 'popular_videos', 'videos', 'index', 'videos_sidebar', 11, 1, '2.0', 'a:1:{s:10:"num_videos";s:1:"5";}', ''),
(11, 'Friend Suggestions', 'friend_suggestions', '', '', 'global_sidebar', 12, 1, '2.0', 'a:1:{s:22:"num_friend_suggestions";s:1:"2";}', ''),
(12, 'Featured Groups', 'featured_groups', 'groups', 'index', 'groups_sidebar', 13, 1, '2.0', 'a:0:{}', ''),
(13, 'Profile Luv', 'profile_luvs', '', '', 'profile_sidebar', 14, 1, '1.0', '', ''),
(14, 'Profile Affections', 'profile_affections', '', '', 'profile_sidebar', 15, 1, '1.0', '', ''),
(15, 'Notifications', 'notifications', '', '', 'profile_sidebar', 16, 1, '1.0', 'a:1:{s:17:"num_notifications";s:2:"10";}', ''),
(16, 'Online Friends', 'online_friends', '', '', 'profile_sidebar', 17, 1, '1.0', 'a:1:{s:11:"num_friends";s:2:"10";}', '');

-- --------------------------------------------------------

--
-- Table structure for table `hugs`
--

CREATE TABLE IF NOT EXISTS `hugs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kisses`
--

CREATE TABLE IF NOT EXISTS `kisses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `key`, `name`) VALUES
(1, 'eng', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `layouts`
--

CREATE TABLE IF NOT EXISTS `layouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout` text COLLATE utf8_unicode_ci,
  `modified` datetime NOT NULL,
  `has_header` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `has_footer` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

--
-- Dumping data for table `layouts`
--

INSERT INTO `layouts` (`id`, `controller`, `task`, `layout`, `modified`, `has_header`, `has_footer`) VALUES
(1, 'Groups', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/group_tabs.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/view.ctp"}]}]}]}', '2014-05-14 14:46:00', 1, 1),
(2, 'Groups', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"groups/leftcol_list.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"groups/list.ctp"}]}]}]}', '2014-02-22 16:53:36', 1, 1),
(3, 'Affections', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"hook","label":"Hook","title":"","divid":"jvleftnav","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"affections/index.ctp"}]}]}]}', '2014-02-11 15:46:53', 1, 1),
(4, 'Pages', 'display', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"pages/index.ctp"}]}]}]}', '2014-02-11 20:22:40', 1, 1),
(5, 'Home', 'contact', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/contact.ctp"}]}]}]}', '2014-02-22 14:28:32', 1, 1),
(6, 'Home', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/header.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/index.ctp"}]}]}]}', '2014-06-24 06:54:05', 1, 1),
(7, 'Conversations', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"conversations/view.ctp"}]}]}]}', '2014-05-09 14:17:46', 0, 0),
(8, 'Users', 'register', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"jvLogin","contents":[{"type":"span8","label":"Column - span8","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/register-bg.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/registration.ctp"}]}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_three.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_four.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_five.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_six.ctp"}]}]}]}', '2014-06-24 06:48:56', 1, 1),
(9, 'Users', 'password', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/password.ctp"}]}]}]}', '2014-06-12 16:25:45', 1, 1),
(10, 'Users', 'recover', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/recover.ctp"}]}]}]}', '2014-02-23 13:53:59', 1, 1),
(11, 'Users', 'resetpass', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/resetpass.ctp"}]}]}]}', '2014-02-23 13:55:12', 1, 1),
(12, 'Users', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/view_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/view_leftcol.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"users/view_avatar.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"profile_luvs","name":"Profile Luv"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"profile_affections","name":"Profile Affections"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/view_center.ctp"}]}]}]}', '2014-06-10 18:05:31', 1, 1),
(13, 'Users', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/index_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"users/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/index_center.ctp"}]}]}]}', '2014-02-23 13:53:05', 1, 1),
(14, 'Videos', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/index.ctp"}]}]}]}', '2014-02-23 13:56:08', 1, 1),
(15, 'Videos', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"videos/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/view.ctp"}]}]}]}', '2014-02-23 13:56:19', 1, 1),
(16, 'Blogs', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/index.ctp"}]}]}]}', '2014-02-11 20:14:07', 1, 1),
(17, 'Blogs', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span7","label":"Column - span7","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/view.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-02-11 21:45:29', 1, 1),
(18, 'Blogs', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/create.ctp"}]}]}]}', '2014-02-11 20:13:37', 1, 1),
(19, 'Events', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"events/index.ctp"}]}]}]}', '2014-02-11 20:36:07', 1, 1),
(20, 'Events', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"events/view_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/view.ctp"}]}]}]}', '2014-02-11 20:35:37', 1, 1),
(21, 'Events', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/create.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-05-01 15:46:46', 1, 1),
(22, 'Tags', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"tags/view_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"tags/view.ctp"}]}]}]}', '2014-02-11 20:16:13', 1, 1),
(23, 'Search', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"search/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"search/index.ctp"}]}]}]}', '2014-02-11 20:16:41', 1, 1),
(24, 'Photos', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"jvleftnav","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index.ctp"}]}]}]}', '2014-02-22 16:54:25', 1, 1),
(25, 'Photos', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/view.ctp"}]}]}]}', '2014-02-11 20:21:13', 1, 1),
(26, 'Photos', 'upload', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/upload.ctp"}]}]}]}', '2014-02-11 20:22:14', 1, 1),
(27, 'Albums', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"hook","label":"Hook","title":"","divid":"leftnav","divclass":"","key":"popular_albums","name":"Popular Albums"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"albums/view.ctp"}]}]}]}', '2014-02-11 15:48:07', 1, 1),
(28, 'Albums', 'edit', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"albums/edit.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-02-11 15:47:34', 1, 1),
(29, 'Friends', 'my', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/users/ajax_browse/home"}]}]}]}', '2014-03-02 18:41:19', 1, 1),
(30, 'Friends', 'invite', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/friends/ajax_invite"}]}]}]}', '2014-02-22 14:27:25', 1, 1),
(31, 'Friends', 'requests', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/requests.ctp"}]}]}]}', '2014-02-22 14:27:37', 1, 1),
(32, 'Notifications', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/notifications/ajax_show/home"}]}]}]}', '2014-02-22 16:54:04', 1, 1),
(33, 'Conversations', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/conversations/ajax_browse"}]}]}]}', '2014-02-23 21:35:11', 1, 1),
(34, 'Users', 'visitors', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/visitors.ctp"}]}]}]}', '2014-02-23 13:55:32', 1, 1),
(35, 'Friends', 'favourites', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/users/ajax_browse/favourites"}]}]}]}', '2014-02-22 14:27:04', 1, 1),
(36, 'Users', 'blocked_users', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/blocked_users.ctp"}]}]}]}', '2014-06-12 16:26:56', 1, 1),
(37, 'Friends', 'birthdays', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/friends/ajax_browse/birthdays"}]}]}]}', '2014-04-24 01:05:25', 1, 1),
(38, 'Users', 'verifyanswer', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"users/verifyanswer.ctp"}]}]}]}', '2014-04-24 19:37:25', 1, 1),
(39, 'Groups', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/create.ctp"}]}]}]}', '2014-05-15 21:31:26', 1, 1),
(40, 'Events', 'all_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/all_events.ctp"}]}]}]}', '2014-05-05 16:48:59', 1, 1),
(41, 'Events', 'categorized_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/categorized_events.ctp"}]}]}]}', '2014-05-05 17:05:48', 1, 1),
(42, 'Events', 'friends_attend_event', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/friends_attend_event.ctp"}]}]}]}', '2014-05-06 16:40:42', 1, 1),
(43, 'Events', 'past_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/past_events.ctp"}]}]}]}]}', '2014-05-06 16:44:01', 1, 1),
(44, 'Events', 'my_upcoming_event', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/my_upcoming_event.ctp"}]}]}]}', '2014-05-06 16:48:05', 1, 1),
(45, 'Blogs', 'my_blogs', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/my_blogs.ctp"}]}]}]}', '2014-05-06 17:54:17', 1, 1),
(46, 'Blogs', 'friends_blogs', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/friends_blogs.ctp"}]}]}]}', '2014-05-06 18:04:37', 1, 1),
(47, 'Videos', 'my_videos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"videos/my_videos.ctp"}]}]}]}', '2014-05-09 15:25:05', 1, 1),
(48, 'Videos', 'friends_videos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/friends_videos.ctp"}]}]}]}', '2014-05-07 17:00:05', 1, 1),
(49, 'Users', 'setnotifications', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/setnotifications.ctp"}]}]}]}', '2014-06-12 16:26:16', 1, 1),
(50, 'Albums', 'my_albums', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"jvcenter","divclass":"","path":"albums/my_albums.ctp"}]}]}]}', '2014-05-07 16:54:17', 1, 1),
(51, 'Albums', 'friends_albums', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"albums/friends_albums.ctp"}]}]}]}', '2014-05-07 16:57:00', 1, 1),
(52, 'Conversations', 'my_conversation', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/my_conversation.ctp"}]}]}]}', '2014-05-16 19:35:06', 1, 1),
(53, 'Friends', 'my_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/my_friends.ctp"}]}]}]}', '2014-05-09 13:36:47', 1, 1),
(54, 'Friends', 'favourite_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/favourite_friends.ctp"}]}]}]}', '2014-05-09 13:32:00', 1, 1),
(55, 'Users', 'profile', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/profile.ctp"}]}]}]}', '2014-06-12 16:24:52', 1, 1);
INSERT INTO `layouts` (`id`, `controller`, `task`, `layout`, `modified`, `has_header`, `has_footer`) VALUES
(56, 'Users', 'my_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/index_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"users/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/my_friends.ctp"}]}]}]}', '2014-05-22 13:53:05', 1, 1),
(57, 'Friends', 'requests_received', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/requests_received.ctp"}]}]}]}', '2014-05-09 13:36:47', 1, 1),
(58, 'Home', 'activities', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/header.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"notifications","name":"Notifications"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/activities.ctp"}]}]}]}', '2014-02-22 14:49:50', 1, 1),
(59, 'Topics', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/index.ctp"}]}]}]}', '2014-06-10 18:51:48', 1, 1),
(60, 'Topics', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/view.ctp"}]}]}]}', '2014-06-10 18:49:06', 1, 1),
(61, 'Topics', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"topics/create.ctp"}]}]}]}', '2014-06-10 18:55:25', 1, 1),
(62, 'Videos', 'videosearch', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"videos/videosearch.ctp"}]}]}]}', '2014-06-18 15:14:15', 1, 1),
(63, 'Playlists', 'allplaylists', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"playlists/allplaylists.ctp"}]}]}]}', '2014-06-18 15:17:45', 1, 1),
(64, 'Playlists', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"playlists/view.ctp"}]}]}]}', '2014-06-18 15:21:49', 1, 1),
(65, 'Ecards', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"ecards/index.ctp"}]}]}]}', '2014-06-23 14:19:01', 1, 1),
(66, 'Ecards', 'my_ecards', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/my_ecards.ctp"}]}]}]}', '2014-06-23 14:20:50', 1, 1),
(67, 'Ecards', 'send_wigets', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/send_wigets.ctp"}]}]}]}', '2014-06-23 14:22:35', 1, 1),
(68, 'Ecards', 'rcv_ecards', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/rcv_ecards.ctp"}]}]}]}', '2014-06-10 18:57:25', 1, 1),
(69, 'Videos', 'storedvideos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/storedvideos.ctp"}]}]}]}', '2014-07-10 15:40:57', 1, 1),
(70, 'Goldbundles', 'buygold', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/buygold.ctp"}]}]}', '2014-07-15 13:15:15', 1, 1),
(71, 'Goldbundles', 'buygoldhistory', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/buygoldhistory.ctp"}]}]}]}', '2014-07-15 13:17:13', 1, 1),
(72, 'Goldbundles', 'friend_gift', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/friend_gift.ctp"}]}]}]}', '2014-07-15 13:19:36', 1, 1),
(73, 'Playlists', 'sharedvideos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"playlists/sharedvideos.ctp"}]}]}]}', '2014-08-01 11:19:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_up` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type` (`type`,`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_infos`
--

CREATE TABLE IF NOT EXISTS `login_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created` date NOT NULL,
  `count` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `luvs`
--

CREATE TABLE IF NOT EXISTS `luvs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mooskin_users`
--

CREATE TABLE IF NOT EXISTS `mooskin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mooskin_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mooskin_id` (`mooskin_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mooskins`
--

CREATE TABLE IF NOT EXISTS `mooskins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `total_used` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `mooskins`
--

INSERT INTO `mooskins` (`id`, `name`, `category_id`, `total_used`, `status`, `created`) VALUES
(1, 'default', 8, 0, 1, '0000-00-00 00:00:00'),
(2, 'First Skin', 8, 0, 1, '0000-00-00 00:00:00'),
(3, 'Second Skin', 8, 0, 1, '0000-00-00 00:00:00'),
(4, 'Cars', 8, 0, 1, '0000-00-00 00:00:00'),
(5, 'Audi', 8, 0, 1, '0000-00-00 00:00:00'),
(6, 'Lamborghini', 8, 0, 1, '0000-00-00 00:00:00'),
(7, 'Citroen', 8, 0, 1, '0000-00-00 00:00:00'),
(8, 'American Choppers', 8, 0, 1, '0000-00-00 00:00:00'),
(9, 'American Choppers2', 8, 0, 1, '0000-00-00 00:00:00'),
(10, 'Bugatti', 8, 0, 1, '0000-00-00 00:00:00'),
(11, 'Custom Bike', 8, 0, 1, '0000-00-00 00:00:00'),
(12, 'Ducati', 8, 0, 1, '0000-00-00 00:00:00'),
(13, 'Ducati V2', 8, 0, 1, '0000-00-00 00:00:00'),
(14, 'Harley', 8, 0, 1, '0000-00-00 00:00:00'),
(15, 'My Buddy', 8, 0, 1, '0000-00-00 00:00:00'),
(16, 'My Buddy V2', 8, 0, 1, '0000-00-00 00:00:00'),
(17, 'The Veyron', 8, 0, 1, '0000-00-00 00:00:00'),
(18, 'The Veyron White', 8, 0, 1, '0000-00-00 00:00:00'),
(19, 'The Veyron White V2', 8, 0, 1, '0000-00-00 00:00:00'),
(20, 'Yamaha R1', 8, 0, 1, '0000-00-00 00:00:00'),
(21, 'BMW Bike', 8, 0, 1, '0000-00-00 00:00:00'),
(22, 'Boga Lake', 8, 0, 1, '0000-00-00 00:00:00'),
(23, 'Field', 8, 0, 1, '0000-00-00 00:00:00'),
(24, 'Flower', 8, 0, 1, '0000-00-00 00:00:00'),
(25, 'Hill', 8, 0, 1, '0000-00-00 00:00:00'),
(26, 'House', 8, 0, 1, '0000-00-00 00:00:00'),
(27, 'Nature', 8, 0, 1, '0000-00-00 00:00:00'),
(28, 'Pink Tree', 8, 0, 1, '0000-00-00 00:00:00'),
(29, 'River', 8, 0, 1, '0000-00-00 00:00:00'),
(30, 'Road', 8, 0, 1, '0000-00-00 00:00:00'),
(31, 'Tree', 8, 0, 1, '0000-00-00 00:00:00'),
(32, 'Brazil 2014', 8, 0, 1, '0000-00-00 00:00:00'),
(33, 'England', 8, 0, 1, '0000-00-00 00:00:00'),
(34, 'FIFA World Cup 2014', 8, 0, 1, '0000-00-00 00:00:00'),
(35, 'Football', 8, 0, 1, '0000-00-00 00:00:00'),
(36, 'Garman', 8, 0, 1, '0000-00-00 00:00:00'),
(37, 'Italy', 8, 0, 1, '0000-00-00 00:00:00'),
(38, 'Lion El Messi', 8, 0, 1, '0000-00-00 00:00:00'),
(39, 'Messi', 8, 0, 1, '0000-00-00 00:00:00'),
(40, 'Messi Addias', 8, 0, 1, '0000-00-00 00:00:00'),
(41, 'Samba', 8, 0, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `myvideos`
--

CREATE TABLE IF NOT EXISTS `myvideos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `group_id` int(10) unsigned DEFAULT NULL,
  `playlist_id` int(11) DEFAULT '0',
  `position_order` int(11) DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `sender_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `params` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `alias`, `content`, `permission`, `params`, `created`, `modified`, `menu`, `icon_class`, `weight`) VALUES
(1, 'Terms', 'terms', '<p>These are our terms</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:19:26', '2014-06-10 16:19:26', 1, '', 0),
(2, 'About', 'about', '<p>This is all about us</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:20:19', '2014-06-10 16:20:19', 1, '', 0),
(3, 'Privacy', 'privacy', '<p>This is your privacy</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:21:03', '2014-06-10 16:21:03', 1, '', 0),
(4, 'Online Safety', 'online_safety', '<p>Be safe</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:21:52', '2014-06-10 16:21:52', 1, '', 0),
(5, 'Help', 'help', '<p>Here is some help</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:22:21', '2014-06-10 16:22:21', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pass_settings`
--

CREATE TABLE IF NOT EXISTS `pass_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(50) NOT NULL,
  `value` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_requests`
--

CREATE TABLE IF NOT EXISTS `password_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo_tags`
--

CREATE TABLE IF NOT EXISTS `photo_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `tagger_id` int(10) unsigned NOT NULL,
  `value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `style` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `photo_id` (`photo_id`),
  KEY `tagger_id` (`tagger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `target_id` (`target_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE IF NOT EXISTS `playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `video_count` int(11) NOT NULL DEFAULT '0',
  `cover` varchar(500) DEFAULT NULL,
  `shared_playlist` tinyint(4) NOT NULL DEFAULT '0',
  `on_off` tinyint(4) DEFAULT '0',
  `display_in_profile` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `core` tinyint(1) unsigned NOT NULL,
  `version` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `weight` smallint(5) unsigned NOT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `key`, `permission`, `settings`, `enabled`, `core`, `version`, `weight`, `menu`, `url`, `icon_class`) VALUES
(1, 'People', 'user', '', 'a:0:{}', 1, 1, '2.0', 1, 1, '/users', 'icon-user'),
(2, 'Blogs', 'blog', '', '', 1, 1, '2.0', 2, 1, '/blogs', 'icon-edit'),
(3, 'Photos', 'photo', '', '', 1, 1, '2.0', 3, 1, '/photos', 'icon-picture'),
(4, 'Videos', 'video', '', '', 1, 1, '2.0', 4, 1, '/videos', 'icon-facetime-video'),
(5, 'Topics', 'topic', '', '', 1, 1, '2.0', 5, 1, '/topics', 'icon-comments'),
(6, 'Groups', 'group', '', '', 1, 1, '2.0', 6, 1, '/groups', 'icon-group'),
(7, 'Events', 'event', '', '', 1, 1, '2.0', 7, 1, '/events', 'icon-calendar'),
(8, 'Conversations', 'conversation', '', '', 1, 1, '2.0', 8, 0, '', ''),
(9, 'Pages', 'page', '', '', 1, 1, '2.0', 9, 0, '', ''),
(10, 'Maintenance', 'maintenance', '', '', 1, 0, '1.0', 10, 0, '', ''),
(11, 'Layout Builder', 'layoutbuilder', '', '', 1, 0, '1.0', 11, 1, '', ''),
(12, 'Affections', 'affection', '', 'a:11:{s:13:"luvs_duration";s:1:"3";s:19:"luvs_perday_regular";s:1:"3";s:15:"luvs_perday_vip";s:2:"10";s:21:"hugs_per_user_regular";s:1:"1";s:17:"hugs_per_user_vip";s:1:"3";s:24:"hifives_per_user_regular";s:1:"1";s:20:"hifives_per_user_vip";s:1:"3";s:23:"kisses_per_user_regular";s:1:"1";s:19:"kisses_per_user_vip";s:1:"3";s:22:"winks_per_user_regular";s:1:"1";s:18:"winks_per_user_vip";s:1:"3";}', 1, 0, '1.0', 12, 1, '/affections', '');

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings`
--

CREATE TABLE IF NOT EXISTS `privacy_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `action_title` varchar(100) NOT NULL,
  `action_controller` varchar(200) NOT NULL,
  `privacy_cat_value` int(11) NOT NULL DEFAULT '0',
  `privacy_sub_cat` int(11) NOT NULL DEFAULT '0',
  `comment_privacy` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `privacy_settings`
--

INSERT INTO `privacy_settings` (`id`, `user_id`, `action_title`, `action_controller`, `privacy_cat_value`, `privacy_sub_cat`, `comment_privacy`) VALUES
(1, 0, 'Profile View', 'users_view', 1, 0, 0),
(2, 0, 'Album View', 'albums_view', 1, 0, 0),
(3, 0, 'Blogs View', 'blogs_view', 1, 0, 0),
(4, 0, 'Contact Message', 'conversations_ajax_doSend', 1, 0, 0),
(5, 0, 'Video Comment', 'videos_view', 1, 0, 1),
(6, 0, 'Photo Comment', 'photos_view', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings_categories`
--

CREATE TABLE IF NOT EXISTS `privacy_settings_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `privacy_settings_categories`
--

INSERT INTO `privacy_settings_categories` (`id`, `name`, `value`, `type`) VALUES
(1, 'public', 1, 0),
(2, 'member', 2, 1),
(3, 'me', 3, 0),
(4, 'friend', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile_field_values`
--

CREATE TABLE IF NOT EXISTS `profile_field_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `profile_field_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `profile_field_id` (`profile_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_fields`
--

CREATE TABLE IF NOT EXISTS `profile_fields` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `values` text COLLATE utf8_unicode_ci NOT NULL,
  `required` tinyint(1) unsigned NOT NULL,
  `registration` tinyint(1) unsigned NOT NULL,
  `searchable` tinyint(1) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  `weight` smallint(5) unsigned NOT NULL,
  `profile` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_views`
--

CREATE TABLE IF NOT EXISTS `profile_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `viewer_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ranks`
--

CREATE TABLE IF NOT EXISTS `ranks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `min_posts` mediumint(8) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_super` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `is_admin`, `is_super`, `params`, `core`) VALUES
(1, 'Super Admin', 1, 1, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,group_delete,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1),
(2, 'Member', 0, 0, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1),
(3, 'Guest', 0, 0, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1);

-- --------------------------------------------------------

--
-- Table structure for table `security_questions`
--

CREATE TABLE IF NOT EXISTS `security_questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `security_questions`
--

INSERT INTO `security_questions` (`id`, `question`, `active`) VALUES
(1, 'What was your childhood nickname? ', 1),
(2, 'In what city did you meet your spouse/significant other?', 1),
(3, 'What is the name of your favorite childhood friend? ', 1),
(4, 'What street did you live on in third grade?', 1),
(5, 'What is your oldest sibling''s middle name?', 1),
(6, 'What was the name of your first stuffed animal?', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `field` (`field`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=140 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `field`, `value`) VALUES
(1, 'site_name', 'animesh'),
(2, 'site_email', 'aniemsh.ruet@gmail.com'),
(3, 'site_keywords', ''),
(4, 'site_description', ''),
(5, 'default_feed', 'everyone'),
(6, 'recaptcha', '0'),
(7, 'recaptcha_publickey', ''),
(8, 'recaptcha_privatekey', ''),
(9, 'email_validation', '0'),
(10, 'guest_message', ''),
(11, 'header_code', ''),
(12, 'sidebar_code', ''),
(13, 'footer_code', ''),
(14, 'member_message', ''),
(15, 'select_theme', '1'),
(16, 'guest_search', '1'),
(17, 'default_theme', 'jcoterie'),
(18, 'force_login', '1'),
(19, 'homepage_code', ''),
(20, 'ban_ips', ''),
(21, 'feed_selection', '1'),
(22, 'admin_notes', ''),
(23, 'site_offline', '0'),
(24, 'offline_message', ''),
(25, 'cron_last_run', ''),
(26, 'version', '2.0.2'),
(27, 'profile_privacy', '1'),
(28, 'fb_app_id', ''),
(29, 'fb_app_secret', ''),
(30, 'fb_integration', '0'),
(31, 'popular_interval', '30'),
(32, 'add_this', '1'),
(33, 'default_language', 'eng'),
(34, 'disable_registration', '0'),
(35, 'time_format', '12h'),
(36, 'date_format', 'F j \\a\\t g:ia'),
(37, 'logo', ''),
(38, 'hide_admin_link', '0'),
(39, 'select_language', '1'),
(40, 'analytics_code', ''),
(41, 'registration_notify', '0'),
(42, 'hide_activites', '0'),
(43, 'username_change', '1'),
(44, 'save_original_image', '0'),
(45, 'mail_transport', 'Mail'),
(46, 'smtp_host', 'localhost'),
(47, 'smtp_username', ''),
(48, 'smtp_password', ''),
(49, 'smtp_port', ''),
(50, 'restricted_usernames', ''),
(51, 'enable_registration_code', '0'),
(52, 'registration_code', ''),
(53, 'languages', 'eng=English'),
(54, 'google_dev_key', ''),
(55, 'timezone', 'Africa/Abidjan'),
(56, 'enable_timezone_selection', '1'),
(57, 'require_birthday', '1'),
(58, 'enable_spam_challenge', '0'),
(59, 'show_credit', '0'),
(60, 'name_change', '1'),
(61, 'registration_message', ''),
(62, 'admin_password', ''),
(63, 'recently_joined', '1'),
(64, 'featured_members', '1'),
(65, 'num_new_members', '10'),
(66, 'num_friend_suggestions', '2'),
(67, 'friend_birthdays', '0'),
(68, 'set_autorefresh_interval', ''),
(69, 'enable_security_ques', ''),
(70, 'enable_fan_pages', ''),
(71, 'number_fan_pages', ''),
(72, 'censor_word', ''),
(73, 'users_not_adult', ''),
(74, 'default_subject', ''),
(75, 'default_body', ''),
(76, 'get_request_subject', ''),
(77, 'get_request_body', ''),
(78, 'acceept_request_subject', ''),
(79, 'acceept_request_body', ''),
(80, 'affection_subject', ''),
(81, 'affection_body', ''),
(82, 'update_photo_subject', ''),
(83, 'update_photo_body', ''),
(84, 'update_status_subject', ''),
(85, 'update_status_body', ''),
(86, 'upcoming_birthday_subject', ''),
(87, 'upcoming_birthday_body', ''),
(88, 'new_message_subject', ''),
(89, 'new_message_body', ''),
(90, 'group_invite_subject', ''),
(91, 'group_invite_body', ''),
(92, 'group_topic_subject', ''),
(93, 'group_topic_body', ''),
(94, 'group_post_subject', ''),
(95, 'group_post_body', ''),
(96, 'comment_status_subject', ''),
(97, 'comment_status_body', ''),
(98, 'comment_photo_subject', ''),
(99, 'comment_photo_body', ''),
(100, 'like_status_subject', ''),
(101, 'like_status_body', ''),
(102, 'like_photo_subject', ''),
(103, 'like_photo_body', ''),
(104, 'comment_blog_subject', ''),
(105, 'comment_blog_body', ''),
(106, 'like_blog_subject', ''),
(107, 'like_blog_body', ''),
(108, 'comment_event_subject', ''),
(109, 'comment_event_body', ''),
(110, 'like_event_subject', ''),
(111, 'like_event_body', ''),
(112, 'new_event_subject', ''),
(113, 'new_event_body', ''),
(114, 'profile_view_subject', ''),
(115, 'profile_view_body', ''),
(116, 'group_rank_subject', ''),
(117, 'group_rank_body', ''),
(118, 'alert_reminder_subject', ''),
(119, 'alert_reminder_body', ''),
(120, 'site_newsletter_subject', ''),
(121, 'site_newsletter_body', ''),
(122, 'admin_approval_registration', '0'),
(123, 'event_auto_publish', '0'),
(124, 'blog_auto_publish', '0'),
(125, 'ecards_received_subject', ''),
(126, 'ecards_received_body', ''),
(127, 'waitfor_approval_subject', ''),
(128, 'waitfor_approval_body', ''),
(129, 'approval_req_subject', ''),
(130, 'approval_req_body', ''),
(131, 'account_approved_subject', ''),
(132, 'account_approved_body', ''),
(133, 'override_privacy_settings', '0'),
(134, 'uploaded_video_auto_publish', '0'),
(135, 'uploaded_video_size_limit', '50'),
(136, 'retricted_wordlist', ''),
(137, 'enable_myvideos', '0'),
(138, 'shared_video_subject', ''),
(139, 'shared_video_body', '');

-- --------------------------------------------------------

--
-- Table structure for table `sharedbyme_playlists`
--

CREATE TABLE IF NOT EXISTS `sharedbyme_playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sharedwithme_playlists`
--

CREATE TABLE IF NOT EXISTS `sharedwithme_playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `skins`
--

CREATE TABLE IF NOT EXISTS `skins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `color` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `font_family` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `font_size` int(10) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `spam_challenges`
--

CREATE TABLE IF NOT EXISTS `spam_challenges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `answers` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status_updates`
--

CREATE TABLE IF NOT EXISTS `status_updates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_create` tinyint(4) NOT NULL DEFAULT '1',
  `blog_comment` tinyint(4) NOT NULL DEFAULT '1',
  `comment_add` tinyint(4) NOT NULL DEFAULT '1',
  `event_attend` tinyint(4) NOT NULL DEFAULT '1',
  `event_create` tinyint(4) NOT NULL DEFAULT '1',
  `friend_add` tinyint(4) NOT NULL DEFAULT '1',
  `group_create` tinyint(4) NOT NULL DEFAULT '1',
  `group_join` tinyint(4) NOT NULL DEFAULT '1',
  `like_add` tinyint(4) NOT NULL DEFAULT '1',
  `photos_add` tinyint(4) NOT NULL DEFAULT '1',
  `photos_tag` tinyint(4) NOT NULL DEFAULT '1',
  `topic_create` tinyint(4) NOT NULL DEFAULT '1',
  `video_add` tinyint(4) NOT NULL DEFAULT '1',
  `wall_post` tinyint(4) NOT NULL DEFAULT '1',
  `wall_post_link` tinyint(4) NOT NULL DEFAULT '1',
  `user_create` tinyint(4) NOT NULL DEFAULT '1',
  `user_avatar` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `status_updates`
--

INSERT INTO `status_updates` (`id`, `blog_create`, `blog_comment`, `comment_add`, `event_attend`, `event_create`, `friend_add`, `group_create`, `group_join`, `like_add`, `photos_add`, `photos_tag`, `topic_create`, `video_add`, `wall_post`, `wall_post_link`, `user_create`, `user_avatar`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `tag` (`tag`),
  KEY `target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `key`, `name`, `core`) VALUES
(3, 'mobile', 'Mobile Theme', 1),
(4, 'jcoterie', 'jCoterie', 1);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `last_post` datetime NOT NULL,
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(10) unsigned NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL,
  `pinned` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `attachment` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `announcement` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `topic_category_id` (`category_id`),
  KEY `lastposter_id` (`lastposter_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_group_likes`
--

CREATE TABLE IF NOT EXISTS `user_group_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `num_likes` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_user_like_key` (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_group_posts`
--

CREATE TABLE IF NOT EXISTS `user_group_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `num_posts` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_user_post_key` (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `security_question_id` tinyint(3) NOT NULL,
  `security_answers` text CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `friend_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `notification_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `friend_request_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `blog_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `topic_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `conversation_user_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `video_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `confirm_status` tinyint(3) NOT NULL DEFAULT '0',
  `confirmed` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `notification_email` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `timezone` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `hide_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cover` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `luvs` float NOT NULL DEFAULT '0',
  `profile_views` int(10) unsigned NOT NULL DEFAULT '0',
  `disabled_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_username` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_by_admin` tinyint(4) DEFAULT '0',
  `no_word_restriction` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `gender` (`gender`),
  KEY `active` (`active`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `email`, `password`, `role_id`, `security_question_id`, `security_answers`, `avatar`, `photo`, `created`, `last_login`, `photo_count`, `friend_count`, `notification_count`, `friend_request_count`, `blog_count`, `topic_count`, `conversation_user_count`, `video_count`, `gender`, `birthday`, `active`, `confirm_status`, `confirmed`, `code`, `notification_email`, `timezone`, `ip_address`, `privacy`, `username`, `url`, `about`, `featured`, `lang`, `hide_online`, `cover`, `luvs`, `profile_views`, `disabled_name`, `disabled_username`, `disabled_avatar`, `disabled_photo`, `disabled_cover`, `approved_by_admin`, `no_word_restriction`) VALUES
(1, 'animesh', 'biswas', 'animesh.ruet@gmail.com', '$2y$10$daIf61pxJEPhS3d3p/b.qubITbFkX8mVvGSDGKRDpVlGl5GiwL5dq', 1, 0, '', '', '', '2014-08-05 20:05:26', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, 0, 'Male', '2014-08-05', 1, 0, 1, '04058c1d39d8cc392b7d57e99f71163f', 1, 'Africa/Abidjan', '', 1, '', '', '', 0, '', 0, '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `group_id` int(10) unsigned NOT NULL,
  `playlist_id` int(11) DEFAULT '0',
  `position_order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_positions`
--

CREATE TABLE IF NOT EXISTS `widget_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parentbox` varchar(50) NOT NULL,
  `positions` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_settings`
--

CREATE TABLE IF NOT EXISTS `widget_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '5',
  `photos` int(11) NOT NULL,
  `friends` int(11) NOT NULL DEFAULT '5',
  `videos` int(11) NOT NULL DEFAULT '5',
  `blogs` int(11) NOT NULL DEFAULT '5',
  `groups` int(11) NOT NULL DEFAULT '5',
  `wallone` int(11) NOT NULL DEFAULT '5',
  `status` int(11) NOT NULL DEFAULT '5',
  `ecard` int(11) NOT NULL DEFAULT '5',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_types`
--

CREATE TABLE IF NOT EXISTS `widget_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE IF NOT EXISTS `widgets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `widget_type_id` int(10) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `order` smallint(5) unsigned NOT NULL DEFAULT '1',
  `wallposition` int(11) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `winks`
--

CREATE TABLE IF NOT EXISTS `winks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
--
-- Database: `807`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group` (`group`,`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `group`, `key`, `description`) VALUES
(1, 'user', 'username', 'Select username'),
(2, 'blog', 'view', 'View blog entry'),
(3, 'blog', 'create', 'Create/Edit blog entry'),
(4, 'album', 'create', 'Create/Edit photo album'),
(5, 'album', 'view', 'View photo album'),
(6, 'event', 'create', 'Create/Edit event'),
(7, 'event', 'view', 'View event'),
(8, 'group', 'create', 'Create/Edit group'),
(9, 'group', 'view', 'View group'),
(10, 'photo', 'upload', 'Upload photos'),
(11, 'photo', 'view', 'View photo'),
(12, 'topic', 'create', 'Create/Edit topic'),
(13, 'topic', 'view', 'View topic'),
(14, 'video', 'share', 'Share video'),
(15, 'video', 'view', 'View video'),
(16, 'attachment', 'upload', 'Upload attachment'),
(17, 'attachment', 'download', 'Download attachment');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `action` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `item_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `activity_comment_count` smallint(5) unsigned NOT NULL,
  `query` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `guest_home` (`type`,`privacy`,`modified`),
  KEY `profile` (`target_id`,`type`,`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `activity_comments`
--

CREATE TABLE IF NOT EXISTS `activity_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `activity_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `wall_id` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_notifications`
--

CREATE TABLE IF NOT EXISTS `admin_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modified` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plugin_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `original_filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `downloads` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plugin_id` (`plugin_id`,`target_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blocked_members`
--

CREATE TABLE IF NOT EXISTS `blocked_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `category_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cake_sessions`
--

CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `weight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `header` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `create_permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `type` (`type`,`active`,`weight`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `type`, `parent_id`, `name`, `description`, `item_count`, `active`, `weight`, `header`, `create_permission`) VALUES
(1, 'album', 0, 'Member Albums', '', 0, 1, 0, 0, ''),
(2, 'group', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(3, 'topic', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(4, 'video', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(5, 'event', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(6, 'friend', 0, 'Friends', '', 0, 1, 0, 0, ''),
(7, 'blog', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(8, 'skin', 0, 'All Categories', '', 0, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `censor_words`
--

CREATE TABLE IF NOT EXISTS `censor_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `emails_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `words_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `replacement` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `censor_words`
--

INSERT INTO `censor_words` (`id`, `users_list`, `emails_list`, `words_list`, `replacement`) VALUES
(1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `circles`
--

CREATE TABLE IF NOT EXISTS `circles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `type` smallint(5) NOT NULL DEFAULT '1',
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type` (`type`,`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_groups`
--

CREATE TABLE IF NOT EXISTS `conversation_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(10) unsigned NOT NULL,
  `group_users` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conversation_id` (`conversation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_users`
--

CREATE TABLE IF NOT EXISTS `conversation_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `unread` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conversation_id` (`conversation_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE IF NOT EXISTS `conversations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `message_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `lastposter_id` (`lastposter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecard_photos`
--

CREATE TABLE IF NOT EXISTS `ecard_photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecard_id` int(10) unsigned NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ecard_id` (`ecard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecard_senders`
--

CREATE TABLE IF NOT EXISTS `ecard_senders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecard_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `receiver_id` int(10) unsigned NOT NULL,
  `text` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecards`
--

CREATE TABLE IF NOT EXISTS `ecards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `text` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_notifications_settings`
--

CREATE TABLE IF NOT EXISTS `email_notifications_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `get_request` tinyint(4) NOT NULL DEFAULT '1',
  `acceept_request` tinyint(4) NOT NULL DEFAULT '1',
  `luvs` tinyint(4) NOT NULL DEFAULT '1',
  `hifives` tinyint(4) NOT NULL DEFAULT '1',
  `hugs` tinyint(4) NOT NULL DEFAULT '1',
  `kisses` tinyint(4) NOT NULL DEFAULT '1',
  `winks` tinyint(4) NOT NULL DEFAULT '1',
  `update_photo` tinyint(4) NOT NULL DEFAULT '1',
  `update_status` tinyint(4) NOT NULL DEFAULT '1',
  `upcoming_birthday` tinyint(4) NOT NULL DEFAULT '1',
  `new_message` tinyint(4) NOT NULL DEFAULT '1',
  `group_invite` tinyint(4) NOT NULL DEFAULT '1',
  `group_topic` tinyint(4) NOT NULL DEFAULT '1',
  `group_post` tinyint(4) NOT NULL DEFAULT '1',
  `comment_status` tinyint(4) NOT NULL DEFAULT '1',
  `comment_photo` tinyint(4) NOT NULL DEFAULT '1',
  `like_status` tinyint(4) NOT NULL DEFAULT '1',
  `like_photo` tinyint(4) NOT NULL DEFAULT '1',
  `comment_blog` tinyint(4) NOT NULL DEFAULT '1',
  `like_blog` tinyint(4) NOT NULL DEFAULT '1',
  `comment_event` tinyint(4) NOT NULL DEFAULT '1',
  `like_event` tinyint(4) NOT NULL DEFAULT '1',
  `new_event` tinyint(4) NOT NULL DEFAULT '1',
  `profile_view` tinyint(4) NOT NULL DEFAULT '1',
  `group_rank` tinyint(4) NOT NULL DEFAULT '1',
  `alert_reminder` tinyint(4) NOT NULL DEFAULT '1',
  `site_newsletter` tinyint(4) NOT NULL DEFAULT '1',
  `ecards_received` tinyint(4) NOT NULL DEFAULT '1',
  `shared_video` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_rsvps`
--

CREATE TABLE IF NOT EXISTS `event_rsvps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `rsvp` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rsvp_list` (`event_id`,`user_id`),
  KEY `event_id` (`event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from` date NOT NULL,
  `from_time` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `to` date NOT NULL,
  `to_time` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `event_rsvp_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `address` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events_category_settings`
--

CREATE TABLE IF NOT EXISTS `events_category_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `location` tinyint(4) NOT NULL DEFAULT '1',
  `address` tinyint(4) NOT NULL DEFAULT '1',
  `cat_description` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friend_requests`
--

CREATE TABLE IF NOT EXISTS `friend_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_accounts`
--

CREATE TABLE IF NOT EXISTS `gold_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `virtual_cash` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_discounts`
--

CREATE TABLE IF NOT EXISTS `gold_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bundle_id` int(11) NOT NULL,
  `bundle_name` varchar(200) NOT NULL,
  `discount_start_date` datetime NOT NULL,
  `discount_end_date` datetime NOT NULL,
  `discount_rate` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_payment_histories`
--

CREATE TABLE IF NOT EXISTS `gold_payment_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `virtual_cash` int(11) NOT NULL,
  `real_money` decimal(10,2) NOT NULL,
  `reason` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goldbundles`
--

CREATE TABLE IF NOT EXISTS `goldbundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `gold_coins` int(11) NOT NULL,
  `real_money` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goldsettings`
--

CREATE TABLE IF NOT EXISTS `goldsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `options` varchar(200) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `goldsettings`
--

INSERT INTO `goldsettings` (`id`, `options`, `value`) VALUES
(1, 'paypal_email', ''),
(2, 'live_or_sandbox_paypal', '1'),
(3, 'gold_conversion_rate', '2'),
(4, 'give_to_friend', '1'),
(5, 'friend_gift_cost', '10'),
(6, 'admin__gold_account', '');

-- --------------------------------------------------------

--
-- Table structure for table `group_users`
--

CREATE TABLE IF NOT EXISTS `group_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` tinyint(2) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(2) unsigned NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_user_count` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `topic_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `video_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `event_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `category_id`, `name`, `group_url`, `user_id`, `description`, `type`, `photo`, `group_user_count`, `created`, `photo_count`, `topic_count`, `video_count`, `event_count`, `comment_count`, `featured`) VALUES
(1, 3, 'Community', 'Community', 0, 'This group is common for all community topics', 1, '', 1, '2014-08-05 20:02:28', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hifives`
--

CREATE TABLE IF NOT EXISTS `hifives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hooks`
--

CREATE TABLE IF NOT EXISTS `hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `weight` smallint(6) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `version` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `controller` (`controller`),
  KEY `action` (`action`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `hooks`
--

INSERT INTO `hooks` (`id`, `name`, `key`, `controller`, `action`, `position`, `weight`, `enabled`, `version`, `settings`, `permission`) VALUES
(1, 'Featured Members', 'featured_members', 'home', 'index', 'home_sidebar', 6, 1, '2.0', 'a:0:{}', ''),
(2, 'Popular Blogs', 'popular_blogs', 'blogs', 'index', 'blogs_sidebar', 2, 1, '2.0', 'a:1:{s:9:"num_blogs";s:1:"5";}', ''),
(3, 'Popular Albums', 'popular_albums', 'photos', 'index', 'photos_sidebar', 3, 1, '2.0', 'a:1:{s:10:"num_albums";s:1:"5";}', ''),
(4, 'Today Birthdays', 'today_birthdays', 'home', 'index', 'home_sidebar', 4, 1, '2.0', 'a:1:{s:16:"friend_birthdays";s:1:"1";}', ''),
(5, 'Online Users', 'online_users', '', '', 'global_sidebar', 5, 1, '2.0', 'a:1:{s:16:"num_online_users";s:2:"12";}', ''),
(6, 'Recently Joined', 'recently_joined', 'home', 'index', 'home_sidebar', 7, 1, '2.0', 'a:1:{s:15:"num_new_members";s:2:"10";}', ''),
(7, 'Popular Events', 'popular_events', 'events', 'index', 'events_sidebar', 8, 1, '2.0', 'a:1:{s:10:"num_events";s:1:"5";}', ''),
(8, 'Popular Groups', 'popular_groups', 'groups', 'index', 'groups_sidebar', 9, 1, '2.0', 'a:1:{s:10:"num_groups";s:1:"5";}', ''),
(9, 'Popular Topics', 'popular_topics', 'topics', 'index', 'topics_sidebar', 10, 1, '2.0', 'a:1:{s:10:"num_topics";s:1:"5";}', ''),
(10, 'Popular Videos', 'popular_videos', 'videos', 'index', 'videos_sidebar', 11, 1, '2.0', 'a:1:{s:10:"num_videos";s:1:"5";}', ''),
(11, 'Friend Suggestions', 'friend_suggestions', '', '', 'global_sidebar', 12, 1, '2.0', 'a:1:{s:22:"num_friend_suggestions";s:1:"2";}', ''),
(12, 'Featured Groups', 'featured_groups', 'groups', 'index', 'groups_sidebar', 13, 1, '2.0', 'a:0:{}', ''),
(13, 'Profile Luv', 'profile_luvs', '', '', 'profile_sidebar', 14, 1, '1.0', '', ''),
(14, 'Profile Affections', 'profile_affections', '', '', 'profile_sidebar', 15, 1, '1.0', '', ''),
(15, 'Notifications', 'notifications', '', '', 'profile_sidebar', 16, 1, '1.0', 'a:1:{s:17:"num_notifications";s:2:"10";}', ''),
(16, 'Online Friends', 'online_friends', '', '', 'profile_sidebar', 17, 1, '1.0', 'a:1:{s:11:"num_friends";s:2:"10";}', '');

-- --------------------------------------------------------

--
-- Table structure for table `hugs`
--

CREATE TABLE IF NOT EXISTS `hugs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kisses`
--

CREATE TABLE IF NOT EXISTS `kisses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `key`, `name`) VALUES
(1, 'eng', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `layouts`
--

CREATE TABLE IF NOT EXISTS `layouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout` text COLLATE utf8_unicode_ci,
  `modified` datetime NOT NULL,
  `has_header` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `has_footer` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

--
-- Dumping data for table `layouts`
--

INSERT INTO `layouts` (`id`, `controller`, `task`, `layout`, `modified`, `has_header`, `has_footer`) VALUES
(1, 'Groups', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/group_tabs.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/view.ctp"}]}]}]}', '2014-05-14 14:46:00', 1, 1),
(2, 'Groups', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"groups/leftcol_list.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"groups/list.ctp"}]}]}]}', '2014-02-22 16:53:36', 1, 1),
(3, 'Affections', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"hook","label":"Hook","title":"","divid":"jvleftnav","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"affections/index.ctp"}]}]}]}', '2014-02-11 15:46:53', 1, 1),
(4, 'Pages', 'display', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"pages/index.ctp"}]}]}]}', '2014-02-11 20:22:40', 1, 1),
(5, 'Home', 'contact', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/contact.ctp"}]}]}]}', '2014-02-22 14:28:32', 1, 1),
(6, 'Home', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/header.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/index.ctp"}]}]}]}', '2014-06-24 06:54:05', 1, 1),
(7, 'Conversations', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"conversations/view.ctp"}]}]}]}', '2014-05-09 14:17:46', 0, 0),
(8, 'Users', 'register', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"jvLogin","contents":[{"type":"span8","label":"Column - span8","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/register-bg.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/registration.ctp"}]}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_three.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_four.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_five.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_six.ctp"}]}]}]}', '2014-06-24 06:48:56', 1, 1),
(9, 'Users', 'password', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/password.ctp"}]}]}]}', '2014-06-12 16:25:45', 1, 1),
(10, 'Users', 'recover', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/recover.ctp"}]}]}]}', '2014-02-23 13:53:59', 1, 1),
(11, 'Users', 'resetpass', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/resetpass.ctp"}]}]}]}', '2014-02-23 13:55:12', 1, 1),
(12, 'Users', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/view_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/view_leftcol.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"users/view_avatar.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"profile_luvs","name":"Profile Luv"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"profile_affections","name":"Profile Affections"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/view_center.ctp"}]}]}]}', '2014-06-10 18:05:31', 1, 1),
(13, 'Users', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/index_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"users/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/index_center.ctp"}]}]}]}', '2014-02-23 13:53:05', 1, 1),
(14, 'Videos', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/index.ctp"}]}]}]}', '2014-02-23 13:56:08', 1, 1),
(15, 'Videos', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"videos/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/view.ctp"}]}]}]}', '2014-02-23 13:56:19', 1, 1),
(16, 'Blogs', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/index.ctp"}]}]}]}', '2014-02-11 20:14:07', 1, 1),
(17, 'Blogs', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span7","label":"Column - span7","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/view.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-02-11 21:45:29', 1, 1),
(18, 'Blogs', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/create.ctp"}]}]}]}', '2014-02-11 20:13:37', 1, 1),
(19, 'Events', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"events/index.ctp"}]}]}]}', '2014-02-11 20:36:07', 1, 1),
(20, 'Events', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"events/view_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/view.ctp"}]}]}]}', '2014-02-11 20:35:37', 1, 1),
(21, 'Events', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/create.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-05-01 15:46:46', 1, 1),
(22, 'Tags', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"tags/view_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"tags/view.ctp"}]}]}]}', '2014-02-11 20:16:13', 1, 1),
(23, 'Search', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"search/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"search/index.ctp"}]}]}]}', '2014-02-11 20:16:41', 1, 1),
(24, 'Photos', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"jvleftnav","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index.ctp"}]}]}]}', '2014-02-22 16:54:25', 1, 1),
(25, 'Photos', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/view.ctp"}]}]}]}', '2014-02-11 20:21:13', 1, 1),
(26, 'Photos', 'upload', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/upload.ctp"}]}]}]}', '2014-02-11 20:22:14', 1, 1),
(27, 'Albums', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"hook","label":"Hook","title":"","divid":"leftnav","divclass":"","key":"popular_albums","name":"Popular Albums"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"albums/view.ctp"}]}]}]}', '2014-02-11 15:48:07', 1, 1),
(28, 'Albums', 'edit', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"albums/edit.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-02-11 15:47:34', 1, 1),
(29, 'Friends', 'my', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/users/ajax_browse/home"}]}]}]}', '2014-03-02 18:41:19', 1, 1),
(30, 'Friends', 'invite', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/friends/ajax_invite"}]}]}]}', '2014-02-22 14:27:25', 1, 1),
(31, 'Friends', 'requests', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/requests.ctp"}]}]}]}', '2014-02-22 14:27:37', 1, 1),
(32, 'Notifications', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/notifications/ajax_show/home"}]}]}]}', '2014-02-22 16:54:04', 1, 1),
(33, 'Conversations', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/conversations/ajax_browse"}]}]}]}', '2014-02-23 21:35:11', 1, 1),
(34, 'Users', 'visitors', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/visitors.ctp"}]}]}]}', '2014-02-23 13:55:32', 1, 1),
(35, 'Friends', 'favourites', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/users/ajax_browse/favourites"}]}]}]}', '2014-02-22 14:27:04', 1, 1),
(36, 'Users', 'blocked_users', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/blocked_users.ctp"}]}]}]}', '2014-06-12 16:26:56', 1, 1),
(37, 'Friends', 'birthdays', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/friends/ajax_browse/birthdays"}]}]}]}', '2014-04-24 01:05:25', 1, 1),
(38, 'Users', 'verifyanswer', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"users/verifyanswer.ctp"}]}]}]}', '2014-04-24 19:37:25', 1, 1),
(39, 'Groups', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/create.ctp"}]}]}]}', '2014-05-15 21:31:26', 1, 1),
(40, 'Events', 'all_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/all_events.ctp"}]}]}]}', '2014-05-05 16:48:59', 1, 1),
(41, 'Events', 'categorized_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/categorized_events.ctp"}]}]}]}', '2014-05-05 17:05:48', 1, 1),
(42, 'Events', 'friends_attend_event', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/friends_attend_event.ctp"}]}]}]}', '2014-05-06 16:40:42', 1, 1),
(43, 'Events', 'past_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/past_events.ctp"}]}]}]}]}', '2014-05-06 16:44:01', 1, 1),
(44, 'Events', 'my_upcoming_event', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/my_upcoming_event.ctp"}]}]}]}', '2014-05-06 16:48:05', 1, 1),
(45, 'Blogs', 'my_blogs', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/my_blogs.ctp"}]}]}]}', '2014-05-06 17:54:17', 1, 1),
(46, 'Blogs', 'friends_blogs', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/friends_blogs.ctp"}]}]}]}', '2014-05-06 18:04:37', 1, 1),
(47, 'Videos', 'my_videos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"videos/my_videos.ctp"}]}]}]}', '2014-05-09 15:25:05', 1, 1),
(48, 'Videos', 'friends_videos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/friends_videos.ctp"}]}]}]}', '2014-05-07 17:00:05', 1, 1),
(49, 'Users', 'setnotifications', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/setnotifications.ctp"}]}]}]}', '2014-06-12 16:26:16', 1, 1),
(50, 'Albums', 'my_albums', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"jvcenter","divclass":"","path":"albums/my_albums.ctp"}]}]}]}', '2014-05-07 16:54:17', 1, 1),
(51, 'Albums', 'friends_albums', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"albums/friends_albums.ctp"}]}]}]}', '2014-05-07 16:57:00', 1, 1),
(52, 'Conversations', 'my_conversation', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/my_conversation.ctp"}]}]}]}', '2014-05-16 19:35:06', 1, 1),
(53, 'Friends', 'my_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/my_friends.ctp"}]}]}]}', '2014-05-09 13:36:47', 1, 1),
(54, 'Friends', 'favourite_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/favourite_friends.ctp"}]}]}]}', '2014-05-09 13:32:00', 1, 1),
(55, 'Users', 'profile', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/profile.ctp"}]}]}]}', '2014-06-12 16:24:52', 1, 1);
INSERT INTO `layouts` (`id`, `controller`, `task`, `layout`, `modified`, `has_header`, `has_footer`) VALUES
(56, 'Users', 'my_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/index_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"users/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/my_friends.ctp"}]}]}]}', '2014-05-22 13:53:05', 1, 1),
(57, 'Friends', 'requests_received', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/requests_received.ctp"}]}]}]}', '2014-05-09 13:36:47', 1, 1),
(58, 'Home', 'activities', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/header.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"notifications","name":"Notifications"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/activities.ctp"}]}]}]}', '2014-02-22 14:49:50', 1, 1),
(59, 'Topics', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/index.ctp"}]}]}]}', '2014-06-10 18:51:48', 1, 1),
(60, 'Topics', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/view.ctp"}]}]}]}', '2014-06-10 18:49:06', 1, 1),
(61, 'Topics', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"topics/create.ctp"}]}]}]}', '2014-06-10 18:55:25', 1, 1),
(62, 'Videos', 'videosearch', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"videos/videosearch.ctp"}]}]}]}', '2014-06-18 15:14:15', 1, 1),
(63, 'Playlists', 'allplaylists', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"playlists/allplaylists.ctp"}]}]}]}', '2014-06-18 15:17:45', 1, 1),
(64, 'Playlists', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"playlists/view.ctp"}]}]}]}', '2014-06-18 15:21:49', 1, 1),
(65, 'Ecards', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"ecards/index.ctp"}]}]}]}', '2014-06-23 14:19:01', 1, 1),
(66, 'Ecards', 'my_ecards', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/my_ecards.ctp"}]}]}]}', '2014-06-23 14:20:50', 1, 1),
(67, 'Ecards', 'send_wigets', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/send_wigets.ctp"}]}]}]}', '2014-06-23 14:22:35', 1, 1),
(68, 'Ecards', 'rcv_ecards', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/rcv_ecards.ctp"}]}]}]}', '2014-06-10 18:57:25', 1, 1),
(69, 'Videos', 'storedvideos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/storedvideos.ctp"}]}]}]}', '2014-07-10 15:40:57', 1, 1),
(70, 'Goldbundles', 'buygold', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/buygold.ctp"}]}]}', '2014-07-15 13:15:15', 1, 1),
(71, 'Goldbundles', 'buygoldhistory', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/buygoldhistory.ctp"}]}]}]}', '2014-07-15 13:17:13', 1, 1),
(72, 'Goldbundles', 'friend_gift', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/friend_gift.ctp"}]}]}]}', '2014-07-15 13:19:36', 1, 1),
(73, 'Playlists', 'sharedvideos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"playlists/sharedvideos.ctp"}]}]}]}', '2014-08-01 11:19:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_up` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type` (`type`,`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_infos`
--

CREATE TABLE IF NOT EXISTS `login_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created` date NOT NULL,
  `count` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `luvs`
--

CREATE TABLE IF NOT EXISTS `luvs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mooskin_users`
--

CREATE TABLE IF NOT EXISTS `mooskin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mooskin_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mooskin_id` (`mooskin_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mooskins`
--

CREATE TABLE IF NOT EXISTS `mooskins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `total_used` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `mooskins`
--

INSERT INTO `mooskins` (`id`, `name`, `category_id`, `total_used`, `status`, `created`) VALUES
(1, 'default', 8, 0, 1, '0000-00-00 00:00:00'),
(2, 'First Skin', 8, 0, 1, '0000-00-00 00:00:00'),
(3, 'Second Skin', 8, 0, 1, '0000-00-00 00:00:00'),
(4, 'Cars', 8, 0, 1, '0000-00-00 00:00:00'),
(5, 'Audi', 8, 0, 1, '0000-00-00 00:00:00'),
(6, 'Lamborghini', 8, 0, 1, '0000-00-00 00:00:00'),
(7, 'Citroen', 8, 0, 1, '0000-00-00 00:00:00'),
(8, 'American Choppers', 8, 0, 1, '0000-00-00 00:00:00'),
(9, 'American Choppers2', 8, 0, 1, '0000-00-00 00:00:00'),
(10, 'Bugatti', 8, 0, 1, '0000-00-00 00:00:00'),
(11, 'Custom Bike', 8, 0, 1, '0000-00-00 00:00:00'),
(12, 'Ducati', 8, 0, 1, '0000-00-00 00:00:00'),
(13, 'Ducati V2', 8, 0, 1, '0000-00-00 00:00:00'),
(14, 'Harley', 8, 0, 1, '0000-00-00 00:00:00'),
(15, 'My Buddy', 8, 0, 1, '0000-00-00 00:00:00'),
(16, 'My Buddy V2', 8, 0, 1, '0000-00-00 00:00:00'),
(17, 'The Veyron', 8, 0, 1, '0000-00-00 00:00:00'),
(18, 'The Veyron White', 8, 0, 1, '0000-00-00 00:00:00'),
(19, 'The Veyron White V2', 8, 0, 1, '0000-00-00 00:00:00'),
(20, 'Yamaha R1', 8, 0, 1, '0000-00-00 00:00:00'),
(21, 'BMW Bike', 8, 0, 1, '0000-00-00 00:00:00'),
(22, 'Boga Lake', 8, 0, 1, '0000-00-00 00:00:00'),
(23, 'Field', 8, 0, 1, '0000-00-00 00:00:00'),
(24, 'Flower', 8, 0, 1, '0000-00-00 00:00:00'),
(25, 'Hill', 8, 0, 1, '0000-00-00 00:00:00'),
(26, 'House', 8, 0, 1, '0000-00-00 00:00:00'),
(27, 'Nature', 8, 0, 1, '0000-00-00 00:00:00'),
(28, 'Pink Tree', 8, 0, 1, '0000-00-00 00:00:00'),
(29, 'River', 8, 0, 1, '0000-00-00 00:00:00'),
(30, 'Road', 8, 0, 1, '0000-00-00 00:00:00'),
(31, 'Tree', 8, 0, 1, '0000-00-00 00:00:00'),
(32, 'Brazil 2014', 8, 0, 1, '0000-00-00 00:00:00'),
(33, 'England', 8, 0, 1, '0000-00-00 00:00:00'),
(34, 'FIFA World Cup 2014', 8, 0, 1, '0000-00-00 00:00:00'),
(35, 'Football', 8, 0, 1, '0000-00-00 00:00:00'),
(36, 'Garman', 8, 0, 1, '0000-00-00 00:00:00'),
(37, 'Italy', 8, 0, 1, '0000-00-00 00:00:00'),
(38, 'Lion El Messi', 8, 0, 1, '0000-00-00 00:00:00'),
(39, 'Messi', 8, 0, 1, '0000-00-00 00:00:00'),
(40, 'Messi Addias', 8, 0, 1, '0000-00-00 00:00:00'),
(41, 'Samba', 8, 0, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `myvideos`
--

CREATE TABLE IF NOT EXISTS `myvideos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `group_id` int(10) unsigned DEFAULT NULL,
  `playlist_id` int(11) DEFAULT '0',
  `position_order` int(11) DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `sender_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `params` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `alias`, `content`, `permission`, `params`, `created`, `modified`, `menu`, `icon_class`, `weight`) VALUES
(1, 'Terms', 'terms', '<p>These are our terms</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:19:26', '2014-06-10 16:19:26', 1, '', 0),
(2, 'About', 'about', '<p>This is all about us</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:20:19', '2014-06-10 16:20:19', 1, '', 0),
(3, 'Privacy', 'privacy', '<p>This is your privacy</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:21:03', '2014-06-10 16:21:03', 1, '', 0),
(4, 'Online Safety', 'online_safety', '<p>Be safe</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:21:52', '2014-06-10 16:21:52', 1, '', 0),
(5, 'Help', 'help', '<p>Here is some help</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:22:21', '2014-06-10 16:22:21', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pass_settings`
--

CREATE TABLE IF NOT EXISTS `pass_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(50) NOT NULL,
  `value` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_requests`
--

CREATE TABLE IF NOT EXISTS `password_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo_tags`
--

CREATE TABLE IF NOT EXISTS `photo_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `tagger_id` int(10) unsigned NOT NULL,
  `value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `style` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `photo_id` (`photo_id`),
  KEY `tagger_id` (`tagger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `target_id` (`target_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE IF NOT EXISTS `playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `video_count` int(11) NOT NULL DEFAULT '0',
  `cover` varchar(500) DEFAULT NULL,
  `shared_playlist` tinyint(4) NOT NULL DEFAULT '0',
  `on_off` tinyint(4) DEFAULT '0',
  `display_in_profile` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `core` tinyint(1) unsigned NOT NULL,
  `version` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `weight` smallint(5) unsigned NOT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `key`, `permission`, `settings`, `enabled`, `core`, `version`, `weight`, `menu`, `url`, `icon_class`) VALUES
(1, 'People', 'user', '', 'a:0:{}', 1, 1, '2.0', 1, 1, '/users', 'icon-user'),
(2, 'Blogs', 'blog', '', '', 1, 1, '2.0', 2, 1, '/blogs', 'icon-edit'),
(3, 'Photos', 'photo', '', '', 1, 1, '2.0', 3, 1, '/photos', 'icon-picture'),
(4, 'Videos', 'video', '', '', 1, 1, '2.0', 4, 1, '/videos', 'icon-facetime-video'),
(5, 'Topics', 'topic', '', '', 1, 1, '2.0', 5, 1, '/topics', 'icon-comments'),
(6, 'Groups', 'group', '', '', 1, 1, '2.0', 6, 1, '/groups', 'icon-group'),
(7, 'Events', 'event', '', '', 1, 1, '2.0', 7, 1, '/events', 'icon-calendar'),
(8, 'Conversations', 'conversation', '', '', 1, 1, '2.0', 8, 0, '', ''),
(9, 'Pages', 'page', '', '', 1, 1, '2.0', 9, 0, '', ''),
(10, 'Maintenance', 'maintenance', '', '', 1, 0, '1.0', 10, 0, '', ''),
(11, 'Layout Builder', 'layoutbuilder', '', '', 1, 0, '1.0', 11, 1, '', ''),
(12, 'Affections', 'affection', '', 'a:11:{s:13:"luvs_duration";s:1:"3";s:19:"luvs_perday_regular";s:1:"3";s:15:"luvs_perday_vip";s:2:"10";s:21:"hugs_per_user_regular";s:1:"1";s:17:"hugs_per_user_vip";s:1:"3";s:24:"hifives_per_user_regular";s:1:"1";s:20:"hifives_per_user_vip";s:1:"3";s:23:"kisses_per_user_regular";s:1:"1";s:19:"kisses_per_user_vip";s:1:"3";s:22:"winks_per_user_regular";s:1:"1";s:18:"winks_per_user_vip";s:1:"3";}', 1, 0, '1.0', 12, 1, '/affections', '');

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings`
--

CREATE TABLE IF NOT EXISTS `privacy_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `action_title` varchar(100) NOT NULL,
  `action_controller` varchar(200) NOT NULL,
  `privacy_cat_value` int(11) NOT NULL DEFAULT '0',
  `privacy_sub_cat` int(11) NOT NULL DEFAULT '0',
  `comment_privacy` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `privacy_settings`
--

INSERT INTO `privacy_settings` (`id`, `user_id`, `action_title`, `action_controller`, `privacy_cat_value`, `privacy_sub_cat`, `comment_privacy`) VALUES
(1, 0, 'Profile View', 'users_view', 1, 0, 0),
(2, 0, 'Album View', 'albums_view', 1, 0, 0),
(3, 0, 'Blogs View', 'blogs_view', 1, 0, 0),
(4, 0, 'Contact Message', 'conversations_ajax_doSend', 1, 0, 0),
(5, 0, 'Video Comment', 'videos_view', 1, 0, 1),
(6, 0, 'Photo Comment', 'photos_view', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings_categories`
--

CREATE TABLE IF NOT EXISTS `privacy_settings_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `privacy_settings_categories`
--

INSERT INTO `privacy_settings_categories` (`id`, `name`, `value`, `type`) VALUES
(1, 'public', 1, 0),
(2, 'member', 2, 1),
(3, 'me', 3, 0),
(4, 'friend', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile_field_values`
--

CREATE TABLE IF NOT EXISTS `profile_field_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `profile_field_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `profile_field_id` (`profile_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_fields`
--

CREATE TABLE IF NOT EXISTS `profile_fields` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `values` text COLLATE utf8_unicode_ci NOT NULL,
  `required` tinyint(1) unsigned NOT NULL,
  `registration` tinyint(1) unsigned NOT NULL,
  `searchable` tinyint(1) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  `weight` smallint(5) unsigned NOT NULL,
  `profile` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_views`
--

CREATE TABLE IF NOT EXISTS `profile_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `viewer_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ranks`
--

CREATE TABLE IF NOT EXISTS `ranks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `min_posts` mediumint(8) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_super` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `is_admin`, `is_super`, `params`, `core`) VALUES
(1, 'Super Admin', 1, 1, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,group_delete,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1),
(2, 'Member', 0, 0, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1),
(3, 'Guest', 0, 0, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1);

-- --------------------------------------------------------

--
-- Table structure for table `security_questions`
--

CREATE TABLE IF NOT EXISTS `security_questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `security_questions`
--

INSERT INTO `security_questions` (`id`, `question`, `active`) VALUES
(1, 'What was your childhood nickname? ', 1),
(2, 'In what city did you meet your spouse/significant other?', 1),
(3, 'What is the name of your favorite childhood friend? ', 1),
(4, 'What street did you live on in third grade?', 1),
(5, 'What is your oldest sibling''s middle name?', 1),
(6, 'What was the name of your first stuffed animal?', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `field` (`field`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=140 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `field`, `value`) VALUES
(1, 'site_name', 'animesh'),
(2, 'site_email', 'animesh.ruet@gmail.com'),
(3, 'site_keywords', ''),
(4, 'site_description', ''),
(5, 'default_feed', 'everyone'),
(6, 'recaptcha', '0'),
(7, 'recaptcha_publickey', ''),
(8, 'recaptcha_privatekey', ''),
(9, 'email_validation', '0'),
(10, 'guest_message', ''),
(11, 'header_code', ''),
(12, 'sidebar_code', ''),
(13, 'footer_code', ''),
(14, 'member_message', ''),
(15, 'select_theme', '1'),
(16, 'guest_search', '1'),
(17, 'default_theme', 'jcoterie'),
(18, 'force_login', '1'),
(19, 'homepage_code', ''),
(20, 'ban_ips', ''),
(21, 'feed_selection', '1'),
(22, 'admin_notes', ''),
(23, 'site_offline', '0'),
(24, 'offline_message', ''),
(25, 'cron_last_run', ''),
(26, 'version', '2.0.2'),
(27, 'profile_privacy', '1'),
(28, 'fb_app_id', ''),
(29, 'fb_app_secret', ''),
(30, 'fb_integration', '0'),
(31, 'popular_interval', '30'),
(32, 'add_this', '1'),
(33, 'default_language', 'eng'),
(34, 'disable_registration', '0'),
(35, 'time_format', '12h'),
(36, 'date_format', 'F j \\a\\t g:ia'),
(37, 'logo', ''),
(38, 'hide_admin_link', '0'),
(39, 'select_language', '1'),
(40, 'analytics_code', ''),
(41, 'registration_notify', '0'),
(42, 'hide_activites', '0'),
(43, 'username_change', '1'),
(44, 'save_original_image', '0'),
(45, 'mail_transport', 'Mail'),
(46, 'smtp_host', 'localhost'),
(47, 'smtp_username', ''),
(48, 'smtp_password', ''),
(49, 'smtp_port', ''),
(50, 'restricted_usernames', ''),
(51, 'enable_registration_code', '0'),
(52, 'registration_code', ''),
(53, 'languages', 'eng=English'),
(54, 'google_dev_key', ''),
(55, 'timezone', 'Africa/Abidjan'),
(56, 'enable_timezone_selection', '1'),
(57, 'require_birthday', '1'),
(58, 'enable_spam_challenge', '0'),
(59, 'show_credit', '0'),
(60, 'name_change', '1'),
(61, 'registration_message', ''),
(62, 'admin_password', ''),
(63, 'recently_joined', '1'),
(64, 'featured_members', '1'),
(65, 'num_new_members', '10'),
(66, 'num_friend_suggestions', '2'),
(67, 'friend_birthdays', '0'),
(68, 'set_autorefresh_interval', ''),
(69, 'enable_security_ques', ''),
(70, 'enable_fan_pages', ''),
(71, 'number_fan_pages', ''),
(72, 'censor_word', ''),
(73, 'users_not_adult', ''),
(74, 'default_subject', ''),
(75, 'default_body', ''),
(76, 'get_request_subject', ''),
(77, 'get_request_body', ''),
(78, 'acceept_request_subject', ''),
(79, 'acceept_request_body', ''),
(80, 'affection_subject', ''),
(81, 'affection_body', ''),
(82, 'update_photo_subject', ''),
(83, 'update_photo_body', ''),
(84, 'update_status_subject', ''),
(85, 'update_status_body', ''),
(86, 'upcoming_birthday_subject', ''),
(87, 'upcoming_birthday_body', ''),
(88, 'new_message_subject', ''),
(89, 'new_message_body', ''),
(90, 'group_invite_subject', ''),
(91, 'group_invite_body', ''),
(92, 'group_topic_subject', ''),
(93, 'group_topic_body', ''),
(94, 'group_post_subject', ''),
(95, 'group_post_body', ''),
(96, 'comment_status_subject', ''),
(97, 'comment_status_body', ''),
(98, 'comment_photo_subject', ''),
(99, 'comment_photo_body', ''),
(100, 'like_status_subject', ''),
(101, 'like_status_body', ''),
(102, 'like_photo_subject', ''),
(103, 'like_photo_body', ''),
(104, 'comment_blog_subject', ''),
(105, 'comment_blog_body', ''),
(106, 'like_blog_subject', ''),
(107, 'like_blog_body', ''),
(108, 'comment_event_subject', ''),
(109, 'comment_event_body', ''),
(110, 'like_event_subject', ''),
(111, 'like_event_body', ''),
(112, 'new_event_subject', ''),
(113, 'new_event_body', ''),
(114, 'profile_view_subject', ''),
(115, 'profile_view_body', ''),
(116, 'group_rank_subject', ''),
(117, 'group_rank_body', ''),
(118, 'alert_reminder_subject', ''),
(119, 'alert_reminder_body', ''),
(120, 'site_newsletter_subject', ''),
(121, 'site_newsletter_body', ''),
(122, 'admin_approval_registration', '0'),
(123, 'event_auto_publish', '0'),
(124, 'blog_auto_publish', '0'),
(125, 'ecards_received_subject', ''),
(126, 'ecards_received_body', ''),
(127, 'waitfor_approval_subject', ''),
(128, 'waitfor_approval_body', ''),
(129, 'approval_req_subject', ''),
(130, 'approval_req_body', ''),
(131, 'account_approved_subject', ''),
(132, 'account_approved_body', ''),
(133, 'override_privacy_settings', '0'),
(134, 'uploaded_video_auto_publish', '0'),
(135, 'uploaded_video_size_limit', '50'),
(136, 'retricted_wordlist', ''),
(137, 'enable_myvideos', '0'),
(138, 'shared_video_subject', ''),
(139, 'shared_video_body', '');

-- --------------------------------------------------------

--
-- Table structure for table `sharedbyme_playlists`
--

CREATE TABLE IF NOT EXISTS `sharedbyme_playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sharedwithme_playlists`
--

CREATE TABLE IF NOT EXISTS `sharedwithme_playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `skins`
--

CREATE TABLE IF NOT EXISTS `skins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `color` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `font_family` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `font_size` int(10) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `spam_challenges`
--

CREATE TABLE IF NOT EXISTS `spam_challenges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `answers` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status_updates`
--

CREATE TABLE IF NOT EXISTS `status_updates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_create` tinyint(4) NOT NULL DEFAULT '1',
  `blog_comment` tinyint(4) NOT NULL DEFAULT '1',
  `comment_add` tinyint(4) NOT NULL DEFAULT '1',
  `event_attend` tinyint(4) NOT NULL DEFAULT '1',
  `event_create` tinyint(4) NOT NULL DEFAULT '1',
  `friend_add` tinyint(4) NOT NULL DEFAULT '1',
  `group_create` tinyint(4) NOT NULL DEFAULT '1',
  `group_join` tinyint(4) NOT NULL DEFAULT '1',
  `like_add` tinyint(4) NOT NULL DEFAULT '1',
  `photos_add` tinyint(4) NOT NULL DEFAULT '1',
  `photos_tag` tinyint(4) NOT NULL DEFAULT '1',
  `topic_create` tinyint(4) NOT NULL DEFAULT '1',
  `video_add` tinyint(4) NOT NULL DEFAULT '1',
  `wall_post` tinyint(4) NOT NULL DEFAULT '1',
  `wall_post_link` tinyint(4) NOT NULL DEFAULT '1',
  `user_create` tinyint(4) NOT NULL DEFAULT '1',
  `user_avatar` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `status_updates`
--

INSERT INTO `status_updates` (`id`, `blog_create`, `blog_comment`, `comment_add`, `event_attend`, `event_create`, `friend_add`, `group_create`, `group_join`, `like_add`, `photos_add`, `photos_tag`, `topic_create`, `video_add`, `wall_post`, `wall_post_link`, `user_create`, `user_avatar`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `tag` (`tag`),
  KEY `target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `key`, `name`, `core`) VALUES
(3, 'mobile', 'Mobile Theme', 1),
(4, 'jcoterie', 'jCoterie', 1);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `last_post` datetime NOT NULL,
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(10) unsigned NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL,
  `pinned` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `attachment` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `announcement` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `topic_category_id` (`category_id`),
  KEY `lastposter_id` (`lastposter_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_group_likes`
--

CREATE TABLE IF NOT EXISTS `user_group_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `num_likes` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_user_like_key` (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_group_posts`
--

CREATE TABLE IF NOT EXISTS `user_group_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `num_posts` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_user_post_key` (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `security_question_id` tinyint(3) NOT NULL,
  `security_answers` text CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `friend_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `notification_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `friend_request_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `blog_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `topic_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `conversation_user_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `video_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `confirm_status` tinyint(3) NOT NULL DEFAULT '0',
  `confirmed` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `notification_email` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `timezone` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `hide_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cover` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `luvs` float NOT NULL DEFAULT '0',
  `profile_views` int(10) unsigned NOT NULL DEFAULT '0',
  `disabled_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_username` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_by_admin` tinyint(4) DEFAULT '0',
  `no_word_restriction` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `gender` (`gender`),
  KEY `active` (`active`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `email`, `password`, `role_id`, `security_question_id`, `security_answers`, `avatar`, `photo`, `created`, `last_login`, `photo_count`, `friend_count`, `notification_count`, `friend_request_count`, `blog_count`, `topic_count`, `conversation_user_count`, `video_count`, `gender`, `birthday`, `active`, `confirm_status`, `confirmed`, `code`, `notification_email`, `timezone`, `ip_address`, `privacy`, `username`, `url`, `about`, `featured`, `lang`, `hide_online`, `cover`, `luvs`, `profile_views`, `disabled_name`, `disabled_username`, `disabled_avatar`, `disabled_photo`, `disabled_cover`, `approved_by_admin`, `no_word_restriction`) VALUES
(1, 'animesh', 'biswas', 'animesh.ruet@gmail.com', '$2y$10$6F9e74CuEUYC0wjGlPjIIOPK.hM4dj/Yslx6MICAc0UMD9D3I8yLi', 1, 0, '', '', '', '2014-08-05 20:03:04', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, 0, 'Male', '2014-08-05', 1, 0, 1, 'e838893a4fb2f75acbf03b6a62f12da4', 1, 'Africa/Abidjan', '', 1, '', '', '', 0, '', 0, '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `group_id` int(10) unsigned NOT NULL,
  `playlist_id` int(11) DEFAULT '0',
  `position_order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_positions`
--

CREATE TABLE IF NOT EXISTS `widget_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parentbox` varchar(50) NOT NULL,
  `positions` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_settings`
--

CREATE TABLE IF NOT EXISTS `widget_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '5',
  `photos` int(11) NOT NULL,
  `friends` int(11) NOT NULL DEFAULT '5',
  `videos` int(11) NOT NULL DEFAULT '5',
  `blogs` int(11) NOT NULL DEFAULT '5',
  `groups` int(11) NOT NULL DEFAULT '5',
  `wallone` int(11) NOT NULL DEFAULT '5',
  `status` int(11) NOT NULL DEFAULT '5',
  `ecard` int(11) NOT NULL DEFAULT '5',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_types`
--

CREATE TABLE IF NOT EXISTS `widget_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE IF NOT EXISTS `widgets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `widget_type_id` int(10) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `order` smallint(5) unsigned NOT NULL DEFAULT '1',
  `wallposition` int(11) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `winks`
--

CREATE TABLE IF NOT EXISTS `winks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
--
-- Database: `808`
--

-- --------------------------------------------------------

--
-- Table structure for table `acos`
--

CREATE TABLE IF NOT EXISTS `acos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group` (`group`,`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=18 ;

--
-- Dumping data for table `acos`
--

INSERT INTO `acos` (`id`, `group`, `key`, `description`) VALUES
(1, 'user', 'username', 'Select username'),
(2, 'blog', 'view', 'View blog entry'),
(3, 'blog', 'create', 'Create/Edit blog entry'),
(4, 'album', 'create', 'Create/Edit photo album'),
(5, 'album', 'view', 'View photo album'),
(6, 'event', 'create', 'Create/Edit event'),
(7, 'event', 'view', 'View event'),
(8, 'group', 'create', 'Create/Edit group'),
(9, 'group', 'view', 'View group'),
(10, 'photo', 'upload', 'Upload photos'),
(11, 'photo', 'view', 'View photo'),
(12, 'topic', 'create', 'Create/Edit topic'),
(13, 'topic', 'view', 'View topic'),
(14, 'video', 'share', 'Share video'),
(15, 'video', 'view', 'View video'),
(16, 'attachment', 'upload', 'Upload attachment'),
(17, 'attachment', 'download', 'Download attachment');

-- --------------------------------------------------------

--
-- Table structure for table `activities`
--

CREATE TABLE IF NOT EXISTS `activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `action` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `items` text COLLATE utf8_unicode_ci NOT NULL,
  `item_type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `activity_comment_count` smallint(5) unsigned NOT NULL,
  `query` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL,
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `guest_home` (`type`,`privacy`,`modified`),
  KEY `profile` (`target_id`,`type`,`action`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `activity_comments`
--

CREATE TABLE IF NOT EXISTS `activity_comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `activity_id` int(10) unsigned NOT NULL,
  `comment` text COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `wall_id` (`activity_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `admin_notifications`
--

CREATE TABLE IF NOT EXISTS `admin_notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE IF NOT EXISTS `albums` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `cover` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `modified` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `attachments`
--

CREATE TABLE IF NOT EXISTS `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `plugin_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `original_filename` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `extension` varchar(8) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `downloads` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `plugin_id` (`plugin_id`,`target_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blocked_members`
--

CREATE TABLE IF NOT EXISTS `blocked_members` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `member_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `member_id` (`member_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE IF NOT EXISTS `blogs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `status` tinyint(4) unsigned NOT NULL DEFAULT '1',
  `category_id` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `privacy` (`privacy`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cake_sessions`
--

CREATE TABLE IF NOT EXISTS `cake_sessions` (
  `id` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data` text COLLATE utf8_unicode_ci NOT NULL,
  `expires` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `expires` (`expires`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `parent_id` smallint(5) unsigned NOT NULL DEFAULT '0',
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_count` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `weight` smallint(5) unsigned NOT NULL DEFAULT '0',
  `header` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `create_permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `type` (`type`,`active`,`weight`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci PACK_KEYS=0 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `type`, `parent_id`, `name`, `description`, `item_count`, `active`, `weight`, `header`, `create_permission`) VALUES
(1, 'album', 0, 'Member Albums', '', 0, 1, 0, 0, ''),
(2, 'group', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(3, 'topic', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(4, 'video', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(5, 'event', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(6, 'friend', 0, 'Friends', '', 0, 1, 0, 0, ''),
(7, 'blog', 0, 'Default Category', '', 0, 1, 0, 0, ''),
(8, 'skin', 0, 'All Categories', '', 0, 1, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `censor_words`
--

CREATE TABLE IF NOT EXISTS `censor_words` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `users_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `emails_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `words_list` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `replacement` varchar(20) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `censor_words`
--

INSERT INTO `censor_words` (`id`, `users_list`, `emails_list`, `words_list`, `replacement`) VALUES
(1, '', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `circles`
--

CREATE TABLE IF NOT EXISTS `circles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `type` smallint(5) NOT NULL DEFAULT '1',
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE IF NOT EXISTS `comments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type` (`type`,`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_groups`
--

CREATE TABLE IF NOT EXISTS `conversation_groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(10) unsigned NOT NULL,
  `group_users` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conversation_id` (`conversation_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversation_users`
--

CREATE TABLE IF NOT EXISTS `conversation_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `conversation_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `unread` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `conversation_id` (`conversation_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `conversations`
--

CREATE TABLE IF NOT EXISTS `conversations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `message_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `lastposter_id` (`lastposter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecard_photos`
--

CREATE TABLE IF NOT EXISTS `ecard_photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecard_id` int(10) unsigned NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ecard_id` (`ecard_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecard_senders`
--

CREATE TABLE IF NOT EXISTS `ecard_senders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ecard_id` int(10) unsigned NOT NULL,
  `user_id` int(11) NOT NULL,
  `receiver_id` int(10) unsigned NOT NULL,
  `text` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ecards`
--

CREATE TABLE IF NOT EXISTS `ecards` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `display` varchar(10) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'Y',
  `text` varchar(500) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `email_notifications_settings`
--

CREATE TABLE IF NOT EXISTS `email_notifications_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `get_request` tinyint(4) NOT NULL DEFAULT '1',
  `acceept_request` tinyint(4) NOT NULL DEFAULT '1',
  `luvs` tinyint(4) NOT NULL DEFAULT '1',
  `hifives` tinyint(4) NOT NULL DEFAULT '1',
  `hugs` tinyint(4) NOT NULL DEFAULT '1',
  `kisses` tinyint(4) NOT NULL DEFAULT '1',
  `winks` tinyint(4) NOT NULL DEFAULT '1',
  `update_photo` tinyint(4) NOT NULL DEFAULT '1',
  `update_status` tinyint(4) NOT NULL DEFAULT '1',
  `upcoming_birthday` tinyint(4) NOT NULL DEFAULT '1',
  `new_message` tinyint(4) NOT NULL DEFAULT '1',
  `group_invite` tinyint(4) NOT NULL DEFAULT '1',
  `group_topic` tinyint(4) NOT NULL DEFAULT '1',
  `group_post` tinyint(4) NOT NULL DEFAULT '1',
  `comment_status` tinyint(4) NOT NULL DEFAULT '1',
  `comment_photo` tinyint(4) NOT NULL DEFAULT '1',
  `like_status` tinyint(4) NOT NULL DEFAULT '1',
  `like_photo` tinyint(4) NOT NULL DEFAULT '1',
  `comment_blog` tinyint(4) NOT NULL DEFAULT '1',
  `like_blog` tinyint(4) NOT NULL DEFAULT '1',
  `comment_event` tinyint(4) NOT NULL DEFAULT '1',
  `like_event` tinyint(4) NOT NULL DEFAULT '1',
  `new_event` tinyint(4) NOT NULL DEFAULT '1',
  `profile_view` tinyint(4) NOT NULL DEFAULT '1',
  `group_rank` tinyint(4) NOT NULL DEFAULT '1',
  `alert_reminder` tinyint(4) NOT NULL DEFAULT '1',
  `site_newsletter` tinyint(4) NOT NULL DEFAULT '1',
  `ecards_received` tinyint(4) NOT NULL DEFAULT '1',
  `shared_video` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `event_rsvps`
--

CREATE TABLE IF NOT EXISTS `event_rsvps` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `event_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `rsvp` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rsvp_list` (`event_id`,`user_id`),
  KEY `event_id` (`event_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE IF NOT EXISTS `events` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from` date NOT NULL,
  `from_time` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `to` date NOT NULL,
  `to_time` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `type` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `event_rsvp_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `address` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `status` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `events_category_settings`
--

CREATE TABLE IF NOT EXISTS `events_category_settings` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `location` tinyint(4) NOT NULL DEFAULT '1',
  `address` tinyint(4) NOT NULL DEFAULT '1',
  `cat_description` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friend_requests`
--

CREATE TABLE IF NOT EXISTS `friend_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `sender_id` (`sender_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `friends`
--

CREATE TABLE IF NOT EXISTS `friends` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `friend_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `friend_id` (`friend_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_accounts`
--

CREATE TABLE IF NOT EXISTS `gold_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `virtual_cash` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_discounts`
--

CREATE TABLE IF NOT EXISTS `gold_discounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bundle_id` int(11) NOT NULL,
  `bundle_name` varchar(200) NOT NULL,
  `discount_start_date` datetime NOT NULL,
  `discount_end_date` datetime NOT NULL,
  `discount_rate` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `gold_payment_histories`
--

CREATE TABLE IF NOT EXISTS `gold_payment_histories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `virtual_cash` int(11) NOT NULL,
  `real_money` decimal(10,2) NOT NULL,
  `reason` varchar(100) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goldbundles`
--

CREATE TABLE IF NOT EXISTS `goldbundles` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `gold_coins` int(11) NOT NULL,
  `real_money` decimal(10,2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `goldsettings`
--

CREATE TABLE IF NOT EXISTS `goldsettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `options` varchar(200) NOT NULL,
  `value` varchar(500) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `goldsettings`
--

INSERT INTO `goldsettings` (`id`, `options`, `value`) VALUES
(1, 'paypal_email', ''),
(2, 'live_or_sandbox_paypal', '1'),
(3, 'gold_conversion_rate', '2'),
(4, 'give_to_friend', '1'),
(5, 'friend_gift_cost', '10'),
(6, 'admin__gold_account', '');

-- --------------------------------------------------------

--
-- Table structure for table `group_users`
--

CREATE TABLE IF NOT EXISTS `group_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `group_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` tinyint(2) unsigned NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `group_id` (`group_id`),
  KEY `user_id` (`user_id`),
  KEY `status` (`status`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_url` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(2) unsigned NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `group_user_count` smallint(5) unsigned NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `topic_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `video_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `event_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `type` (`type`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `category_id`, `name`, `group_url`, `user_id`, `description`, `type`, `photo`, `group_user_count`, `created`, `photo_count`, `topic_count`, `video_count`, `event_count`, `comment_count`, `featured`) VALUES
(1, 3, 'Community', 'Community', 0, 'This group is common for all community topics', 1, '', 1, '2014-08-05 20:21:13', 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hifives`
--

CREATE TABLE IF NOT EXISTS `hifives` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `hooks`
--

CREATE TABLE IF NOT EXISTS `hooks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `controller` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `position` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `weight` smallint(6) NOT NULL DEFAULT '0',
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `version` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`),
  KEY `controller` (`controller`),
  KEY `action` (`action`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=17 ;

--
-- Dumping data for table `hooks`
--

INSERT INTO `hooks` (`id`, `name`, `key`, `controller`, `action`, `position`, `weight`, `enabled`, `version`, `settings`, `permission`) VALUES
(1, 'Featured Members', 'featured_members', 'home', 'index', 'home_sidebar', 6, 1, '2.0', 'a:0:{}', ''),
(2, 'Popular Blogs', 'popular_blogs', 'blogs', 'index', 'blogs_sidebar', 2, 1, '2.0', 'a:1:{s:9:"num_blogs";s:1:"5";}', ''),
(3, 'Popular Albums', 'popular_albums', 'photos', 'index', 'photos_sidebar', 3, 1, '2.0', 'a:1:{s:10:"num_albums";s:1:"5";}', ''),
(4, 'Today Birthdays', 'today_birthdays', 'home', 'index', 'home_sidebar', 4, 1, '2.0', 'a:1:{s:16:"friend_birthdays";s:1:"1";}', ''),
(5, 'Online Users', 'online_users', '', '', 'global_sidebar', 5, 1, '2.0', 'a:1:{s:16:"num_online_users";s:2:"12";}', ''),
(6, 'Recently Joined', 'recently_joined', 'home', 'index', 'home_sidebar', 7, 1, '2.0', 'a:1:{s:15:"num_new_members";s:2:"10";}', ''),
(7, 'Popular Events', 'popular_events', 'events', 'index', 'events_sidebar', 8, 1, '2.0', 'a:1:{s:10:"num_events";s:1:"5";}', ''),
(8, 'Popular Groups', 'popular_groups', 'groups', 'index', 'groups_sidebar', 9, 1, '2.0', 'a:1:{s:10:"num_groups";s:1:"5";}', ''),
(9, 'Popular Topics', 'popular_topics', 'topics', 'index', 'topics_sidebar', 10, 1, '2.0', 'a:1:{s:10:"num_topics";s:1:"5";}', ''),
(10, 'Popular Videos', 'popular_videos', 'videos', 'index', 'videos_sidebar', 11, 1, '2.0', 'a:1:{s:10:"num_videos";s:1:"5";}', ''),
(11, 'Friend Suggestions', 'friend_suggestions', '', '', 'global_sidebar', 12, 1, '2.0', 'a:1:{s:22:"num_friend_suggestions";s:1:"2";}', ''),
(12, 'Featured Groups', 'featured_groups', 'groups', 'index', 'groups_sidebar', 13, 1, '2.0', 'a:0:{}', ''),
(13, 'Profile Luv', 'profile_luvs', '', '', 'profile_sidebar', 14, 1, '1.0', '', ''),
(14, 'Profile Affections', 'profile_affections', '', '', 'profile_sidebar', 15, 1, '1.0', '', ''),
(15, 'Notifications', 'notifications', '', '', 'profile_sidebar', 16, 1, '1.0', 'a:1:{s:17:"num_notifications";s:2:"10";}', ''),
(16, 'Online Friends', 'online_friends', '', '', 'profile_sidebar', 17, 1, '1.0', 'a:1:{s:11:"num_friends";s:2:"10";}', '');

-- --------------------------------------------------------

--
-- Table structure for table `hugs`
--

CREATE TABLE IF NOT EXISTS `hugs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `kisses`
--

CREATE TABLE IF NOT EXISTS `kisses` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `languages`
--

CREATE TABLE IF NOT EXISTS `languages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `languages`
--

INSERT INTO `languages` (`id`, `key`, `name`) VALUES
(1, 'eng', 'English');

-- --------------------------------------------------------

--
-- Table structure for table `layouts`
--

CREATE TABLE IF NOT EXISTS `layouts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `controller` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `task` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `layout` text COLLATE utf8_unicode_ci,
  `modified` datetime NOT NULL,
  `has_header` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `has_footer` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=74 ;

--
-- Dumping data for table `layouts`
--

INSERT INTO `layouts` (`id`, `controller`, `task`, `layout`, `modified`, `has_header`, `has_footer`) VALUES
(1, 'Groups', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/group_tabs.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/view.ctp"}]}]}]}', '2014-05-14 14:46:00', 1, 1),
(2, 'Groups', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"groups/leftcol_list.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"groups/list.ctp"}]}]}]}', '2014-02-22 16:53:36', 1, 1),
(3, 'Affections', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"hook","label":"Hook","title":"","divid":"jvleftnav","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"affections/index.ctp"}]}]}]}', '2014-02-11 15:46:53', 1, 1),
(4, 'Pages', 'display', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"pages/index.ctp"}]}]}]}', '2014-02-11 20:22:40', 1, 1),
(5, 'Home', 'contact', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/contact.ctp"}]}]}]}', '2014-02-22 14:28:32', 1, 1),
(6, 'Home', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/header.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/index.ctp"}]}]}]}', '2014-06-24 06:54:05', 1, 1),
(7, 'Conversations', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"conversations/view.ctp"}]}]}]}', '2014-05-09 14:17:46', 0, 0),
(8, 'Users', 'register', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"jvLogin","contents":[{"type":"span8","label":"Column - span8","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/register-bg.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/registration.ctp"}]}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_three.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_four.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_five.ctp"}]},{"type":"span4","label":"Column - span4","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/picture_six.ctp"}]}]}]}', '2014-06-24 06:48:56', 1, 1),
(9, 'Users', 'password', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/password.ctp"}]}]}]}', '2014-06-12 16:25:45', 1, 1),
(10, 'Users', 'recover', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/recover.ctp"}]}]}]}', '2014-02-23 13:53:59', 1, 1),
(11, 'Users', 'resetpass', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/resetpass.ctp"}]}]}]}', '2014-02-23 13:55:12', 1, 1),
(12, 'Users', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/view_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/view_leftcol.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"users/view_avatar.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"profile_luvs","name":"Profile Luv"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"profile_affections","name":"Profile Affections"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/view_center.ctp"}]}]}]}', '2014-06-10 18:05:31', 1, 1),
(13, 'Users', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/index_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"users/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/index_center.ctp"}]}]}]}', '2014-02-23 13:53:05', 1, 1),
(14, 'Videos', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/index.ctp"}]}]}]}', '2014-02-23 13:56:08', 1, 1),
(15, 'Videos', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"videos/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/view.ctp"}]}]}]}', '2014-02-23 13:56:19', 1, 1),
(16, 'Blogs', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/index.ctp"}]}]}]}', '2014-02-11 20:14:07', 1, 1),
(17, 'Blogs', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span7","label":"Column - span7","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/view.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-02-11 21:45:29', 1, 1),
(18, 'Blogs', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"blogs/create.ctp"}]}]}]}', '2014-02-11 20:13:37', 1, 1),
(19, 'Events', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"events/index.ctp"}]}]}]}', '2014-02-11 20:36:07', 1, 1),
(20, 'Events', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"events/view_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/view.ctp"}]}]}]}', '2014-02-11 20:35:37', 1, 1),
(21, 'Events', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/create.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-05-01 15:46:46', 1, 1),
(22, 'Tags', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"tags/view_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"tags/view.ctp"}]}]}]}', '2014-02-11 20:16:13', 1, 1),
(23, 'Search', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"search/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"search/index.ctp"}]}]}]}', '2014-02-11 20:16:41', 1, 1),
(24, 'Photos', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"jvleftnav","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"jvcenter","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index.ctp"}]}]}]}', '2014-02-22 16:54:25', 1, 1),
(25, 'Photos', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/view.ctp"}]}]}]}', '2014-02-11 20:21:13', 1, 1),
(26, 'Photos', 'upload', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/upload.ctp"}]}]}]}', '2014-02-11 20:22:14', 1, 1),
(27, 'Albums', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"hook","label":"Hook","title":"","divid":"leftnav","divclass":"","key":"popular_albums","name":"Popular Albums"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"albums/view.ctp"}]}]}]}', '2014-02-11 15:48:07', 1, 1),
(28, 'Albums', 'edit', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"albums/edit.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]}]}]}', '2014-02-11 15:47:34', 1, 1),
(29, 'Friends', 'my', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/users/ajax_browse/home"}]}]}]}', '2014-03-02 18:41:19', 1, 1),
(30, 'Friends', 'invite', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/friends/ajax_invite"}]}]}]}', '2014-02-22 14:27:25', 1, 1),
(31, 'Friends', 'requests', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/requests.ctp"}]}]}]}', '2014-02-22 14:27:37', 1, 1),
(32, 'Notifications', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/notifications/ajax_show/home"}]}]}]}', '2014-02-22 16:54:04', 1, 1),
(33, 'Conversations', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"","url":"/conversations/ajax_browse"}]}]}]}', '2014-02-23 21:35:11', 1, 1),
(34, 'Users', 'visitors', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/visitors.ctp"}]}]}]}', '2014-02-23 13:55:32', 1, 1),
(35, 'Friends', 'favourites', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/users/ajax_browse/favourites"}]}]}]}', '2014-02-22 14:27:04', 1, 1),
(36, 'Users', 'blocked_users', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/blocked_users.ctp"}]}]}]}', '2014-06-12 16:26:56', 1, 1),
(37, 'Friends', 'birthdays', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"ajax","label":"Ajax","title":"","divid":"","divclass":"myfriends","url":"/friends/ajax_browse/birthdays"}]}]}]}', '2014-04-24 01:05:25', 1, 1),
(38, 'Users', 'verifyanswer', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"users/verifyanswer.ctp"}]}]}]}', '2014-04-24 19:37:25', 1, 1),
(39, 'Groups', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"groups/create.ctp"}]}]}]}', '2014-05-15 21:31:26', 1, 1),
(40, 'Events', 'all_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/all_events.ctp"}]}]}]}', '2014-05-05 16:48:59', 1, 1),
(41, 'Events', 'categorized_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/categorized_events.ctp"}]}]}]}', '2014-05-05 17:05:48', 1, 1),
(42, 'Events', 'friends_attend_event', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/friends_attend_event.ctp"}]}]}]}', '2014-05-06 16:40:42', 1, 1),
(43, 'Events', 'past_events', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/past_events.ctp"}]}]}]}]}', '2014-05-06 16:44:01', 1, 1),
(44, 'Events', 'my_upcoming_event', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"events/my_upcoming_event.ctp"}]}]}]}', '2014-05-06 16:48:05', 1, 1),
(45, 'Blogs', 'my_blogs', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/my_blogs.ctp"}]}]}]}', '2014-05-06 17:54:17', 1, 1),
(46, 'Blogs', 'friends_blogs', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/index_leftcol.ctp"},{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"blogs/friends_blogs.ctp"}]}]}]}', '2014-05-06 18:04:37', 1, 1),
(47, 'Videos', 'my_videos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"videos/my_videos.ctp"}]}]}]}', '2014-05-09 15:25:05', 1, 1),
(48, 'Videos', 'friends_videos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"videos/friends_videos.ctp"}]}]}]}', '2014-05-07 17:00:05', 1, 1),
(49, 'Users', 'setnotifications', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/setnotifications.ctp"}]}]}]}', '2014-06-12 16:26:16', 1, 1),
(50, 'Albums', 'my_albums', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"jvcenter","divclass":"","path":"albums/my_albums.ctp"}]}]}]}', '2014-05-07 16:54:17', 1, 1),
(51, 'Albums', 'friends_albums', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"photos/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"albums/friends_albums.ctp"}]}]}]}', '2014-05-07 16:57:00', 1, 1),
(52, 'Conversations', 'my_conversation', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"conversations/my_conversation.ctp"}]}]}]}', '2014-05-16 19:35:06', 1, 1),
(53, 'Friends', 'my_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/my_friends.ctp"}]}]}]}', '2014-05-09 13:36:47', 1, 1),
(54, 'Friends', 'favourite_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/favourite_friends.ctp"}]}]}]}', '2014-05-09 13:32:00', 1, 1),
(55, 'Users', 'profile', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/profile.ctp"}]}]}]}', '2014-06-12 16:24:52', 1, 1);
INSERT INTO `layouts` (`id`, `controller`, `task`, `layout`, `modified`, `has_header`, `has_footer`) VALUES
(56, 'Users', 'my_friends', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"users/index_scripts.ctp"}]},{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvleftnav","divclass":"","path":"users/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"users/my_friends.ctp"}]}]}]}', '2014-05-22 13:53:05', 1, 1),
(57, 'Friends', 'requests_received', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"friends/requests_received.ctp"}]}]}]}', '2014-05-09 13:36:47', 1, 1),
(58, 'Home', 'activities', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/header.ctp"}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"home/leftcol.ctp"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"notifications","name":"Notifications"},{"type":"hook","label":"Hook","title":"","divid":"","divclass":"","key":"online_friends","name":"Online Friends"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"jvcenter","divclass":"","path":"home/activities.ctp"}]}]}]}', '2014-02-22 14:49:50', 1, 1),
(59, 'Topics', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/index_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/index.ctp"}]}]}]}', '2014-06-10 18:51:48', 1, 1),
(60, 'Topics', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/view_leftcol.ctp"},{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"topics/view.ctp"}]}]}]}', '2014-06-10 18:49:06', 1, 1),
(61, 'Topics', 'create', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"topics/create.ctp"}]}]}]}', '2014-06-10 18:55:25', 1, 1),
(62, 'Videos', 'videosearch', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"videos/videosearch.ctp"}]}]}]}', '2014-06-18 15:14:15', 1, 1),
(63, 'Playlists', 'allplaylists', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"playlists/allplaylists.ctp"}]}]}]}', '2014-06-18 15:17:45', 1, 1),
(64, 'Playlists', 'view', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"playlists/view.ctp"}]}]}]}', '2014-06-18 15:21:49', 1, 1),
(65, 'Ecards', 'index', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"ecards/index.ctp"}]}]}]}', '2014-06-23 14:19:01', 1, 1),
(66, 'Ecards', 'my_ecards', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/my_ecards.ctp"}]}]}]}', '2014-06-23 14:20:50', 1, 1),
(67, 'Ecards', 'send_wigets', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/send_wigets.ctp"}]}]}]}', '2014-06-23 14:22:35', 1, 1),
(68, 'Ecards', 'rcv_ecards', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"ecards/rcv_ecards.ctp"}]}]}]}', '2014-06-10 18:57:25', 1, 1),
(69, 'Videos', 'storedvideos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/wide_ads.ctp"}]}]},{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span2","label":"Column - span2","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"custom/tall_ads.ctp"}]},{"type":"span10","label":"Column - span10","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","title":"","divid":"","divclass":"","path":"videos/storedvideos.ctp"}]}]}]}', '2014-07-10 15:40:57', 1, 1),
(70, 'Goldbundles', 'buygold', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/buygold.ctp"}]}]}', '2014-07-15 13:15:15', 1, 1),
(71, 'Goldbundles', 'buygoldhistory', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/buygoldhistory.ctp"}]}]}]}', '2014-07-15 13:17:13', 1, 1),
(72, 'Goldbundles', 'friend_gift', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"goldbundles/friend_gift.ctp"}]}]}]}', '2014-07-15 13:19:36', 1, 1),
(73, 'Playlists', 'sharedvideos', '{"details":[{"type":"row","label":"Row","title":"","divid":"","divclass":"","contents":[{"type":"span12","label":"Column - span12","title":"","divid":"","divclass":"","contents":[{"type":"element","label":"Element","divid":"","divclass":"","path":"playlists/sharedvideos.ctp"}]}]}]}', '2014-08-01 11:19:29', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE IF NOT EXISTS `likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `thumb_up` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `type` (`type`,`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `login_infos`
--

CREATE TABLE IF NOT EXISTS `login_infos` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `created` date NOT NULL,
  `count` smallint(6) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `luvs`
--

CREATE TABLE IF NOT EXISTS `luvs` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mooskin_users`
--

CREATE TABLE IF NOT EXISTS `mooskin_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mooskin_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `mooskin_id` (`mooskin_id`),
  KEY `user_id` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `mooskins`
--

CREATE TABLE IF NOT EXISTS `mooskins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `total_used` int(10) NOT NULL DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `category_id` (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=42 ;

--
-- Dumping data for table `mooskins`
--

INSERT INTO `mooskins` (`id`, `name`, `category_id`, `total_used`, `status`, `created`) VALUES
(1, 'default', 8, 0, 1, '0000-00-00 00:00:00'),
(2, 'First Skin', 8, 0, 1, '0000-00-00 00:00:00'),
(3, 'Second Skin', 8, 0, 1, '0000-00-00 00:00:00'),
(4, 'Cars', 8, 0, 1, '0000-00-00 00:00:00'),
(5, 'Audi', 8, 0, 1, '0000-00-00 00:00:00'),
(6, 'Lamborghini', 8, 0, 1, '0000-00-00 00:00:00'),
(7, 'Citroen', 8, 0, 1, '0000-00-00 00:00:00'),
(8, 'American Choppers', 8, 0, 1, '0000-00-00 00:00:00'),
(9, 'American Choppers2', 8, 0, 1, '0000-00-00 00:00:00'),
(10, 'Bugatti', 8, 0, 1, '0000-00-00 00:00:00'),
(11, 'Custom Bike', 8, 0, 1, '0000-00-00 00:00:00'),
(12, 'Ducati', 8, 0, 1, '0000-00-00 00:00:00'),
(13, 'Ducati V2', 8, 0, 1, '0000-00-00 00:00:00'),
(14, 'Harley', 8, 0, 1, '0000-00-00 00:00:00'),
(15, 'My Buddy', 8, 0, 1, '0000-00-00 00:00:00'),
(16, 'My Buddy V2', 8, 0, 1, '0000-00-00 00:00:00'),
(17, 'The Veyron', 8, 0, 1, '0000-00-00 00:00:00'),
(18, 'The Veyron White', 8, 0, 1, '0000-00-00 00:00:00'),
(19, 'The Veyron White V2', 8, 0, 1, '0000-00-00 00:00:00'),
(20, 'Yamaha R1', 8, 0, 1, '0000-00-00 00:00:00'),
(21, 'BMW Bike', 8, 0, 1, '0000-00-00 00:00:00'),
(22, 'Boga Lake', 8, 0, 1, '0000-00-00 00:00:00'),
(23, 'Field', 8, 0, 1, '0000-00-00 00:00:00'),
(24, 'Flower', 8, 0, 1, '0000-00-00 00:00:00'),
(25, 'Hill', 8, 0, 1, '0000-00-00 00:00:00'),
(26, 'House', 8, 0, 1, '0000-00-00 00:00:00'),
(27, 'Nature', 8, 0, 1, '0000-00-00 00:00:00'),
(28, 'Pink Tree', 8, 0, 1, '0000-00-00 00:00:00'),
(29, 'River', 8, 0, 1, '0000-00-00 00:00:00'),
(30, 'Road', 8, 0, 1, '0000-00-00 00:00:00'),
(31, 'Tree', 8, 0, 1, '0000-00-00 00:00:00'),
(32, 'Brazil 2014', 8, 0, 1, '0000-00-00 00:00:00'),
(33, 'England', 8, 0, 1, '0000-00-00 00:00:00'),
(34, 'FIFA World Cup 2014', 8, 0, 1, '0000-00-00 00:00:00'),
(35, 'Football', 8, 0, 1, '0000-00-00 00:00:00'),
(36, 'Garman', 8, 0, 1, '0000-00-00 00:00:00'),
(37, 'Italy', 8, 0, 1, '0000-00-00 00:00:00'),
(38, 'Lion El Messi', 8, 0, 1, '0000-00-00 00:00:00'),
(39, 'Messi', 8, 0, 1, '0000-00-00 00:00:00'),
(40, 'Messi Addias', 8, 0, 1, '0000-00-00 00:00:00'),
(41, 'Samba', 8, 0, 1, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `myvideos`
--

CREATE TABLE IF NOT EXISTS `myvideos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned DEFAULT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `thumb` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `group_id` int(10) unsigned DEFAULT NULL,
  `playlist_id` int(11) DEFAULT '0',
  `position_order` int(11) DEFAULT '0',
  `status` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `notifications`
--

CREATE TABLE IF NOT EXISTS `notifications` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `sender_id` int(10) unsigned NOT NULL,
  `created` datetime NOT NULL,
  `text` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `read` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `sender_id` (`sender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE IF NOT EXISTS `pages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `alias` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `params` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `weight` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `alias` (`alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`id`, `title`, `alias`, `content`, `permission`, `params`, `created`, `modified`, `menu`, `icon_class`, `weight`) VALUES
(1, 'Terms', 'terms', '<p>These are our terms</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:19:26', '2014-06-10 16:19:26', 1, '', 0),
(2, 'About', 'about', '<p>This is all about us</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:20:19', '2014-06-10 16:20:19', 1, '', 0),
(3, 'Privacy', 'privacy', '<p>This is your privacy</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:21:03', '2014-06-10 16:21:03', 1, '', 0),
(4, 'Online Safety', 'online_safety', '<p>Be safe</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:21:52', '2014-06-10 16:21:52', 1, '', 0),
(5, 'Help', 'help', '<p>Here is some help</p>', '', 'a:1:{s:8:"comments";s:1:"1";}', '2014-06-10 16:22:21', '2014-06-10 16:22:21', 1, '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pass_settings`
--

CREATE TABLE IF NOT EXISTS `pass_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `option` varchar(50) NOT NULL,
  `value` varchar(1000) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `password_requests`
--

CREATE TABLE IF NOT EXISTS `password_requests` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `code` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photo_tags`
--

CREATE TABLE IF NOT EXISTS `photo_tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `photo_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `tagger_id` int(10) unsigned NOT NULL,
  `value` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `style` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `photo_id` (`photo_id`),
  KEY `tagger_id` (`tagger_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `caption` text COLLATE utf8_unicode_ci NOT NULL,
  `path` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `original` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `target_id` (`target_id`,`type`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `playlists`
--

CREATE TABLE IF NOT EXISTS `playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `video_count` int(11) NOT NULL DEFAULT '0',
  `cover` varchar(500) DEFAULT NULL,
  `shared_playlist` tinyint(4) NOT NULL DEFAULT '0',
  `on_off` tinyint(4) DEFAULT '0',
  `display_in_profile` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `plugins`
--

CREATE TABLE IF NOT EXISTS `plugins` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `key` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `permission` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `settings` text COLLATE utf8_unicode_ci NOT NULL,
  `enabled` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `core` tinyint(1) unsigned NOT NULL,
  `version` varchar(16) COLLATE utf8_unicode_ci NOT NULL,
  `weight` smallint(5) unsigned NOT NULL,
  `menu` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `url` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `icon_class` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=13 ;

--
-- Dumping data for table `plugins`
--

INSERT INTO `plugins` (`id`, `name`, `key`, `permission`, `settings`, `enabled`, `core`, `version`, `weight`, `menu`, `url`, `icon_class`) VALUES
(1, 'People', 'user', '', 'a:0:{}', 1, 1, '2.0', 1, 1, '/users', 'icon-user'),
(2, 'Blogs', 'blog', '', '', 1, 1, '2.0', 2, 1, '/blogs', 'icon-edit'),
(3, 'Photos', 'photo', '', '', 1, 1, '2.0', 3, 1, '/photos', 'icon-picture'),
(4, 'Videos', 'video', '', '', 1, 1, '2.0', 4, 1, '/videos', 'icon-facetime-video'),
(5, 'Topics', 'topic', '', '', 1, 1, '2.0', 5, 1, '/topics', 'icon-comments'),
(6, 'Groups', 'group', '', '', 1, 1, '2.0', 6, 1, '/groups', 'icon-group'),
(7, 'Events', 'event', '', '', 1, 1, '2.0', 7, 1, '/events', 'icon-calendar'),
(8, 'Conversations', 'conversation', '', '', 1, 1, '2.0', 8, 0, '', ''),
(9, 'Pages', 'page', '', '', 1, 1, '2.0', 9, 0, '', ''),
(10, 'Maintenance', 'maintenance', '', '', 1, 0, '1.0', 10, 0, '', ''),
(11, 'Layout Builder', 'layoutbuilder', '', '', 1, 0, '1.0', 11, 1, '', ''),
(12, 'Affections', 'affection', '', 'a:11:{s:13:"luvs_duration";s:1:"3";s:19:"luvs_perday_regular";s:1:"3";s:15:"luvs_perday_vip";s:2:"10";s:21:"hugs_per_user_regular";s:1:"1";s:17:"hugs_per_user_vip";s:1:"3";s:24:"hifives_per_user_regular";s:1:"1";s:20:"hifives_per_user_vip";s:1:"3";s:23:"kisses_per_user_regular";s:1:"1";s:19:"kisses_per_user_vip";s:1:"3";s:22:"winks_per_user_regular";s:1:"1";s:18:"winks_per_user_vip";s:1:"3";}', 1, 0, '1.0', 12, 1, '/affections', '');

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings`
--

CREATE TABLE IF NOT EXISTS `privacy_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `action_title` varchar(100) NOT NULL,
  `action_controller` varchar(200) NOT NULL,
  `privacy_cat_value` int(11) NOT NULL DEFAULT '0',
  `privacy_sub_cat` int(11) NOT NULL DEFAULT '0',
  `comment_privacy` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `privacy_settings`
--

INSERT INTO `privacy_settings` (`id`, `user_id`, `action_title`, `action_controller`, `privacy_cat_value`, `privacy_sub_cat`, `comment_privacy`) VALUES
(1, 0, 'Profile View', 'users_view', 1, 0, 0),
(2, 0, 'Album View', 'albums_view', 1, 0, 0),
(3, 0, 'Blogs View', 'blogs_view', 1, 0, 0),
(4, 0, 'Contact Message', 'conversations_ajax_doSend', 1, 0, 0),
(5, 0, 'Video Comment', 'videos_view', 1, 0, 1),
(6, 0, 'Photo Comment', 'photos_view', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `privacy_settings_categories`
--

CREATE TABLE IF NOT EXISTS `privacy_settings_categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `value` int(11) NOT NULL,
  `type` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `privacy_settings_categories`
--

INSERT INTO `privacy_settings_categories` (`id`, `name`, `value`, `type`) VALUES
(1, 'public', 1, 0),
(2, 'member', 2, 1),
(3, 'me', 3, 0),
(4, 'friend', 4, 1);

-- --------------------------------------------------------

--
-- Table structure for table `profile_field_values`
--

CREATE TABLE IF NOT EXISTS `profile_field_values` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `profile_field_id` int(10) unsigned NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `profile_field_id` (`profile_field_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_fields`
--

CREATE TABLE IF NOT EXISTS `profile_fields` (
  `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `values` text COLLATE utf8_unicode_ci NOT NULL,
  `required` tinyint(1) unsigned NOT NULL,
  `registration` tinyint(1) unsigned NOT NULL,
  `searchable` tinyint(1) unsigned NOT NULL,
  `active` tinyint(1) unsigned NOT NULL,
  `weight` smallint(5) unsigned NOT NULL,
  `profile` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `profile_views`
--

CREATE TABLE IF NOT EXISTS `profile_views` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `viewer_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ranks`
--

CREATE TABLE IF NOT EXISTS `ranks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rank` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `min_posts` mediumint(8) unsigned NOT NULL,
  `role_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `reports`
--

CREATE TABLE IF NOT EXISTS `reports` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `target_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `reason` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE IF NOT EXISTS `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `is_super` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `params` text COLLATE utf8_unicode_ci NOT NULL,
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `is_admin`, `is_super`, `params`, `core`) VALUES
(1, 'Super Admin', 1, 1, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,group_delete,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1),
(2, 'Member', 0, 0, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1),
(3, 'Guest', 0, 0, 'global_search,user_username,blog_view,blog_create,album_create,album_view,event_create,event_view,group_create,group_view,photo_upload,photo_view,topic_create,topic_view,video_share,video_view,attachment_upload,attachment_download', 1);

-- --------------------------------------------------------

--
-- Table structure for table `security_questions`
--

CREATE TABLE IF NOT EXISTS `security_questions` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `security_questions`
--

INSERT INTO `security_questions` (`id`, `question`, `active`) VALUES
(1, 'What was your childhood nickname? ', 1),
(2, 'In what city did you meet your spouse/significant other?', 1),
(3, 'What is the name of your favorite childhood friend? ', 1),
(4, 'What street did you live on in third grade?', 1),
(5, 'What is your oldest sibling''s middle name?', 1),
(6, 'What was the name of your first stuffed animal?', 1);

-- --------------------------------------------------------

--
-- Table structure for table `settings`
--

CREATE TABLE IF NOT EXISTS `settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `field` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `field` (`field`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=140 ;

--
-- Dumping data for table `settings`
--

INSERT INTO `settings` (`id`, `field`, `value`) VALUES
(1, 'site_name', 'animesh'),
(2, 'site_email', 'aniemsh.ruet@gmail.com'),
(3, 'site_keywords', ''),
(4, 'site_description', ''),
(5, 'default_feed', 'everyone'),
(6, 'recaptcha', '0'),
(7, 'recaptcha_publickey', ''),
(8, 'recaptcha_privatekey', ''),
(9, 'email_validation', '0'),
(10, 'guest_message', ''),
(11, 'header_code', ''),
(12, 'sidebar_code', ''),
(13, 'footer_code', ''),
(14, 'member_message', ''),
(15, 'select_theme', '1'),
(16, 'guest_search', '1'),
(17, 'default_theme', 'jcoterie'),
(18, 'force_login', '1'),
(19, 'homepage_code', ''),
(20, 'ban_ips', ''),
(21, 'feed_selection', '1'),
(22, 'admin_notes', ''),
(23, 'site_offline', '0'),
(24, 'offline_message', ''),
(25, 'cron_last_run', ''),
(26, 'version', '2.0.2'),
(27, 'profile_privacy', '1'),
(28, 'fb_app_id', ''),
(29, 'fb_app_secret', ''),
(30, 'fb_integration', '0'),
(31, 'popular_interval', '30'),
(32, 'add_this', '1'),
(33, 'default_language', 'eng'),
(34, 'disable_registration', '0'),
(35, 'time_format', '12h'),
(36, 'date_format', 'F j \\a\\t g:ia'),
(37, 'logo', ''),
(38, 'hide_admin_link', '0'),
(39, 'select_language', '1'),
(40, 'analytics_code', ''),
(41, 'registration_notify', '0'),
(42, 'hide_activites', '0'),
(43, 'username_change', '1'),
(44, 'save_original_image', '0'),
(45, 'mail_transport', 'Mail'),
(46, 'smtp_host', 'localhost'),
(47, 'smtp_username', ''),
(48, 'smtp_password', ''),
(49, 'smtp_port', ''),
(50, 'restricted_usernames', ''),
(51, 'enable_registration_code', '0'),
(52, 'registration_code', ''),
(53, 'languages', 'eng=English'),
(54, 'google_dev_key', ''),
(55, 'timezone', 'Africa/Abidjan'),
(56, 'enable_timezone_selection', '1'),
(57, 'require_birthday', '1'),
(58, 'enable_spam_challenge', '0'),
(59, 'show_credit', '0'),
(60, 'name_change', '1'),
(61, 'registration_message', ''),
(62, 'admin_password', ''),
(63, 'recently_joined', '1'),
(64, 'featured_members', '1'),
(65, 'num_new_members', '10'),
(66, 'num_friend_suggestions', '2'),
(67, 'friend_birthdays', '0'),
(68, 'set_autorefresh_interval', ''),
(69, 'enable_security_ques', ''),
(70, 'enable_fan_pages', ''),
(71, 'number_fan_pages', ''),
(72, 'censor_word', ''),
(73, 'users_not_adult', ''),
(74, 'default_subject', ''),
(75, 'default_body', ''),
(76, 'get_request_subject', ''),
(77, 'get_request_body', ''),
(78, 'acceept_request_subject', ''),
(79, 'acceept_request_body', ''),
(80, 'affection_subject', ''),
(81, 'affection_body', ''),
(82, 'update_photo_subject', ''),
(83, 'update_photo_body', ''),
(84, 'update_status_subject', ''),
(85, 'update_status_body', ''),
(86, 'upcoming_birthday_subject', ''),
(87, 'upcoming_birthday_body', ''),
(88, 'new_message_subject', ''),
(89, 'new_message_body', ''),
(90, 'group_invite_subject', ''),
(91, 'group_invite_body', ''),
(92, 'group_topic_subject', ''),
(93, 'group_topic_body', ''),
(94, 'group_post_subject', ''),
(95, 'group_post_body', ''),
(96, 'comment_status_subject', ''),
(97, 'comment_status_body', ''),
(98, 'comment_photo_subject', ''),
(99, 'comment_photo_body', ''),
(100, 'like_status_subject', ''),
(101, 'like_status_body', ''),
(102, 'like_photo_subject', ''),
(103, 'like_photo_body', ''),
(104, 'comment_blog_subject', ''),
(105, 'comment_blog_body', ''),
(106, 'like_blog_subject', ''),
(107, 'like_blog_body', ''),
(108, 'comment_event_subject', ''),
(109, 'comment_event_body', ''),
(110, 'like_event_subject', ''),
(111, 'like_event_body', ''),
(112, 'new_event_subject', ''),
(113, 'new_event_body', ''),
(114, 'profile_view_subject', ''),
(115, 'profile_view_body', ''),
(116, 'group_rank_subject', ''),
(117, 'group_rank_body', ''),
(118, 'alert_reminder_subject', ''),
(119, 'alert_reminder_body', ''),
(120, 'site_newsletter_subject', ''),
(121, 'site_newsletter_body', ''),
(122, 'admin_approval_registration', '0'),
(123, 'event_auto_publish', '0'),
(124, 'blog_auto_publish', '0'),
(125, 'ecards_received_subject', ''),
(126, 'ecards_received_body', ''),
(127, 'waitfor_approval_subject', ''),
(128, 'waitfor_approval_body', ''),
(129, 'approval_req_subject', ''),
(130, 'approval_req_body', ''),
(131, 'account_approved_subject', ''),
(132, 'account_approved_body', ''),
(133, 'override_privacy_settings', '0'),
(134, 'uploaded_video_auto_publish', '0'),
(135, 'uploaded_video_size_limit', '50'),
(136, 'retricted_wordlist', ''),
(137, 'enable_myvideos', '0'),
(138, 'shared_video_subject', ''),
(139, 'shared_video_body', '');

-- --------------------------------------------------------

--
-- Table structure for table `sharedbyme_playlists`
--

CREATE TABLE IF NOT EXISTS `sharedbyme_playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `sharedwithme_playlists`
--

CREATE TABLE IF NOT EXISTS `sharedwithme_playlists` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `playlist_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `skins`
--

CREATE TABLE IF NOT EXISTS `skins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `color` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `font_family` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `font_size` int(10) NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `spam_challenges`
--

CREATE TABLE IF NOT EXISTS `spam_challenges` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `question` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `answers` text COLLATE utf8_unicode_ci NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `status_updates`
--

CREATE TABLE IF NOT EXISTS `status_updates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `blog_create` tinyint(4) NOT NULL DEFAULT '1',
  `blog_comment` tinyint(4) NOT NULL DEFAULT '1',
  `comment_add` tinyint(4) NOT NULL DEFAULT '1',
  `event_attend` tinyint(4) NOT NULL DEFAULT '1',
  `event_create` tinyint(4) NOT NULL DEFAULT '1',
  `friend_add` tinyint(4) NOT NULL DEFAULT '1',
  `group_create` tinyint(4) NOT NULL DEFAULT '1',
  `group_join` tinyint(4) NOT NULL DEFAULT '1',
  `like_add` tinyint(4) NOT NULL DEFAULT '1',
  `photos_add` tinyint(4) NOT NULL DEFAULT '1',
  `photos_tag` tinyint(4) NOT NULL DEFAULT '1',
  `topic_create` tinyint(4) NOT NULL DEFAULT '1',
  `video_add` tinyint(4) NOT NULL DEFAULT '1',
  `wall_post` tinyint(4) NOT NULL DEFAULT '1',
  `wall_post_link` tinyint(4) NOT NULL DEFAULT '1',
  `user_create` tinyint(4) NOT NULL DEFAULT '1',
  `user_avatar` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `status_updates`
--

INSERT INTO `status_updates` (`id`, `blog_create`, `blog_comment`, `comment_add`, `event_attend`, `event_create`, `friend_add`, `group_create`, `group_join`, `like_add`, `photos_add`, `photos_tag`, `topic_create`, `video_add`, `wall_post`, `wall_post_link`, `user_create`, `user_avatar`) VALUES
(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `target_id` int(10) unsigned NOT NULL,
  `type` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `tag` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  PRIMARY KEY (`id`),
  KEY `type` (`type`),
  KEY `tag` (`tag`),
  KEY `target_id` (`target_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `themes`
--

CREATE TABLE IF NOT EXISTS `themes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `core` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `themes`
--

INSERT INTO `themes` (`id`, `key`, `name`, `core`) VALUES
(3, 'mobile', 'Mobile Theme', 1),
(4, 'jcoterie', 'jCoterie', 1);

-- --------------------------------------------------------

--
-- Table structure for table `topics`
--

CREATE TABLE IF NOT EXISTS `topics` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` smallint(5) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `last_post` datetime NOT NULL,
  `comment_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `lastposter_id` int(10) unsigned NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `group_id` int(10) unsigned NOT NULL,
  `pinned` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `attachment` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `announcement` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `topic_category_id` (`category_id`),
  KEY `lastposter_id` (`lastposter_id`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_group_likes`
--

CREATE TABLE IF NOT EXISTS `user_group_likes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `num_likes` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_user_like_key` (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_group_posts`
--

CREATE TABLE IF NOT EXISTS `user_group_posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  `num_posts` smallint(5) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_user_post_key` (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` tinyint(3) unsigned NOT NULL DEFAULT '2',
  `security_question_id` tinyint(3) NOT NULL,
  `security_answers` text CHARACTER SET utf8 NOT NULL,
  `avatar` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `last_login` datetime NOT NULL,
  `photo_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `friend_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `notification_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `friend_request_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `blog_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `topic_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `conversation_user_count` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `video_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `gender` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `birthday` date NOT NULL,
  `active` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `confirm_status` tinyint(3) NOT NULL DEFAULT '0',
  `confirmed` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `code` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `notification_email` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `timezone` varchar(64) COLLATE utf8_unicode_ci NOT NULL,
  `ip_address` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `username` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `about` text COLLATE utf8_unicode_ci NOT NULL,
  `featured` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `lang` varchar(4) COLLATE utf8_unicode_ci NOT NULL,
  `hide_online` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `cover` varchar(256) COLLATE utf8_unicode_ci NOT NULL,
  `luvs` float NOT NULL DEFAULT '0',
  `profile_views` int(10) unsigned NOT NULL DEFAULT '0',
  `disabled_name` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_username` varchar(128) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `disabled_cover` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `approved_by_admin` tinyint(4) DEFAULT '0',
  `no_word_restriction` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `email` (`email`),
  KEY `gender` (`gender`),
  KEY `active` (`active`),
  KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `last_name`, `email`, `password`, `role_id`, `security_question_id`, `security_answers`, `avatar`, `photo`, `created`, `last_login`, `photo_count`, `friend_count`, `notification_count`, `friend_request_count`, `blog_count`, `topic_count`, `conversation_user_count`, `video_count`, `gender`, `birthday`, `active`, `confirm_status`, `confirmed`, `code`, `notification_email`, `timezone`, `ip_address`, `privacy`, `username`, `url`, `about`, `featured`, `lang`, `hide_online`, `cover`, `luvs`, `profile_views`, `disabled_name`, `disabled_username`, `disabled_avatar`, `disabled_photo`, `disabled_cover`, `approved_by_admin`, `no_word_restriction`) VALUES
(1, 'animesh', 'biswas', 'animesh.ruet@gmail.com', '$2y$10$pmzQYJFU9dGkmo24DfxGw.GFW/l4WLjnJLBgc1XzrwklWRT7la07S', 1, 0, '', '', '', '2014-08-05 20:21:52', '0000-00-00 00:00:00', 0, 0, 0, 0, 0, 0, 0, 0, 'Male', '2014-08-05', 1, 0, 1, '430d58464df73cd862ccd49a11184d90', 1, 'Africa/Abidjan', '', 1, '', '', '', 0, '', 0, '', 0, 0, NULL, NULL, NULL, NULL, NULL, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `videos`
--

CREATE TABLE IF NOT EXISTS `videos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `category_id` smallint(5) unsigned NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `thumb` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `source_id` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `like_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `dislike_count` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL,
  `privacy` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `group_id` int(10) unsigned NOT NULL,
  `playlist_id` int(11) DEFAULT '0',
  `position_order` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `user_id` (`user_id`),
  KEY `category_id` (`category_id`),
  KEY `privacy` (`privacy`),
  KEY `group_id` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_positions`
--

CREATE TABLE IF NOT EXISTS `widget_positions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `parentbox` varchar(50) NOT NULL,
  `positions` varchar(500) NOT NULL,
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_settings`
--

CREATE TABLE IF NOT EXISTS `widget_settings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL DEFAULT '5',
  `photos` int(11) NOT NULL,
  `friends` int(11) NOT NULL DEFAULT '5',
  `videos` int(11) NOT NULL DEFAULT '5',
  `blogs` int(11) NOT NULL DEFAULT '5',
  `groups` int(11) NOT NULL DEFAULT '5',
  `wallone` int(11) NOT NULL DEFAULT '5',
  `status` int(11) NOT NULL DEFAULT '5',
  `ecard` int(11) NOT NULL DEFAULT '5',
  `created` datetime NOT NULL,
  `modified` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widget_types`
--

CREATE TABLE IF NOT EXISTS `widget_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `widgets`
--

CREATE TABLE IF NOT EXISTS `widgets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `widget_type_id` int(10) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `created` datetime NOT NULL,
  `order` smallint(5) unsigned NOT NULL DEFAULT '1',
  `wallposition` int(11) unsigned DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `winks`
--

CREATE TABLE IF NOT EXISTS `winks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `sender_id` int(10) unsigned NOT NULL,
  `recipient_id` int(10) unsigned NOT NULL,
  `tstamp` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;
--
-- Database: `900`
--
--
-- Database: `bbphoto`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_19_053955_create_photos_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE IF NOT EXISTS `photos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=48 ;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`id`, `title`, `description`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Ratione est sint culpa nihil officiis ullam aspernatur.', 'Cheshire Cat, she was appealed to by the English, who wanted leaders, and had to leave it behind?'' She said the Caterpillar. ''I''m afraid I am, sir,'' said Alice; ''I can''t remember half of anger, and tried to open it; but, as the question was evidently meant for her. ''Yes!'' shouted Alice. ''Come on, then,'' said Alice, who had got its neck nicely straightened out, and was just in time to go, for the next witness.'' And he added in an offended tone, ''was, that the reason they''re called lessons,'' the Gryphon repeated impatiently: ''it begins "I passed by his garden."'' Alice did not get dry very soon. ''Ahem!'' said the Mouse. ''Of course,'' the Gryphon remarked: ''because they lessen from day to day.'' This was such a dear little puppy it was!'' said Alice, ''how am I then? Tell me that first, and then added them up, and began to cry again, for she thought, ''it''s sure to make out that it had fallen into a tree. By the use of this elegant thimble''; and, when it saw mine coming!'' ''How do you know I''m.', 'http://lorempixel.com/640/480/cats/?80253', '2015-01-19 00:07:08', '2015-01-19 00:07:08'),
(2, 'animesh biswas', 'March Hare went on. ''Or would you tell me,'' said Alice, ''and why it is right?'' ''In my youth,'' Father William replied to his ear. Alice considered a little, half expecting to see how the Dodo in an offended tone. And the Gryphon whispered in reply, ''for fear they should forget them before the officer could get to twenty at that rate! However, the Multiplication Table doesn''t signify: let''s try the first to speak. ''What size do you know what they''re like.'' ''I believe so,'' Alice replied eagerly, for she was dozing off, and Alice was silent. The King and the roof off.'' After a time there could be NO mistake about it: it was YOUR table,'' said Alice; ''I can''t explain MYSELF, I''m afraid, sir'' said Alice, whose thoughts were still running on the glass table and the March Hare interrupted, yawning. ''I''m getting tired of being upset, and their curls got entangled together. Alice laughed so much already, that it had made. ''He took me for his housemaid,'' she said this, she looked up, but it was.', '/bbphoto/public/image/bYXXzS_p1.jpeg', '2015-01-19 00:07:08', '2015-02-18 00:56:22'),
(6, 'Nesciunt consequatur explicabo eligendi.', 'Hatter. He had been broken to pieces. ''Please, then,'' said the Dormouse, who seemed to be executed for having missed their turns, and she hastily dried her eyes to see if she could not make out what she was coming to, but it was a bright idea came into Alice''s head. ''Is that the best way to hear the rattle of the deepest contempt. ''I''ve seen hatters before,'' she said to Alice, that she had never been in a low curtain she had succeeded in bringing herself down to look at a king,'' said Alice. ''Nothing WHATEVER?'' persisted the King. ''Nothing whatever,'' said Alice. ''I don''t like it, yer honour, at all, as the March Hare and the Dormouse began in a wondering tone. ''Why, what a Mock Turtle is.'' ''It''s the stupidest tea-party I ever saw in my time, but never ONE with such a fall as this, I shall have to ask the question?'' said the Cat remarked. ''Don''t be impertinent,'' said the King, ''that saves a world of trouble, you know, with oh, such long curly brown hair! And it''ll fetch things when you.', 'http://lorempixel.com/640/480/cats/?20504', '2015-01-19 00:07:08', '2015-01-19 00:07:08'),
(7, 'Iste explicabo nulla in doloribus amet voluptate fugit.', 'If she should chance to be a very pretty dance,'' said Alice to herself. ''I dare say there may be ONE.'' ''One, indeed!'' said Alice, and she went on, ''you throw the--'' ''The lobsters!'' shouted the Gryphon, and the pool a little way off, panting, with its head, it WOULD twist itself round and swam slowly back again, and made believe to worry it; then Alice, thinking it was perfectly round, she came upon a low voice, ''Why the fact is, you see, so many out-of-the-way things to happen, that it signifies much,'' she said these words her foot slipped, and in another moment, splash! she was surprised to see if there are, nobody attends to them--and you''ve no idea how to set about it; and as he spoke, ''we were trying--'' ''I see!'' said the Gryphon, with a table in the world am I? Ah, THAT''S the great concert given by the little glass box that was trickling down his brush, and had just begun to repeat it, but her voice close to them, they set to work shaking him and punching him in the kitchen that.', 'http://lorempixel.com/640/480/cats/?88648', '2015-01-19 00:07:08', '2015-01-19 00:07:08'),
(44, '33', '33', '/bbphoto/public/image/rZV9PC_p2.jpeg', '2015-01-21 02:05:14', '2015-01-21 02:05:14'),
(45, '666', '666', '/bbphoto/public/image/aTfHFm_p1.jpeg', '2015-01-21 02:06:15', '2015-01-21 02:06:15'),
(47, '44', '44', '/bbphoto/public/image/bM9lIJ_p2.jpeg', '2015-01-21 02:07:54', '2015-01-21 02:07:54');
--
-- Database: `cdcol`
--

-- --------------------------------------------------------

--
-- Table structure for table `cds`
--

CREATE TABLE IF NOT EXISTS `cds` (
  `titel` varchar(200) DEFAULT NULL,
  `interpret` varchar(200) DEFAULT NULL,
  `jahr` int(11) DEFAULT NULL,
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `cds`
--

INSERT INTO `cds` (`titel`, `interpret`, `jahr`, `id`) VALUES
('Beauty', 'Ryuichi Sakamoto', 1990, 1),
('Goodbye Country (Hello Nightclub)', 'Groove Armada', 2001, 4),
('Glee', 'Bran Van 3000', 1997, 5);
--
-- Database: `django`
--
--
-- Database: `django_fitness`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE IF NOT EXISTS `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE IF NOT EXISTS `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=40 ;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can add permission', 2, 'add_permission'),
(5, 'Can change permission', 2, 'change_permission'),
(6, 'Can delete permission', 2, 'delete_permission'),
(7, 'Can add group', 3, 'add_group'),
(8, 'Can change group', 3, 'change_group'),
(9, 'Can delete group', 3, 'delete_group'),
(10, 'Can add user', 4, 'add_user'),
(11, 'Can change user', 4, 'change_user'),
(12, 'Can delete user', 4, 'delete_user'),
(13, 'Can add content type', 5, 'add_contenttype'),
(14, 'Can change content type', 5, 'change_contenttype'),
(15, 'Can delete content type', 5, 'delete_contenttype'),
(16, 'Can add session', 6, 'add_session'),
(17, 'Can change session', 6, 'change_session'),
(18, 'Can delete session', 6, 'delete_session'),
(19, 'Can add profile', 7, 'add_profile'),
(20, 'Can change profile', 7, 'change_profile'),
(21, 'Can delete profile', 7, 'delete_profile'),
(22, 'Can add assessment', 8, 'add_assessment'),
(23, 'Can change assessment', 8, 'change_assessment'),
(24, 'Can delete assessment', 8, 'delete_assessment'),
(25, 'Can add medical', 9, 'add_medical'),
(26, 'Can change medical', 9, 'change_medical'),
(27, 'Can delete medical', 9, 'delete_medical'),
(28, 'Can add content', 10, 'add_content'),
(29, 'Can change content', 10, 'change_content'),
(30, 'Can delete content', 10, 'delete_content'),
(31, 'Can add user assessment', 11, 'add_userassessment'),
(32, 'Can change user assessment', 11, 'change_userassessment'),
(33, 'Can delete user assessment', 11, 'delete_userassessment'),
(34, 'Can add schedule', 12, 'add_schedule'),
(35, 'Can change schedule', 12, 'change_schedule'),
(36, 'Can delete schedule', 12, 'delete_schedule'),
(37, 'Can add snippet', 13, 'add_snippet'),
(38, 'Can change snippet', 13, 'change_snippet'),
(39, 'Can delete snippet', 13, 'delete_snippet');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE IF NOT EXISTS `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$12000$ihp6N2v9TT13$J8AoJungbOsYNtCMsbibQl+cdu5kZJgwmuoJMzV51yI=', '2015-03-07 11:10:17', 0, 'aa', 'animesh', 'aa', 'aa@aa.com', 1, 1, '2015-02-28 15:49:46'),
(2, 'pbkdf2_sha256$12000$uR7hcKePelxe$lGyUxymZpu+Io7PC2+uHEUNi7e2UUic6hOr0erq+oeA=', '2015-03-01 06:08:05', 0, 'bb', 'bb', 'bb', 'bb@bb.com', 0, 1, '2015-03-01 06:08:05'),
(3, 'pbkdf2_sha256$12000$3q34k8wjzQxA$kXEwyIRmRdUUY3VUScudHLwpvz2z0FfZ02WwaxyEQ1s=', '2015-03-01 06:08:41', 0, 'c', 'cc', 'c', 'cc@cc.com', 0, 1, '2015-03-01 06:08:41');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE IF NOT EXISTS `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE IF NOT EXISTS `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE IF NOT EXISTS `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE IF NOT EXISTS `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `name`, `app_label`, `model`) VALUES
(1, 'log entry', 'admin', 'logentry'),
(2, 'permission', 'auth', 'permission'),
(3, 'group', 'auth', 'group'),
(4, 'user', 'auth', 'user'),
(5, 'content type', 'contenttypes', 'contenttype'),
(6, 'session', 'sessions', 'session'),
(7, 'profile', 'fitness', 'profile'),
(8, 'assessment', 'fitness', 'assessment'),
(9, 'medical', 'fitness', 'medical'),
(10, 'content', 'fitness', 'content'),
(11, 'user assessment', 'fitness', 'userassessment'),
(12, 'schedule', 'fitness', 'schedule'),
(13, 'snippet', 'snippets', 'snippet');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE IF NOT EXISTS `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2015-02-28 15:08:13'),
(2, 'auth', '0001_initial', '2015-02-28 15:08:17'),
(3, 'admin', '0001_initial', '2015-02-28 15:08:18'),
(4, 'fitness', '0001_initial', '2015-02-28 15:08:20'),
(5, 'sessions', '0001_initial', '2015-02-28 15:08:20'),
(6, 'fitness', '0002_auto_20150228_1816', '2015-02-28 18:16:29'),
(7, 'fitness', '0002_auto_20150228_1820', '2015-02-28 18:20:34'),
(8, 'fitness', '0003_schedule', '2015-03-03 16:43:34'),
(9, 'fitness', '0004_auto_20150303_1837', '2015-03-03 18:37:10'),
(10, 'fitness', '0005_auto_20150304_0625', '2015-03-04 06:25:56'),
(11, 'snippets', '0001_initial', '2015-03-04 18:34:06'),
(12, 'snippets', '0002_auto_20150305_0915', '2015-03-05 09:17:37');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_session`
--

INSERT INTO `django_session` (`session_key`, `session_data`, `expire_date`) VALUES
('03b6u521qrg90mohcwvxo9y58u4w4s26', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:31:32'),
('0u498pscqn0enypswru44ylr2nu6jkue', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:32:25'),
('124m5f7wbmw1kwr53fmny6w9fcye4cs6', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:12:45'),
('1lhhzcy1rak3p5dxx9uh9y85vtedlgq9', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:39:02'),
('1wdgz6wv0ws58xh8yib4cyr4ibzyse8d', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:13:56'),
('3cfvo46p6d48suo5a284bt5f9iga54ms', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:48:43'),
('3ve4kyq9l0g288p9k0tvcd8givngefsk', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-18 16:26:32'),
('3xbc4zzege5nqz5d1lqxt40u24qje9im', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-14 16:09:52'),
('44cjcqzo3zokaghfpgatyapbf9wqfpbt', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:37:29'),
('5em3dtixhejr7cdrnzk5nxgbupb42gu2', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 14:50:04'),
('5nywyettnywbxk3k9kp82iye39m3hj5j', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 07:26:55'),
('5o33i9676hg2nmjfjxdrscx2e4fybtey', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 07:44:32'),
('6fzn1t802yjvg0iveqf40i5aj7mkwz97', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 18:42:44'),
('6kc6g9bimvu9kogs30a3vhgigu3hwca1', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:30:18'),
('6ylvzed1mhbkiia6mnosp9vvam1mqk0x', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:52:37'),
('71num69nrvs6j22e8eov6fp3xzdndk8g', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 18:43:43'),
('73kbf9yp5f59zy3wicizjnbsb85smr1p', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:33:12'),
('7ohjlyirvg3bxhxcxnpmxxqhyaazs314', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:50:52'),
('7ywmollq5n5ktrmo1s1ebvn6ogvx2ihj', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:51:24'),
('87gqe0vtgdcjy8l1kufhyf9ediu9wws8', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 07:28:29'),
('8m05qj0txngbzt76f38kau0anhrcvbwo', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:30:49'),
('8o9wd6t9tecpn1ofagjerh5y8bdfj4n4', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:07:21'),
('9egd9wkl524a2jxylhy10yjnmacggoh4', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:50:12'),
('9gc93uawlo6rujceilqvv5fedyxm5sy5', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-14 17:47:33'),
('9vcb17ypx9ty7tmdig1vhdkfavnevuo6', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:26:20'),
('9zscsmjq1dlabmtmhc5ffj4bipba5kc8', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:51:41'),
('ao826kpnhpcpsmi6ocfargi4a6k8qatr', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 18:30:43'),
('b0h8nbn8tz1djbc686a0h60xkmtpyre7', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 14:42:43'),
('ck49e09rbfqaxebhuakupa0pvo93dipn', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:19:14'),
('cl7v8l2o0hr33o7fm3f3d3oig2h115ok', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:48:26'),
('e3cgpa2wqf2rpuroi7d0tfghuybptgo5', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 07:29:34'),
('e6jmp5z9p7441xqcl8973w1ogs0bp2d6', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 14:53:40'),
('eeaaa7yvdkydny6q0gl0xs5kfbxlea9n', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:29:40'),
('el23o9l4crlt19hwt6ecnobz28k3s1zu', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:03:13'),
('eshnbiruiw4lluivwebh18wuvlqbxnj7', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:37:55'),
('fmoj0vahzqvd5r1wrpadtkiveawggbv9', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:10:45'),
('ghf9nvs1u9k36xaiqmlkiwqiyj0lf13n', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:11:34'),
('hdu95ho2dzozvm7da1owmrcvf65jfqw0', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:36:17'),
('hg59ahj8lvxxpzmx9teokua8gms4cmj1', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:51:43'),
('hq6544voaj7fcwfg2jjkhgv4wgu2nbil', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 14:57:55'),
('hrq6q6b9lk522rld513h9npwvxitcmg4', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 11:08:41'),
('hxvmm7krxudrmzb84kzppvggal1vgri4', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 19:47:40'),
('i4iaaea2wu4zo72uss2cs8o3glfp9jiz', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 11:25:30'),
('j9ajfoq5xtkej7gkpjiqa2g668uhsstf', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:34:45'),
('jb0w1ajhw5v6sy6k1a7ymtt9w14f8r06', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:34:55'),
('jhd067mdquoxxubbb7qb5scqfkkczevo', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-14 16:09:30'),
('jobyu18o85fkhedn631o0t08zkyaas3x', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 14:41:39'),
('joc56xe0b153ojhcusyuu979c88orxsx', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:45:21'),
('jueouq392ikz466c575jkw9cvkhls38u', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 20:23:43'),
('jymt7wrzlibb9z1gz7bqhp3tcsbmfrtz', 'NjAzYjMyMjQ3ZDU0N2U1Yzg5NzBiNjJhNDJlOGY0Y2UzYjBiZWU5MTp7Il9hdXRoX3VzZXJfaGFzaCI6IjNmMDRlNDU2N2JkZWUyNTk3YTk2OTUyODlhNjQ3ODAwZDI1Y2Y5MTUiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9', '2015-03-21 11:10:17'),
('k723402hnc8dxcbf2s9n774zk8a02e2o', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:02:47'),
('kak3hoj703v7pbiow5iicu95p12a8b11', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:37:34'),
('kqpqufq04spv8tw07pfnh000br5946ff', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:34:26'),
('ktqz4vypgt5jaqjud5jlb9hkw1au1fmc', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:44:26'),
('l3tav8b9uftf8jiztzs72f67zqa7t2fv', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:47:49'),
('lic9zb223srw8zu913q9l5v1l4g2decf', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 18:43:43'),
('lz77jwlgjajmty10nb1mki6rmlv2u5qe', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:33:01'),
('m85uyh6r0qvy795z7ovov5t2pm9b5xj6', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:14:16'),
('ma0dfyvyjzmomh6be5sfd8ogu6mmrd7t', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 11:05:31'),
('mfv1211wre0xxghtmqsffsy0xc8hwx5o', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 11:10:01'),
('mtmw0sk2em8ruzk4lj82vuf2nw6i2stl', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 18:31:58'),
('nn96fzqz2bqjs1znnitopep4wwlejp3t', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 11:27:36'),
('opk1ifa35uhbc5zuw97yzaoouynnj5tb', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:34:30'),
('ovu08e77iy494t24gwu8jc2la2nsmijk', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:18:37'),
('pxm8d8nbetvjgrw7am8jmu51djxvf7xr', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:51:45'),
('r7v6xj90qvplg9mcx6i0p9y1lqjswb4t', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 14:43:30'),
('rd48gbyeh7jzsnlbrsrgi1lz427hi24s', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:34:32'),
('ruprfmt5ox2u51jar0oa1lllcxf8hd52', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:36:32'),
('sd20gtd4d8odpnif3ksqmlmms8xrvm84', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 18:33:31'),
('st4aiaofp8cuji7hp6wpmf1xdawj92ir', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:48:11'),
('td5pqpeeiv6rhytmbtfy56oef4xoxv8r', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:48:29'),
('ttv1a9pg1awkqzhkjydwfbjgidl5ecbr', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:37:03'),
('umcg6cprjajiljn04tzvjqpbq9nuhw9d', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 18:54:57'),
('uvu7y836heyabr1x0mb0kdw8y5scmo62', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:25:49'),
('ux4ks96x422wzhtabsgzw93hi1evo63d', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:36:40'),
('wl8w79hs4dl1j14qv50h55l1jm84aodm', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 20:22:42'),
('x3o6wtttfvhrqfs5vp54s4bmn4554lcl', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:02:57'),
('xgcp20m3ha3jqqam5h178zp3nhm97b2i', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-15 14:12:29'),
('xl7ixtvohw073nrszqn67lc48hqokogw', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:12:40'),
('xsc7y9lhdbl8if9x1aaibmi1ccifmtht', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-21 09:36:03'),
('xxfs4uij3ix9a4y4fa2ockizf2ojq762', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 07:26:13'),
('ymgi4p2g4fpuhgq0en7z1ngmmwrrno21', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 19:07:30'),
('yrp39e4994r3z8j9gmtkafjes35j6q79', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:39:14'),
('z5y3dri76f2ddkq2fmkehmxg5nwv4uk7', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 06:33:35'),
('zcqk0o6x4p3io5tm1yfo1cfqlpsc48l1', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-17 07:29:25'),
('zipdsx6obw1c6zx7ludm47f7xj4bqdt0', 'YzU5Njk3MDc3MjA5ZjkwNjVjMzZlNTI2ZWE3Y2YwMTQ2YjIzYzhhMjp7fQ==', '2015-03-16 11:29:43');

-- --------------------------------------------------------

--
-- Table structure for table `fitness_assessment`
--

CREATE TABLE IF NOT EXISTS `fitness_assessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `unit` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `fitness_assessment`
--

INSERT INTO `fitness_assessment` (`id`, `name`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'quod', 12, '2015-02-16 07:56:25', '2015-03-04 07:49:05'),
(2, 'iusto', 6, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(3, 'totam', 9, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(4, 'quod', 7, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(5, 'dolorem', 8, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(6, 'ad', 5, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(8, 'et', 9, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(9, 'est', 10, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(10, 'omnis', 9, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(11, 'animesh', 10, '2015-02-16 07:59:32', '2015-02-16 07:59:32');

-- --------------------------------------------------------

--
-- Table structure for table `fitness_content`
--

CREATE TABLE IF NOT EXISTS `fitness_content` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(15) NOT NULL,
  `body` longtext NOT NULL,
  `url` varchar(200) NOT NULL,
  `types` int(11) NOT NULL,
  `created` datetime NOT NULL,
  `updated` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=11 ;

--
-- Dumping data for table `fitness_content`
--

INSERT INTO `fitness_content` (`id`, `title`, `body`, `url`, `types`, `created`, `updated`) VALUES
(1, 'Molestiae dicta', 'Maxime facere consequatur eum qui animi.', 'http://Stracke.info/illum-non-aperiam-dolor-illo-reiciendis-cum', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(3, 'Et possimus vel', 'Delectus ex repudiandae voluptatem voluptatum.', 'http://www.Leuschke.com/quia-in-soluta-tenetur-dolorum-in', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(4, 'Non perspiciati', 'Eius omnis natus omnis eos maiores.', 'http://Quitzon.com/libero-laboriosam-aliquam-atque-quo-aut-quia-inventore-cumque.html', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(5, 'animesh', 'Et sit enim iusto.', 'http://www.Hyatt.com/', 1, '2015-02-16 07:56:24', '2015-02-16 07:58:58'),
(6, 'Officiis eos.', 'Vero quas qui illo dicta autem aspernatur aut.', 'http://Bruen.com/', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(7, 'Sit veritatis.', 'Eligendi incidunt excepturi dolores sequi.', 'http://Rosenbaum.com/et-voluptas-numquam-sit-iste', 1, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(8, 'Et reiciendis q', 'Accusamus dolores eligendi sapiente sunt aut.', 'https://www.Gleichner.info/sunt-non-non-adipisci-tempore-iusto', 1, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(10, 'Ex accusamus.', 'Qui nesciunt quod ea ut hic est aut.', 'http://Haag.biz/', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24');

-- --------------------------------------------------------

--
-- Table structure for table `fitness_medical`
--

CREATE TABLE IF NOT EXISTS `fitness_medical` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(15) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `fitness_medical`
--

INSERT INTO `fitness_medical` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Cupiditate saep', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(2, 'Quisquam sint.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(3, 'Neque molestiae', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(4, 'Ea non quia.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(5, 'Sit itaque sit.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(6, 'Nisi quas.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(7, 'Ut unde rerum.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(8, 'Sit aut repella', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(9, 'Et reprehenderi', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(10, 'Rerum eos.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(11, 'disco', '2015-02-16 08:03:06', '2015-02-16 08:03:06'),
(12, 'lorem', '2015-02-28 17:50:02', '2015-02-28 17:50:02'),
(13, 'animesh', '2015-03-07 09:29:29', '2015-03-07 09:29:29');

-- --------------------------------------------------------

--
-- Table structure for table `fitness_medical_users`
--

CREATE TABLE IF NOT EXISTS `fitness_medical_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `medical_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `medical_id` (`medical_id`,`user_id`),
  KEY `fitness_medical_users_795df9e0` (`medical_id`),
  KEY `fitness_medical_users_e8701ad4` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `fitness_medical_users`
--

INSERT INTO `fitness_medical_users` (`id`, `medical_id`, `user_id`) VALUES
(12, 2, 2),
(15, 2, 3),
(11, 3, 2),
(14, 3, 3),
(13, 4, 2),
(22, 12, 1),
(21, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `fitness_profile`
--

CREATE TABLE IF NOT EXISTS `fitness_profile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `regId` varchar(15) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `birthdate` date NOT NULL,
  `address1` longtext NOT NULL,
  `phone` varchar(15) NOT NULL,
  `mobile` varchar(15) NOT NULL,
  `address2` longtext NOT NULL,
  `city` varchar(10) NOT NULL,
  `postcode` varchar(10) NOT NULL,
  `state` varchar(10) NOT NULL,
  `country` varchar(10) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `fitness_profile`
--

INSERT INTO `fitness_profile` (`id`, `regId`, `gender`, `birthdate`, `address1`, `phone`, `mobile`, `address2`, `city`, `postcode`, `state`, `country`, `created_at`, `updated_at`, `user_id`) VALUES
(1, '123', 'male', '2015-02-28', 'aa', 'aa', 'aa', 'aa', 'aa', 'aa', 'aus', 'au', '2015-02-28 15:49:46', '2015-03-07 11:10:27', 1),
(2, 'bb', 'male', '2015-03-01', 'bb', 'b', 'b', 'b', 'b', 'b', 'bb', 'b', '2015-03-01 06:08:06', '2015-03-01 06:08:06', 2),
(3, 'cc', 'male', '2015-03-01', 'c', 'c', 'c', 'cc', 'c', 'c', 'cc', 'c', '2015-03-01 06:08:41', '2015-03-01 06:08:41', 3);

-- --------------------------------------------------------

--
-- Table structure for table `fitness_schedule`
--

CREATE TABLE IF NOT EXISTS `fitness_schedule` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `prev_date` date NOT NULL,
  `prev_time` time DEFAULT NULL,
  `schedule_date` date NOT NULL,
  `schedule_time` time DEFAULT NULL,
  `place` varchar(15) NOT NULL,
  `sent_to` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `fitness_schedule`
--

INSERT INTO `fitness_schedule` (`id`, `prev_date`, `prev_time`, `schedule_date`, `schedule_time`, `place`, `sent_to`) VALUES
(2, '2015-03-17', '00:15:00', '2015-04-30', '00:45:00', 'dhk', 'bb@bb.com,aa@aa.com'),
(3, '2015-03-19', '00:15:00', '2015-04-29', '00:15:00', 'ctg', 'all');

-- --------------------------------------------------------

--
-- Table structure for table `fitness_userassessment`
--

CREATE TABLE IF NOT EXISTS `fitness_userassessment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `assessment_date` date NOT NULL,
  `assessment_time` time NOT NULL,
  `assessent_unit` int(11) NOT NULL,
  `assessment_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fitness_userassessment_a4079fcf` (`assessment_id`),
  KEY `fitness_userassessment_e8701ad4` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=359 ;

--
-- Dumping data for table `fitness_userassessment`
--

INSERT INTO `fitness_userassessment` (`id`, `assessment_date`, `assessment_time`, `assessent_unit`, `assessment_id`, `user_id`) VALUES
(309, '2015-03-31', '00:45:00', 7, 1, 1),
(310, '2015-03-31', '00:45:00', 9, 10, 1),
(311, '2015-03-31', '00:45:00', 3, 3, 1),
(312, '2015-03-31', '00:45:00', 200, 11, 1),
(313, '2015-03-31', '00:45:00', 6, 6, 1),
(314, '2015-03-31', '00:45:00', 10, 1, 1),
(315, '2015-03-31', '00:45:00', 5, 5, 1),
(316, '2015-03-31', '00:45:00', 8, 9, 1),
(317, '2015-03-31', '00:45:00', 4, 4, 1),
(318, '2015-03-31', '00:45:00', 2, 2, 1),
(339, '2015-03-12', '08:15:00', 5, 11, 1),
(340, '2015-03-12', '08:15:00', 5, 6, 1),
(341, '2015-03-12', '08:15:00', 55, 4, 1),
(342, '2015-03-12', '08:15:00', 0, 2, 2),
(343, '2015-03-12', '08:15:00', 55, 10, 1),
(344, '2015-03-12', '08:15:00', 0, 3, 1),
(345, '2015-03-12', '08:15:00', 5, 8, 1),
(346, '2015-03-12', '08:15:00', 100, 1, 1),
(347, '2015-03-12', '08:15:00', 5, 9, 1),
(348, '2015-03-12', '08:15:00', 0, 5, 2),
(349, '2015-03-12', '08:15:00', 0, 11, 2),
(350, '2015-03-12', '08:15:00', 0, 4, 2),
(351, '2015-03-12', '08:15:00', 0, 8, 2),
(352, '2015-03-12', '08:15:00', 0, 10, 2),
(353, '2015-03-12', '08:15:00', 5, 5, 1),
(354, '2015-03-12', '08:15:00', 0, 1, 2),
(355, '2015-03-12', '08:15:00', 0, 6, 2),
(356, '2015-03-12', '08:15:00', 5, 2, 1),
(357, '2015-03-12', '08:15:00', 0, 9, 2),
(358, '2015-03-12', '08:15:00', 0, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `snippets_snippet`
--

CREATE TABLE IF NOT EXISTS `snippets_snippet` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `created` datetime NOT NULL,
  `title` varchar(100) NOT NULL,
  `code` longtext NOT NULL,
  `linenos` tinyint(1) NOT NULL,
  `language` varchar(100) NOT NULL,
  `style` varchar(100) NOT NULL,
  `highlighted` longtext NOT NULL,
  `owner_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `snippets_snippet_5e7b1936` (`owner_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `snippets_snippet`
--

INSERT INTO `snippets_snippet` (`id`, `created`, `title`, `code`, `linenos`, `language`, `style`, `highlighted`, `owner_id`) VALUES
(1, '2015-03-04 18:44:04', '', 'foo = "bar"\n', 0, 'python', 'friendly', '1', 1),
(2, '2015-03-04 18:44:15', '', 'print "hello, world"\n', 0, 'python', 'friendly', '1', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `fitness_medical_users`
--
ALTER TABLE `fitness_medical_users`
  ADD CONSTRAINT `fitness_medical_users_user_id_61cd526d950ef3ef_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `fitness_medica_medical_id_15a1a6c73c9ccaec_fk_fitness_medical_id` FOREIGN KEY (`medical_id`) REFERENCES `fitness_medical` (`id`);

--
-- Constraints for table `fitness_profile`
--
ALTER TABLE `fitness_profile`
  ADD CONSTRAINT `fitness_profile_user_id_702f2017bad60d86_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `fitness_userassessment`
--
ALTER TABLE `fitness_userassessment`
  ADD CONSTRAINT `fitness_userassessment_user_id_7e68017490164e41_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  ADD CONSTRAINT `fitness__assessment_id_495aab902aa77a20_fk_fitness_assessment_id` FOREIGN KEY (`assessment_id`) REFERENCES `fitness_assessment` (`id`);

--
-- Constraints for table `snippets_snippet`
--
ALTER TABLE `snippets_snippet`
  ADD CONSTRAINT `snippets_snippet_owner_id_39e63c80a2b9f50a_fk_auth_user_id` FOREIGN KEY (`owner_id`) REFERENCES `auth_user` (`id`);
--
-- Database: `kfc`
--

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE IF NOT EXISTS `participants` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT NULL,
  `email` varchar(50) NOT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `tnc` tinyint(1) NOT NULL DEFAULT '0',
  `optIn` tinyint(1) NOT NULL DEFAULT '0',
  `created` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=115 ;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `name`, `email`, `phone`, `tnc`, `optIn`, `created`) VALUES
(1, '', 'test@test.com', NULL, 0, 0, '2014-11-06 13:59:36'),
(114, 'ani', 'ani@ani.com', NULL, 1, 0, '2015-02-24 08:01:27');

-- --------------------------------------------------------

--
-- Table structure for table `questions`
--

CREATE TABLE IF NOT EXISTS `questions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `question` varchar(200) NOT NULL DEFAULT '0',
  `customercomment` tinyint(4) DEFAULT NULL,
  `dependentoption` varchar(255) DEFAULT NULL,
  `displaytype` tinyint(1) DEFAULT NULL,
  `options` text NOT NULL COMMENT 'JSON data with a,b,c and answers',
  `stepno` int(11) DEFAULT NULL,
  `sequence` tinyint(2) DEFAULT NULL,
  `dependent_show_opt` varchar(50) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=13 ;

--
-- Dumping data for table `questions`
--

INSERT INTO `questions` (`id`, `question`, `customercomment`, `dependentoption`, `displaytype`, `options`, `stepno`, `sequence`, `dependent_show_opt`, `created`, `modified`) VALUES
(1, 'Are you?', NULL, NULL, 1, '["Male","Female"]', 1, 1, NULL, '2014-10-22 07:24:59', '2014-10-22 07:24:59'),
(2, 'What age are you?', NULL, NULL, 1, '["13-17","18-24","25-34","35-44", "45+"]', 1, 2, NULL, NULL, NULL),
(3, 'Do you have children?', 0, 'If YES, did you buy go cup for yourself or for a child ? ; Myself ; For a child', 1, '["Yes","No"]', 1, 3, 'Yes', '2014-10-22 08:52:42', '2014-10-22 08:52:42'),
(4, 'What do you think of the new Go Cup? Scale 1-5 (1 being dislike it intensely and 5 being like it extremely)', 0, '', 1, '["1","2","3","4", "5"]', 2, 1, NULL, '2014-10-22 10:01:42', '2014-10-22 10:01:42'),
(5, 'Do you prefer this to the normal snack box?', 1, 'Why?;text', 1, '["Yes","No"]', 2, 2, NULL, '2014-10-22 07:35:49', '2014-10-22 07:35:49'),
(6, 'What did you think about the size of the pack?', 0, '', 1, '["Too small", "Just right", "Too big"]', 2, 3, NULL, '2014-11-03 23:45:40', '2014-11-03 23:45:40'),
(7, 'How long a period was it between buying and eating?', 0, '', 1, '["5 mins","10 mins","15 mins","Longer than 15 mins"]', 3, 1, NULL, '2014-10-22 10:00:26', '2014-10-22 10:00:26'),
(8, 'Where did you eat it?', NULL, NULL, 1, '["In the car", "In the store", "Out & about", "At home", "None of the above"]', 3, 2, NULL, NULL, NULL),
(9, 'How easy was the pack to open? ', 0, '', 0, '["Very easy","A little difficult","Very difficult"]', 4, 1, NULL, '2014-10-22 09:58:51', '2014-10-22 09:58:51'),
(10, 'How easy was the pack to carry around vs the snack box?', 0, '', 0, '["Very easy","A little difficult","Very difficult"]', 4, 2, NULL, NULL, NULL),
(11, 'How easy was the pack to eat from vs the snack box?', NULL, '0', 0, '["Very easy","A little difficult","Very difficult"]', 4, 3, NULL, NULL, NULL),
(12, 'In the future, are you more/neutral/less likely to buy the snack box now it is in the Go Cup?', 1, '', 1, '["More Likely","Neutral","Less Likely"]', 4, 4, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `results`
--

CREATE TABLE IF NOT EXISTS `results` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `question_id` smallint(4) NOT NULL,
  `survey_id` bigint(20) NOT NULL,
  `choice` varchar(2) NOT NULL,
  `dependent_choice` varchar(2) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1025 ;

--
-- Dumping data for table `results`
--

INSERT INTO `results` (`id`, `question_id`, `survey_id`, `choice`, `dependent_choice`, `created`, `modified`) VALUES
(1012, 1, 116, 'A', NULL, '2015-02-24 08:01:34', '2015-02-24 08:01:34'),
(1013, 2, 116, 'B', NULL, '2015-02-24 08:01:34', '2015-02-24 08:01:34'),
(1014, 3, 116, 'B', NULL, '2015-02-24 08:01:35', '2015-02-24 08:01:35'),
(1015, 6, 116, 'B', NULL, '2015-02-24 08:01:41', '2015-02-24 08:01:41'),
(1016, 5, 116, '', '', '2015-02-24 08:01:41', '2015-02-24 08:01:41'),
(1017, 4, 116, 'B', NULL, '2015-02-24 08:01:41', '2015-02-24 08:01:41'),
(1018, 5, 116, 'B', NULL, '2015-02-24 08:01:41', '2015-02-24 08:01:41'),
(1019, 8, 116, 'B', NULL, '2015-02-24 08:01:45', '2015-02-24 08:01:45'),
(1020, 7, 116, 'B', NULL, '2015-02-24 08:01:45', '2015-02-24 08:01:45'),
(1021, 10, 116, 'A', NULL, '2015-02-24 08:01:49', '2015-02-24 08:01:49'),
(1022, 9, 116, 'B', NULL, '2015-02-24 08:01:49', '2015-02-24 08:01:49'),
(1023, 11, 116, 'B', NULL, '2015-02-24 08:01:52', '2015-02-24 08:01:52'),
(1024, 12, 116, 'B', NULL, '2015-02-24 08:01:52', '2015-02-24 08:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `stores`
--

CREATE TABLE IF NOT EXISTS `stores` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `store` varchar(50) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `stores`
--

INSERT INTO `stores` (`id`, `store`, `created`, `modified`) VALUES
(2, 'BELMONT - Cnr Pacific Hwy & Merleview St', NULL, NULL),
(3, 'CESSNOCK - 226 - 228 Wollombi Rd', NULL, NULL),
(4, 'CHARLESTOWN SQUARE - Shop L 2047,Charlestown', NULL, NULL),
(5, 'EAST MAITLAND - Cnr Mollymorgan Dr & Mitchell Dr', NULL, NULL),
(6, 'GLENDALE - Glendale Super Centre', NULL, NULL),
(7, 'GREEN HILLS - Green Hills Shopping Centre', NULL, NULL),
(8, 'JESMOND - Stockland Mall Blue Gum Rd', NULL, NULL),
(9, 'KOTARA - 62 Northcott Dr (Cnr Park Ave)', NULL, NULL),
(10, 'KURRI KURRI - 103-105 Barton Street', NULL, NULL),
(11, 'MAYFIELD - 114 Maitland Road', NULL, NULL),
(12, 'MORISSET - 5 Gateway Boulevard', NULL, NULL),
(13, 'MT. HUTTON - Mt Hutton Shopping Centre Wilson Rd', NULL, NULL),
(14, 'MUSWELLBROOK - 139-145 Maitland Street', NULL, NULL),
(15, 'PALAIS - 684 Hunter Street', NULL, NULL),
(16, 'RAYMOND TERRACE - Cnr Masonite Rd & Pacific Hwy', NULL, '2014-11-05 09:04:27'),
(17, 'RUTHERFORD - Cnr Arthur St & New England Hwy', NULL, NULL),
(18, 'SALAMANDER BAY - The Rigby Centre', NULL, NULL),
(19, 'SINGLETON - 83 - 87 William St & 56 George St', NULL, NULL),
(20, 'WARNERS BAY - 240 Hillsborough Road', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `surveys`
--

CREATE TABLE IF NOT EXISTS `surveys` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `participant_id` bigint(20) DEFAULT NULL COMMENT 'if no participant id then this was incomplete',
  `code` varchar(50) NOT NULL,
  `exp_date` varchar(20) DEFAULT NULL,
  `used` tinyint(1) NOT NULL DEFAULT '0',
  `store_id` bigint(20) NOT NULL,
  `nfc` varchar(50) DEFAULT NULL,
  `opt_in` tinyint(1) NOT NULL DEFAULT '0',
  `tnc` tinyint(1) NOT NULL DEFAULT '0',
  `completed` tinyint(1) NOT NULL DEFAULT '0',
  `redeemed_open` int(11) NOT NULL DEFAULT '0',
  `edm_opened` tinyint(1) NOT NULL DEFAULT '0',
  `redeem_store_id` int(11) NOT NULL,
  `created` datetime DEFAULT NULL,
  `modified` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=117 ;

--
-- Dumping data for table `surveys`
--

INSERT INTO `surveys` (`id`, `participant_id`, `code`, `exp_date`, `used`, `store_id`, `nfc`, `opt_in`, `tnc`, `completed`, `redeemed_open`, `edm_opened`, `redeem_store_id`, `created`, `modified`) VALUES
(116, 114, '1146072', '2014-11-30', 0, 4, '0', 0, 1, 1, 0, 0, 0, '2015-02-24 08:01:27', '2015-02-24 08:01:52');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) DEFAULT NULL,
  `password` varchar(50) DEFAULT NULL,
  `role` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'admin', '114624d6268059f4425725cd0b6a111bc904230b', 1);
--
-- Database: `larafitness`
--

-- --------------------------------------------------------

--
-- Table structure for table `assessment_user`
--

CREATE TABLE IF NOT EXISTS `assessment_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `assessment_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `assessment` int(10) unsigned NOT NULL,
  `assessment_date` date NOT NULL,
  `assessment_time` time NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `assessment_user_assessment_id_index` (`assessment_id`),
  KEY `assessment_user_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=85 ;

--
-- Dumping data for table `assessment_user`
--

INSERT INTO `assessment_user` (`id`, `assessment_id`, `user_id`, `assessment`, `assessment_date`, `assessment_time`, `created_at`, `updated_at`) VALUES
(45, 1, 4, 1, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(46, 1, 11, 10, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(47, 2, 4, 5, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(48, 2, 11, 20, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(49, 3, 4, 1, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(50, 3, 11, 30, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(51, 4, 4, 1, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(52, 4, 11, 40, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(53, 5, 4, 11, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(54, 5, 11, 50, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(55, 6, 4, 1, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(56, 6, 11, 60, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(57, 8, 4, 1, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(58, 8, 11, 70, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(59, 9, 4, 1, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(60, 9, 11, 80, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(61, 10, 4, 1, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(62, 10, 11, 90, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(63, 11, 4, 1, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(64, 11, 11, 100, '2015-02-17', '00:20:15', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(65, 1, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(66, 2, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(67, 3, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(68, 4, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(69, 5, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(70, 6, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(71, 8, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(72, 9, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(73, 10, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(74, 11, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(75, 1, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(76, 2, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(77, 3, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(78, 4, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(79, 5, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(80, 6, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(81, 8, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(82, 9, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(83, 10, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(84, 11, 4, 0, '2015-03-31', '08:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `assessments`
--

CREATE TABLE IF NOT EXISTS `assessments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `unit` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `assessments`
--

INSERT INTO `assessments` (`id`, `name`, `unit`, `created_at`, `updated_at`) VALUES
(1, 'quod', 1, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(2, 'iusto', 6, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(3, 'totam', 9, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(4, 'quod', 7, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(5, 'dolorem', 8, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(6, 'ad', 5, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(8, 'et', 9, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(9, 'est', 10, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(10, 'omnis', 9, '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(11, 'animesh', 10, '2015-02-16 07:59:32', '2015-02-16 07:59:32');

-- --------------------------------------------------------

--
-- Table structure for table `contents`
--

CREATE TABLE IF NOT EXISTS `contents` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` tinyint(1) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `contents`
--

INSERT INTO `contents` (`id`, `title`, `body`, `url`, `type`, `created_at`, `updated_at`) VALUES
(1, 'Molestiae dicta ut.', 'Maxime facere consequatur eum qui animi.', 'http://Stracke.info/illum-non-aperiam-dolor-illo-reiciendis-cum', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(3, 'Et possimus vel aut.', 'Delectus ex repudiandae voluptatem voluptatum.', 'http://www.Leuschke.com/quia-in-soluta-tenetur-dolorum-in', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(4, 'Non perspiciatis.', 'Eius omnis natus omnis eos maiores.', 'http://Quitzon.com/libero-laboriosam-aliquam-atque-quo-aut-quia-inventore-cumque.html', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(5, 'animesh', 'Et sit enim iusto.', 'http://www.Hyatt.com/', 1, '2015-02-16 07:56:24', '2015-02-16 07:58:58'),
(6, 'Officiis eos.', 'Vero quas qui illo dicta autem aspernatur aut.', 'http://Bruen.com/', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(7, 'Sit veritatis.', 'Eligendi incidunt excepturi dolores sequi.', 'http://Rosenbaum.com/et-voluptas-numquam-sit-iste', 1, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(8, 'Et reiciendis quasi.', 'Accusamus dolores eligendi sapiente sunt aut.', 'https://www.Gleichner.info/sunt-non-non-adipisci-tempore-iusto', 1, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(10, 'Ex accusamus.', 'Qui nesciunt quod ea ut hic est aut.', 'http://Haag.biz/', 2, '2015-02-16 07:56:24', '2015-02-16 07:56:24');

-- --------------------------------------------------------

--
-- Table structure for table `medical_user`
--

CREATE TABLE IF NOT EXISTS `medical_user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `medical_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `medical_user_medical_id_index` (`medical_id`),
  KEY `medical_user_user_id_index` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=9 ;

--
-- Dumping data for table `medical_user`
--

INSERT INTO `medical_user` (`id`, `medical_id`, `user_id`, `created_at`, `updated_at`) VALUES
(5, 1, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 2, 6, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 1, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(8, 2, 11, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `medicals`
--

CREATE TABLE IF NOT EXISTS `medicals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `medicals`
--

INSERT INTO `medicals` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Cupiditate saepe ut.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(2, 'Quisquam sint.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(3, 'Neque molestiae nobis.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(4, 'Ea non quia.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(5, 'Sit itaque sit.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(6, 'Nisi quas.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(7, 'Ut unde rerum.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(8, 'Sit aut repellat.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(9, 'Et reprehenderit pariatur.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(10, 'Rerum eos.', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(11, 'disco', '2015-02-16 08:03:06', '2015-02-16 08:03:06');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_02_10_133238_create_users_table', 1),
('2015_02_12_103041_create_contents_table', 1),
('2015_02_12_131327_create_assessments_table', 1),
('2015_02_12_135944_create_medicals_table', 1),
('2015_02_12_170727_create_schedules_table', 1),
('2015_02_13_083731_create_assessment_user_table', 1),
('2015_02_15_064104_create_medical_user_table', 1),
('2015_02_15_064649_create_profiles_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `regId` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `gender` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `birth_date` date NOT NULL,
  `address1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mobile` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `postcode` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `profiles`
--

INSERT INTO `profiles` (`id`, `first_name`, `last_name`, `regId`, `user_id`, `gender`, `birth_date`, `address1`, `phone`, `mobile`, `address2`, `city`, `postcode`, `state`, `country`, `created_at`, `updated_at`) VALUES
(1, 'aa', 'aa', 'aa', 6, 'male', '0000-00-00', 'ss', '11', '11', '', '', '', '', '', '2015-02-16 07:57:51', '2015-02-16 07:57:51'),
(2, 'ruet', 'biswas', '', 11, 'male', '1981-02-01', 'qq', 'qqq', 'qq', '', '', '', 'NSW', 'au', '2015-02-16 08:03:06', '2015-02-28 11:45:38');

-- --------------------------------------------------------

--
-- Table structure for table `schedules`
--

CREATE TABLE IF NOT EXISTS `schedules` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `prev_date` date NOT NULL,
  `prev_time` time NOT NULL,
  `schedule_date` date NOT NULL,
  `schedule_time` time NOT NULL,
  `place` text COLLATE utf8_unicode_ci NOT NULL,
  `sent_to` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=7 ;

--
-- Dumping data for table `schedules`
--

INSERT INTO `schedules` (`id`, `prev_date`, `prev_time`, `schedule_date`, `schedule_time`, `place`, `sent_to`, `created_at`, `updated_at`) VALUES
(1, '1976-01-26', '07:14:01', '1996-12-12', '06:54:16', 'ctg', 'all', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(2, '2012-10-10', '00:33:42', '2011-08-05', '10:31:58', 'ctg', 'all', '2015-02-16 07:56:25', '2015-02-16 07:56:25'),
(6, '2015-02-04', '00:00:00', '2015-02-27', '00:00:00', 'dhk', '["user@example.com"]', '2015-02-16 08:10:36', '2015-02-16 08:10:36');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=12 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `is_admin`, `token`, `remember_token`, `created_at`, `updated_at`) VALUES
(4, 'lLeffler@hotmail.com', '$2y$10$fxSAEPVZkTx96q9fShYBI.6v5XJ9JX.datM1DrLWPfHkFdwYXMvnm', 0, 'i64b3d2d8183f73a64bd81757f8be024', NULL, '2015-02-16 07:56:23', '2015-02-16 07:56:23'),
(5, 'Alysson07@hotmail.com', '$2y$10$umG/Iu6fCychAAZJmiggYu0NRn7GV2etGkd0CWkdojRtoqxOO4.Pi', 0, 'x64b3d2d8183f73a64bd81757f8be024', NULL, '2015-02-16 07:56:23', '2015-02-16 07:56:23'),
(6, 'Kuphal.Marianne@hotmail.com', '$2y$10$z.JJj6IFFK3UTh1WdXWSF.u/uz5oC27VI5aIlfOOkVd3QZvjIi4e6', 0, 'uc4e92d6220cf17c6c2166966cdcaa66', NULL, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(7, 'Parker.Santiago@gmail.com', '$2y$10$Eo4m8BVWjV1cu6NfBisA5uR/./I0N3JfOrLiSzDbixtxydQXxg0na', 0, 'yc4e92d6220cf17c6c2166966cdcaa66', NULL, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(8, 'Jordy26@hotmail.com', '$2y$10$yL7Huj341JGljcPJw.eEyO2Tt3SMcrk5LsW0U.mcDgRtIMdxnj8Cy', 0, 'dc4e92d6220cf17c6c2166966cdcaa66', NULL, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(9, 'xHettinger@hotmail.com', '$2y$10$h29sLGF2CuX68vzKiK3.2OOk1ydOrT3u27gWfAnKyfthWbSQPG6.G', 0, 'lc4e92d6220cf17c6c2166966cdcaa66', NULL, '2015-02-16 07:56:24', '2015-02-16 07:56:24'),
(10, 'a@a.com', '$2y$10$5SxQij3SQwvac/PAF0KEjODqEM5/oMtYMMNnSPVr0W8UXeYtsvHiK', 1, 'cc4e92d6220cf17c6c2166966cdcaa66', 'zU4NvIs0Vlnv7zKfeFd4CE80qyhltK9PjIZ8czEd5eXuhJuei3fSJwCerd6L', '2015-02-16 07:56:24', '2015-03-05 10:22:30'),
(11, 'user@example.com', '$2y$10$zHu.SB/WeFQ6qXJF1kIHsuMgpl/zQ4RWwo0xkuTze/0ZjiHRAq5Ki', 0, 'w574f91da30d9b83ae88fde287291394', 'imBHb0gBGkvkOBQpLTj200qqMuhuEVUJOyI5sOv06ShTrzE7P1y7QlhVSCjg', '2015-02-16 08:03:06', '2015-02-16 08:12:43');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `assessment_user`
--
ALTER TABLE `assessment_user`
  ADD CONSTRAINT `assessment_user_assessment_id_foreign` FOREIGN KEY (`assessment_id`) REFERENCES `assessments` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `assessment_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `medical_user`
--
ALTER TABLE `medical_user`
  ADD CONSTRAINT `medical_user_medical_id_foreign` FOREIGN KEY (`medical_id`) REFERENCES `medicals` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `medical_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;
--
-- Database: `larasample`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2015_01_13_064752_create_posts_table', 1),
('2015_01_13_071455_create_users_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE IF NOT EXISTS `posts` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `body` text COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=11 ;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `title`, `body`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Et consequatur asperiores ut ut occaecati.fff', 'Elsie, Lacie, and Tillie; and they all moved off, and that makes them sour--and camomile that makes the matter with it. There could be no sort of present!'' thought Alice. ''I''m a--I''m a--'' ''Well! WHAT are you?'' said Alice, as the game was going to say,'' said the Dormouse turned out, and, by the carrier,'' she thought; ''and how funny it''ll seem, sending presents to one''s own feet! And how odd the directions will look! ALICE''S RIGHT FOOT, ESQ. HEARTHRUG, NEAR THE FENDER, (WITH ALICE''S LOVE). Oh dear, what nonsense I''m talking!'' Just then she looked back once or twice she had sat down again in a minute, nurse! But I''ve got to see what was on the bank--the birds with draggled feathers, the animals with their heads down and began an account of the thing yourself, some winter day, I will tell you my history, and you''ll understand why it is almost certain to disagree with you, sooner or later. However, this bottle was a child,'' said the Duchess, ''as pigs have to ask any more HERE.'' ''But.', 3, '2015-01-13 01:24:13', '2015-01-14 01:00:56'),
(2, 'Vel eveniet odit quo provident aliquid recusandae.', 'I''ve nothing to what I used to say.'' ''So he did, so he with his nose Trims his belt and his friends shared their never-ending meal, and the little magic bottle had now had its full effect, and she went on, ''What HAVE you been doing here?'' ''May it please your Majesty,'' said the Hatter. ''You might just as usual. ''Come, there''s half my plan done now! How puzzling all these strange Adventures of hers would, in the sand with wooden spades, then a great hurry. ''You did!'' said the Caterpillar. ''I''m afraid I don''t like the three were all shaped like ears and the March Hare said to itself ''Then I''ll go round a deal too flustered to tell me the list of the table. ''Nothing can be clearer than THAT. Then again--"BEFORE SHE HAD THIS FIT--" you never to lose YOUR temper!'' ''Hold your tongue!'' said the cook. ''Treacle,'' said a sleepy voice behind her. ''Collar that Dormouse,'' the Queen in front of them, and the other side of the day; and this time the Queen had never had to leave off this minute!'' She.', 1, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(3, 'Odio porro eaque non est earum.', 'This seemed to her in an offended tone. And the executioner myself,'' said the Hatter: ''it''s very rude.'' The Hatter was the first to speak. ''What size do you know what "it" means well enough, when I grow at a king,'' said Alice. ''I''ve tried every way, and then nodded. ''It''s no use going back to the Queen, ''and he shall tell you my history, and you''ll understand why it is right?'' ''In my youth,'' Father William replied to his ear. Alice considered a little, and then I''ll tell him--it was for bringing the cook was busily stirring the soup, and seemed to be found: all she could guess, she was going off into a tree. ''Did you say it.'' ''That''s nothing to do." Said the mouse doesn''t get out." Only I don''t think,'' Alice went timidly up to her usual height. It was as steady as ever; Yet you turned a corner, ''Oh my ears and the roof of the baby?'' said the Pigeon; ''but if you''ve seen them at last, and managed to swallow a morsel of the house before she had got to go on. ''And so these three little.', 2, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(4, 'Consequuntur porro totam doloremque animi molestiae vel et.', 'I hadn''t to bring tears into her eyes--and still as she left her, leaning her head to keep herself from being run over; and the little golden key and hurried upstairs, in great fear lest she should push the matter with it. There was exactly one a-piece all round. ''But she must have a trial: For really this morning I''ve nothing to do." Said the mouse to the little door: but, alas! the little crocodile Improve his shining tail, And pour the waters of the ground.'' So she began again: ''Ou est ma chatte?'' which was full of soup. ''There''s certainly too much frightened to say when I was sent for.'' ''You ought to have got into it), and sometimes she scolded herself so severely as to go and take it away!'' There was not even get her head made her next remark. ''Then the eleventh day must have been that,'' said the Cat, and vanished again. Alice waited till the puppy''s bark sounded quite faint in the wood,'' continued the King. The next witness was the first figure,'' said the Duchess, digging her.', 5, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(5, 'Ex assumenda maxime necessitatibus saepe.', 'Pigeon had finished. ''As if I must, I must,'' the King sharply. ''Do you mean "purpose"?'' said Alice. ''Why, there they lay on the bank, and of having the sentence first!'' ''Hold your tongue, Ma!'' said the Dodo, ''the best way to hear her try and repeat "''TIS THE VOICE OF THE SLUGGARD,"'' said the Mouse, frowning, but very glad she had got its neck nicely straightened out, and was just saying to herself ''It''s the oldest rule in the grass, merely remarking that a moment''s delay would cost them their lives. All the time she had felt quite unhappy at the Queen, ''and he shall tell you his history,'' As they walked off together, Alice heard it say to itself ''The Duchess! The Duchess! Oh my dear paws! Oh my dear Dinah! I wonder if I know I have none, Why, I wouldn''t be so easily offended!'' ''You''ll get used up.'' ''But what did the Dormouse sulkily remarked, ''If you knew Time as well to introduce some other subject of conversation. While she was trying to fix on one, the cook was leaning over the.', 5, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(6, 'Vel blanditiis aspernatur adipisci voluptas amet unde.', 'Queen added to one of the Queen of Hearts, she made it out to sea. So they got thrown out to sea as you can--'' ''Swim after them!'' screamed the Gryphon. ''It''s all her fancy, that: he hasn''t got no sorrow, you know. So you see, Miss, this here ought to eat or drink anything; so I''ll just see what was coming. It was opened by another footman in livery came running out of sight. Alice remained looking thoughtfully at the Duchess sneezed occasionally; and as he could go. Alice took up the fan she was getting quite crowded with the tarts, you know--'' ''What did they live at the Queen, turning purple. ''I won''t!'' said Alice. ''Who''s making personal remarks now?'' the Hatter said, turning to Alice to herself, and began bowing to the Knave. The Knave of Hearts, he stole those tarts, And took them quite away!'' ''Consider your verdict,'' the King replied. Here the other side, the puppy made another snatch in the window?'' ''Sure, it''s an arm, yer honour!'' (He pronounced it ''arrum.'') ''An arm, you goose!.', 3, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(7, 'Non quod magnam et nam fugit ea ut.', 'ALL RETURNED FROM HIM TO YOU,"'' said Alice. ''Did you say pig, or fig?'' said the Hatter. Alice felt a little bit of mushroom, and raised herself to some tea and bread-and-butter, and went down on their hands and feet at once, and ran till she fancied she heard a little timidly, for she thought, and looked at Alice, as she heard the Queen was to eat some of YOUR adventures.'' ''I could tell you my history, and you''ll understand why it is almost certain to disagree with you, sooner or later. However, this bottle was NOT marked ''poison,'' so Alice went on all the rats and--oh dear!'' cried Alice, quite forgetting that she had this fit) An obstacle that came between Him, and ourselves, and it. Don''t let him know she liked them best, For this must be Mabel after all, and I could shut up like a candle. I wonder what CAN have happened to you? Tell us all about as curious as it was an uncomfortably sharp chin. However, she did not seem to encourage the witness at all: he kept shifting from one.', 5, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(8, 'Excepturi fugit laudantium repellendus excepturi inventore voluptatem dolores.', 'Alice, ''when one wasn''t always growing larger and smaller, and being so many out-of-the-way things had happened lately, that Alice said; ''there''s a large arm-chair at one and then raised himself upon tiptoe, put his shoes on. ''--and just take his head sadly. ''Do I look like one, but it said in a low, hurried tone. He looked anxiously over his shoulder with some surprise that the mouse to the Knave of Hearts, carrying the King''s crown on a summer day: The Knave of Hearts, who only bowed and smiled in reply. ''That''s right!'' shouted the Queen, ''Really, my dear, and that is enough,'' Said his father; ''don''t give yourself airs! Do you think you''re changed, do you?'' ''I''m afraid I am, sir,'' said Alice; ''but when you throw them, and was delighted to find that she never knew whether it would be of any good reason, and as it lasted.) ''Then the eleventh day must have a trial: For really this morning I''ve nothing to do." Said the mouse doesn''t get out." Only I don''t remember where.'' ''Well, it.', 2, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(9, 'Natus repellendus sed et rerum vel debitis doloribus.', 'I must, I must,'' the King say in a voice outside, and stopped to listen. The Fish-Footman began by taking the little golden key and hurried upstairs, in great fear lest she should meet the real Mary Ann, and be turned out of a procession,'' thought she, ''what would become of me?'' Luckily for Alice, the little creature down, and was delighted to find herself talking familiarly with them, as if she could not think of what sort it was) scratching and scrambling about in all my life, never!'' They had not attended to this mouse? Everything is so out-of-the-way down here, and I''m sure _I_ shan''t be able! I shall have to turn into a large ring, with the clock. For instance, suppose it were white, but there were no arches left, and all would change to tinkling sheep-bells, and the small ones choked and had no very clear notion how delightful it will be much the same as they would die. ''The trial cannot proceed,'' said the Caterpillar decidedly, and the reason so many tea-things are put out.', 2, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(10, 'Fugiat molestias quisquam rerum corrupti enim qui sit.', 'Alice could think of nothing better to say when I got up this morning, but I think I should understand that better,'' Alice said nothing: she had read about them in books, and she jumped up in spite of all the unjust things--'' when his eye chanced to fall a long argument with the edge of the evening, beautiful Soup! ''Beautiful Soup! Who cares for fish, Game, or any other dish? Who would not give all else for two Pennyworth only of beautiful Soup? Pennyworth only of beautiful Soup? Pennyworth only of beautiful Soup? Beau--ootiful Soo--oop! Beau--ootiful Soo--oop! Beau--ootiful Soo--oop! Soo--oop of the baby, the shriek of the wood--(she considered him to be lost: away went Alice like the right size to do THAT in a shrill, passionate voice. ''Would YOU like cats if you drink much from a bottle marked ''poison,'' it is all the rest, Between yourself and me.'' ''That''s the judge,'' she said to the confused clamour of the teacups as the door began sneezing all at once. The Dormouse shook itself,.', 5, '2015-01-13 01:24:13', '2015-01-13 01:24:13');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=6 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `name`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'gCummings@Waelchi.com', '$2y$10$YroxSjza4llODNEk.Mf3M.kn36j6GeJWCSq73Cu46ZiPtd0a03jBq', 'Mrs. Vernie Reinger DDS', NULL, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(2, 'lKohler@yahoo.com', '$2y$10$OjkLddWMH7k/Ci8eCwCAPOEHk8DbxIXVG.8IyRaPlOVykRlKrKEF2', 'Helen Boehm', 'GuolzDeRZTpRW795QvUd3f1oFXxCpxVoomAQY7uoLOj2oEMMvCSWCaX1z3db', '2015-01-13 01:24:13', '2015-01-14 01:53:17'),
(3, 'Pfeffer.Genoveva@Howe.com', '$2y$10$p8Qj282hEwBCvDlQFUnb1eZ3kZQjPCYzmHFDwaIIBOrIeZaK34jga', 'Betsy Fadel MD', NULL, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(4, 'Gabriel.Hammes@hotmail.com', '$2y$10$/JhUjhKWQl0rBGbD4ErcPO9jSeXiYNy.EIh69YWmDBN62u0oh5ili', 'Miss Alycia Ernser', NULL, '2015-01-13 01:24:13', '2015-01-13 01:24:13'),
(5, 'Elyssa12@gmail.com', '$2y$10$KYj0kO1rkoylamTUfK1YReoo/i/4MBhW0QTsisna65xJMq.yrxshu', 'Mr. Theron Schmitt II', NULL, '2015-01-13 01:24:13', '2015-01-13 01:24:13');
--
-- Database: `laravel-tricks`
--

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE IF NOT EXISTS `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `order` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_slug_unique` (`slug`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `slug`, `description`, `order`, `created_at`, `updated_at`) VALUES
(1, 'Views', 'views', 'All tricks related to the View class, e.g. View Composers.', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Eloquent', 'eloquent', 'Eloquent ORM', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `category_trick`
--

CREATE TABLE IF NOT EXISTS `category_trick` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `category_id` int(10) unsigned NOT NULL,
  `trick_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `category_trick_category_id_foreign` (`category_id`),
  KEY `category_trick_trick_id_foreign` (`trick_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `category_trick`
--

INSERT INTO `category_trick` (`id`, `category_id`, `trick_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 1, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 2, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE IF NOT EXISTS `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2013_11_13_222806_create_users_table', 1),
('2013_11_13_222807_create_tricks_table', 1),
('2013_11_13_222808_create_tags_table', 1),
('2013_11_13_222809_create_categories_table', 1),
('2013_11_13_222810_create_votes_table', 1),
('2013_11_13_222811_create_tag_trick_table', 1),
('2013_11_13_222812_create_category_trick_table', 1),
('2013_11_13_222813_create_profiles_table', 1),
('2013_11_20_072925_add_order_to_category_table', 1),
('2013_11_22_072925_add_spam_to_tricks_table', 1),
('2013_11_25_202456_create_password_reminders_table', 1),
('2014_04_17_181442_add_remember_to_user_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_reminders`
--

CREATE TABLE IF NOT EXISTS `password_reminders` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  KEY `password_reminders_email_index` (`email`),
  KEY `password_reminders_token_index` (`token`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `profiles`
--

CREATE TABLE IF NOT EXISTS `profiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `uid` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `location` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `image_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `access_token_secret` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `profiles_user_id_foreign` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tag_trick`
--

CREATE TABLE IF NOT EXISTS `tag_trick` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `tag_id` int(10) unsigned NOT NULL,
  `trick_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tag_trick_tag_id_foreign` (`tag_id`),
  KEY `tag_trick_trick_id_foreign` (`trick_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tag_trick`
--

INSERT INTO `tag_trick` (`id`, `tag_id`, `trick_id`, `created_at`, `updated_at`) VALUES
(1, 1, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 2, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 1, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 3, 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tags_slug_unique` (`slug`),
  KEY `tags_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`, `slug`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'database', 'database', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'view data', 'view-data', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, '4.1', '41', NULL, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `tricks`
--

CREATE TABLE IF NOT EXISTS `tricks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `spam` tinyint(1) NOT NULL DEFAULT '0',
  `title` varchar(140) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `code` text COLLATE utf8_unicode_ci NOT NULL,
  `vote_cache` int(10) unsigned NOT NULL DEFAULT '0',
  `view_cache` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `tricks_slug_unique` (`slug`),
  KEY `tricks_user_id_foreign` (`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=4 ;

--
-- Dumping data for table `tricks`
--

INSERT INTO `tricks` (`id`, `spam`, `title`, `slug`, `description`, `code`, `vote_cache`, `view_cache`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 0, 'Display all SQL executed in Eloquent', 'display-all-sql-executed-in-eloquent', 'Put this in your routes.php file and you will see the SQL that Eloquent is executing when you go to pages that have any sort of access to Eloquent models. This helps a lot when debugging SQL in Laravel.', '// Display all SQL executed in Eloquent\nEvent::listen(''illuminate.query'', function($query)\n{\n    var_dump($query);\n});', 0, 0, 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 0, 'Dynamic View assignments', 'dynamic-view-assignments', 'Laravel offers a dynamic interface for assigning certain data. This can be used e.g. for adding a dynamic ''where'' to your Eloquent queries. However, not everyone know that this same trick also works on View assignments!', '// In Eloquent, you can assigned ''where'' clauses dynamically\nPost::whereSlug(''slug'')->get();\n// Results in ...WHERE `slug` = ''slug''...\n\n// The same trick is possible when using Views!\nView::make(''posts.index'')->withPosts($posts);\n\n// Same as: View::make(''posts.index'')->with(''posts'', $posts);', 0, 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 0, 'Eager loading constraints', 'eager-loading-constraints', 'Ever find yourself wanting to only find models that have a certain relationship matching certain conditions? Laravel 4.1 makes this really easy, and introduces great eager loading constraints!', '1', 0, 0, 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `photo` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=3 ;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `remember_token`, `email`, `photo`, `username`, `password`, `is_admin`, `created_at`, `updated_at`) VALUES
(1, NULL, 'user1@example.com', NULL, 'msurguy', '$2y$10$al/2fVeOYEdcUV36sPJDSe1RmBjEyHrKYBYb1Y3WNJy0yzh1S2Ffq', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, NULL, 'user2@example.com', NULL, 'stidges', '$2y$10$qYwH/pZbB8FbXL00ZleVgugwOUh4LSDA23/yWyM5zOx90AZmGF1ga', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `votes`
--

CREATE TABLE IF NOT EXISTS `votes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `trick_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `votes_user_id_foreign` (`user_id`),
  KEY `votes_trick_id_foreign` (`trick_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=1 ;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `category_trick`
--
ALTER TABLE `category_trick`
  ADD CONSTRAINT `category_trick_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `category_trick_trick_id_foreign` FOREIGN KEY (`trick_id`) REFERENCES `tricks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `profiles`
--
ALTER TABLE `profiles`
  ADD CONSTRAINT `profiles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tag_trick`
--
ALTER TABLE `tag_trick`
  ADD CONSTRAINT `tag_trick_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `tag_trick_trick_id_foreign` FOREIGN KEY (`trick_id`) REFERENCES `tricks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tags`
--
ALTER TABLE `tags`
  ADD CONSTRAINT `tags_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `tricks`
--
ALTER TABLE `tricks`
  ADD CONSTRAINT `tricks_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `votes`
--
ALTER TABLE `votes`
  ADD CONSTRAINT `votes_trick_id_foreign` FOREIGN KEY (`trick_id`) REFERENCES `tricks` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `votes_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
--
-- Database: `phpmyadmin`
--

-- --------------------------------------------------------

--
-- Table structure for table `pma__bookmark`
--

CREATE TABLE IF NOT EXISTS `pma__bookmark` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dbase` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `user` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `label` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `query` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Bookmarks' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pma__column_info`
--

CREATE TABLE IF NOT EXISTS `pma__column_info` (
  `id` int(5) unsigned NOT NULL AUTO_INCREMENT,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `column_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `comment` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `mimetype` varchar(255) CHARACTER SET utf8 NOT NULL DEFAULT '',
  `transformation` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  `transformation_options` varchar(255) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `db_name` (`db_name`,`table_name`,`column_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Column information for phpMyAdmin' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `pma__column_info`
--

INSERT INTO `pma__column_info` (`id`, `db_name`, `table_name`, `column_name`, `comment`, `mimetype`, `transformation`, `transformation_options`) VALUES
(1, 'larasample', 'users', 'remember_token', '', '', '_', ''),
(2, 'django_fitness', 'fitness_profile', 'gender', '', '', '_', '');

-- --------------------------------------------------------

--
-- Table structure for table `pma__designer_coords`
--

CREATE TABLE IF NOT EXISTS `pma__designer_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `x` int(11) DEFAULT NULL,
  `y` int(11) DEFAULT NULL,
  `v` tinyint(4) DEFAULT NULL,
  `h` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`db_name`,`table_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for Designer';

-- --------------------------------------------------------

--
-- Table structure for table `pma__history`
--

CREATE TABLE IF NOT EXISTS `pma__history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sqlquery` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`,`db`,`table`,`timevalue`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='SQL history for phpMyAdmin' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pma__pdf_pages`
--

CREATE TABLE IF NOT EXISTS `pma__pdf_pages` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `page_nr` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `page_descr` varchar(50) CHARACTER SET utf8 NOT NULL DEFAULT '',
  PRIMARY KEY (`page_nr`),
  KEY `db_name` (`db_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='PDF relation pages for phpMyAdmin' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `pma__recent`
--

CREATE TABLE IF NOT EXISTS `pma__recent` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `tables` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Recently accessed tables';

--
-- Dumping data for table `pma__recent`
--

INSERT INTO `pma__recent` (`username`, `tables`) VALUES
('root', '[{"db":"django_fitness","table":"fitness_profile"},{"db":"larafitness","table":"users"},{"db":"django_fitness","table":"fitness_medical"},{"db":"django_fitness","table":"fitness_schedule"},{"db":"django_fitness","table":"fitness_content"},{"db":"django_fitness","table":"fitness_userassessment"},{"db":"django_fitness","table":"auth_user"},{"db":"django_fitness","table":"snippets_snippet"},{"db":"larafitness","table":"schedules"},{"db":"django_fitness","table":"fitness_assessment"}]');

-- --------------------------------------------------------

--
-- Table structure for table `pma__relation`
--

CREATE TABLE IF NOT EXISTS `pma__relation` (
  `master_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `master_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_db` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_table` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `foreign_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`master_db`,`master_table`,`master_field`),
  KEY `foreign_field` (`foreign_db`,`foreign_table`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Relation table';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_coords`
--

CREATE TABLE IF NOT EXISTS `pma__table_coords` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `pdf_page_number` int(11) NOT NULL DEFAULT '0',
  `x` float unsigned NOT NULL DEFAULT '0',
  `y` float unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`db_name`,`table_name`,`pdf_page_number`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table coordinates for phpMyAdmin PDF output';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_info`
--

CREATE TABLE IF NOT EXISTS `pma__table_info` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  `display_field` varchar(64) COLLATE utf8_bin NOT NULL DEFAULT '',
  PRIMARY KEY (`db_name`,`table_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Table information for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__table_uiprefs`
--

CREATE TABLE IF NOT EXISTS `pma__table_uiprefs` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `prefs` text COLLATE utf8_bin NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`username`,`db_name`,`table_name`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='Tables'' UI preferences';

--
-- Dumping data for table `pma__table_uiprefs`
--

INSERT INTO `pma__table_uiprefs` (`username`, `db_name`, `table_name`, `prefs`, `last_update`) VALUES
('root', 'django_fitness', 'fitness_medical_users', '{"sorted_col":"`id` ASC"}', '2015-02-28 13:18:02'),
('root', 'django_fitness', 'fitness_profile', '{"sorted_col":"`user_id` ASC"}', '2015-02-28 14:59:08');

-- --------------------------------------------------------

--
-- Table structure for table `pma__tracking`
--

CREATE TABLE IF NOT EXISTS `pma__tracking` (
  `db_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `table_name` varchar(64) COLLATE utf8_bin NOT NULL,
  `version` int(10) unsigned NOT NULL,
  `date_created` datetime NOT NULL,
  `date_updated` datetime NOT NULL,
  `schema_snapshot` text COLLATE utf8_bin NOT NULL,
  `schema_sql` text COLLATE utf8_bin,
  `data_sql` longtext COLLATE utf8_bin,
  `tracking` set('UPDATE','REPLACE','INSERT','DELETE','TRUNCATE','CREATE DATABASE','ALTER DATABASE','DROP DATABASE','CREATE TABLE','ALTER TABLE','RENAME TABLE','DROP TABLE','CREATE INDEX','DROP INDEX','CREATE VIEW','ALTER VIEW','DROP VIEW') COLLATE utf8_bin DEFAULT NULL,
  `tracking_active` int(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`db_name`,`table_name`,`version`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin ROW_FORMAT=COMPACT COMMENT='Database changes tracking for phpMyAdmin';

-- --------------------------------------------------------

--
-- Table structure for table `pma__userconfig`
--

CREATE TABLE IF NOT EXISTS `pma__userconfig` (
  `username` varchar(64) COLLATE utf8_bin NOT NULL,
  `timevalue` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `config_data` text COLLATE utf8_bin NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_bin COMMENT='User preferences storage for phpMyAdmin';

--
-- Dumping data for table `pma__userconfig`
--

INSERT INTO `pma__userconfig` (`username`, `timevalue`, `config_data`) VALUES
('root', '2014-08-02 18:39:51', '{"collation_connection":"utf8mb4_general_ci"}');
--
-- Database: `test`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `test_multi_sets`()
    DETERMINISTIC
begin
        select user() as first_col;
        select user() as first_col, now() as second_col;
        select user() as first_col, now() as second_col, now() as third_col;
        end$$

DELIMITER ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
